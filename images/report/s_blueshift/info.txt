Scene configuration:

output scene information: yes
frames:
  count: 1
  dimensions: 1000x1000 pixels
  output color images: yes
  output redshift images: yes
  output pixel data: no

metric:
  m: 1
  a: 0
  q: 0

observer used for frames:
  r: 2.1
  theta: 85 deg
  phi: 0 deg
  yaw: 180 deg
  pitch: 0 deg
  roll: 0 deg
  hFov: 70 deg
  vFov: computed from aspect ratio

accretion disc:
  color1: 0, 255, 0
  color2: 255, 0, 255, calculated from color1
  resolution: 2, 12
  yaw: 0 deg
  radius: 2, 15

--------------------------------------------------------------

frame 0 animated values:
  no animated values.


