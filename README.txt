===========================================================
 cuRRay - CUDA relativistic ray tracer
 Version 2.0
 (c) 2018 by Sébastien C. Garmier
===========================================================

0 - Description
===========================================================

cuRRay is an acronym for CUDA Relativistic Raytracer.
cuRRay is a relativistic raytracer for photons in Kerr-
Newman spacetime.

The folder /cuRRay_dev contains the source code.
/images contains some images created with cuRRay.
/bin contains binaries for Windows and linux.
/pdf contains the cuRRay paper. This is also where more
information about the usage of the software can be found.

1 - Compiling on Windows 10
using Microsoft Visual Studio 2013
===========================================================

Visual Studio 2013 as well as the CUDA toolkit 8.0
need to be installed.

The following libraries are required and need to be
compiled with the vc 2013 compiler:
 libPNG (http://www.libpng.org/pub/png/libpng.html)
 zlib (http://zlib.net/)
 boost (http://www.boost.org/)
 yaml-cpp (https://github.com/jbeder/yaml-cpp)

libpng16.dll needs to be copied in the same directory as
cuRRay.exe.
 
Following environment variables need to be created:
 INCLUDE : path to header files of required libraries
 LIB	 : path to .lib files of required libraries

cuRRay should be compiled in 64-bit mode.

The subfolder cuRRay_dev contains the project files for
cuRRay. There is a CUDA-Version (cuRRay_dev) and a 
CPU-Version (cuRRay_dev_cpu)

2 - Compiling on Linux
using G++ 5
===========================================================

The GNU C++ compiler version 5 as well as
the CUDA toolkit version 8.0 need to be installed.

cuRRay should be compiled in 64-bit mode.

The following libraries are required and need to be
compiled with the G++ compiler:
 libPNG (http://www.libpng.org/pub/png/libpng.html)
 zlib (http://zlib.net/)
 boost (http://www.boost.org/)
 yaml-cpp (https://github.com/jbeder/yaml-cpp)

The subfolder cuRRay_dev contains the makefiles for cuRRay.
There is a CUDA-Version (release.makefile) and a CPU-Version
(release_cpu.makefile).
Once compiled, cuRRay is created in either the debug or
release folder depending on the configuration.

LD_LIBRARY_PATH needs to be set to the directory or 
directories where the dynamic library files are located.

A simple solution is to put all required library files
alongside cuRRay and set LD_LIBRARY_PATH to ./