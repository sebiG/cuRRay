// pngIO.h
//
// part of pngIO version 1.0
// Contains functions to load and save PNG files using streams
//
// (c) 2016 S?bastien Garmier

#include <string>
#include <cstdio>
#include <cmath>
#include <cstring>
#include <fstream>
#include <cstdint>

// This needs to be added to the include path
#include <png.h>

#ifndef PNGIO_H
#define PNGIO_H

class PngIO {

private:

	struct WriteStruct;
	struct ReadStruct;

public:

	/// \brief allows writing pngs row by row
	class PngWriter {

	public:

		/// \brief Constructor
		/// \param out Pointer to the output stream.
		/// \param width The image width.
		/// \param height The image height.
		/// \param colorType libpng color type of the image.
		/// \param bitDepth Bit depth of the image. Optional.
		/// \param interlaceMethod libpng interlace method to be used. Optional.
		/// \param compressionMethod libpng compression method to be used. Optional.
		/// \param filterType libpng filter type to be used. Optional.
		PngWriter(
			std::ostream* out,
			uint32_t width, uint32_t height,
			int colorType = PNG_COLOR_TYPE_RGB,
			uint32_t bitDepth = 8,
			int interlaceMethod = PNG_INTERLACE_NONE,
			int compressionMethod = PNG_COMPRESSION_TYPE_BASE,
			int filterType = PNG_FILTER_TYPE_BASE
		) :
			out(out),
			width(width),
			height(height),
			colorType(colorType),
			bitDepth(bitDepth),
			interlaceMethod(interlaceMethod),
			compressionMethod(compressionMethod),
			filterType(filterType),

			state(UNINITIALIZED),

			png_ptr(NULL),
			info_ptr(NULL),

			spp(0),
			bps(0),

			ws(nullptr)
		{}

		/// \brief Destructor
		~PngWriter() {
			Clean();
		}

		/// \brief Internal state
		enum State {
			UNINITIALIZED,
			PNG_STARTED,
			PNG_ENDED
		};

		/// \brief Returns state
		State GetState() { return state; }

		/// \brief Returns samples per pixel
		uint32_t GetSpp() { return spp; }

		/// \brief Returns bytes per sample
		uint32_t GetBps() { return bps; }

		/// \brief Start png file
		/// \exception Throws <tt>std::runtime_error</tt> in case of failure.
		void StartPng() {

			// Check stream state
			if(out->bad()) {
				Clean();
				throw std::runtime_error("PngIO::PngWriter::StartPng: ostream is bad.");
			}

			// Create png write struct
			png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
			if(png_ptr == NULL) {
				Clean();
				throw std::runtime_error("PngIO::PngWriter::StartPng: png_create_write_struct failed.");
			}

			// Create png info struct
			info_ptr = png_create_info_struct(png_ptr);
			if(info_ptr == NULL) {
				Clean();
				throw std::runtime_error("PngIO::PngWriter::StartPng: png_create_info_struct failed.");
			}

			// Set error handler
			if(setjmp(png_jmpbuf(png_ptr))) {
				Clean();
				throw std::runtime_error("PngIO::PngWriter::StartPng: some libpng function failed and triggered longjmp.");
			}

			// Init IO
			ws = new WriteStruct{ out };
			png_set_write_fn(png_ptr, ws, WriteToStream, FlushStream);

			// Write header
			png_set_IHDR(png_ptr, info_ptr, width, height, bitDepth, colorType, interlaceMethod, compressionMethod, filterType);

			// Write information
			png_write_info(png_ptr, info_ptr);

			// Get samples based on color type
			spp = GetSamples(colorType);
			// Get bytes per sample
			bps = (uint32_t)(ceil((double)bitDepth / 8.0));

			// Set state
			state = State::PNG_STARTED;
		}

		/// \brief Writes a row
		/// \param rowData Pointer to the row data. Must be a valid pointer.
		/// \exception Throws <tt>std::runtime_error</tt> in case of failure.
		void WriteRow(
			unsigned char const* rowData
		) {
			if(state != State::PNG_STARTED) {
				throw std::runtime_error("PngIO::PngWriter::WriteRow: PngWriter is in the wrong state.");
			}

			// Set error handler
			if(setjmp(png_jmpbuf(png_ptr))) {
				Clean();
				throw std::runtime_error("PngIO::PngWriter::WriteRow: some libpng function failed and triggered longjmp.");
			}

			png_write_row(png_ptr, const_cast<png_bytep>(rowData));
		}

		/// \brief Ends png file
		/// \exception Throws <tt>std::runtime_error</tt> in case of failure.
		void EndPng() {
			if(state != State::PNG_STARTED) {
				throw std::runtime_error("PngIO::PngWriter::WriteRow: PngWriter is in the wrong state.");
			}

			png_write_end(png_ptr, NULL);

			Clean();

			// Set state
			state = State::PNG_ENDED;
		}

	private:

		/// \brief Clean png structures
		void Clean() {
			if(info_ptr != NULL) png_free_data(png_ptr, info_ptr, PNG_FREE_ALL, -1);
			if(png_ptr != NULL) png_destroy_write_struct((png_structpp)&png_ptr, (png_infopp)NULL);

			if(ws != nullptr) {
				delete ws;
				ws = nullptr;
			}
		}

		std::ostream* out;
		uint32_t width, height;
		int colorType;
		uint32_t bitDepth;
		int interlaceMethod;
		int compressionMethod;
		int filterType;

		State state;

		png_struct* png_ptr;
		png_info* info_ptr;

		uint32_t spp;
		uint32_t bps;

		WriteStruct* ws;
	};

	/// \brief Returns version string
	/// \return The version string
	static std::string VERSION() {
		return "1.1";
	}

	/// \brief Reads PNG data from stream into a raw memory buffer
	/// \param in The input stream
	/// \param width Pointer to where the width of the PNG is to be stored. Can be a nullptr.
	/// \param height Pointer to where the height of the PNG is to be stored. Can be a nullptr.
	/// \param colorType Pointer to where the color type of the PNG is the be stored. Corresponds to the libpng color types. Can be a nullptr.
	/// \param channels Pointer to where the channel count of the PNG is to be stored. Can be a nullptr.
	/// \param bitDepth Pointer to where the bit depth of the PNG is to be stored. Can be a nullptr.
	/// \param data Pointer to where the raw data is to be stored. Must be a valid pointer.
	/// \exception Throws <tt>std::runtime_error</tt> in case of generic failure.
	/// \exception Throws <tt>std::bad_alloc</tt> in case of bad allocation.
	static void Read(
		std::istream& in,
		uint32_t* width, uint32_t* height,
		int* colorType,
		uint32_t* channels,
		uint32_t* bitDepth,
		unsigned char** data
	) {

		// Variables
		png_struct* png_ptr = NULL;
		png_info* info_ptr = NULL;
		char header[8];

		// Cleanup code
		auto Clean = [png_ptr, info_ptr]() {
			if(info_ptr != NULL) png_free_data(png_ptr, info_ptr, PNG_FREE_ALL, -1);
			if(png_ptr != NULL) png_destroy_read_struct((png_structpp)&png_ptr, (png_infopp)&info_ptr, NULL);
		};
		
		// Check state of input stream
		// Hope the stream is binary
		if(in.bad()) {
			Clean();
			throw std::runtime_error("PngIO::Read: istream is bad.");
		}

		// Check header
		in.read(header, 8);
		if(png_sig_cmp((png_bytep)header, 0, 8)) {
			Clean();
			throw std::runtime_error("PngIO::Read: wrong header.");
		}

		// Create read struct
		png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
		if(png_ptr == NULL) {
			Clean();
			throw std::runtime_error("PngIO::Read: error creating png read struct.");
		}

		// Create info struct
		info_ptr = png_create_info_struct(png_ptr);
		if(info_ptr == NULL) {
			Clean();
			throw std::runtime_error("PngIO::Read: error creating png info struct.");
		}

		// Set error handler
		if(setjmp(png_jmpbuf(png_ptr))) {
			Clean();
			throw std::runtime_error("PngIO::Read: some libpng function failed and triggered longjmp.");
		}

		// Init IO
		ReadStruct rs = { &in };
		png_set_read_fn(png_ptr, &rs, ReadFromStream);

		// Set sig bytes
		png_set_sig_bytes(png_ptr, 8);

		// Read info
		png_read_info(png_ptr, info_ptr);

		uint32_t _width, _height;
		int _colorType;
		uint32_t _channels, _bitDepth;

		_width = (uint32_t)png_get_image_width(png_ptr, info_ptr);
		_height = (uint32_t)png_get_image_height(png_ptr, info_ptr);
		_colorType = png_get_color_type(png_ptr, info_ptr);
		_channels = png_get_channels(png_ptr, info_ptr);
		_bitDepth = (uint32_t)png_get_bit_depth(png_ptr, info_ptr);

		int numberOfPasses = png_set_interlace_handling(png_ptr);
		png_read_update_info(png_ptr, info_ptr);

		// Alloc image
		uint32_t rowBytes = (uint32_t)png_get_rowbytes(png_ptr, info_ptr);
		*data = nullptr;
		*data = new unsigned char[rowBytes * _height];
		if(data == nullptr) {
			throw std::runtime_error("PngIO::Read: cannot allocate memory for image data.");
		}

		// Read image
		for(uint32_t y = 0; y < _height; y++) {
			png_read_row(png_ptr, &(*data)[y * rowBytes], NULL);
		}

		// Store information
		if(width != nullptr) *width = _width;
		if(height != nullptr) *height = _height;
		if(colorType != nullptr) *colorType = _colorType;
		if(channels != nullptr) *channels = _channels;
		if(bitDepth != nullptr) *bitDepth = _bitDepth;

		// End
		png_read_end(png_ptr, info_ptr);

		Clean();
	}

	/// \brief Writes raw image data to stream in PNG format.
	/// \param out The output stream.
	/// \param width The image width.
	/// \param height The image height.
	/// \param data Pointer to the raw data. Must be a valid pointer.
	/// \param colorType libpng color type of the image.
	/// \param bitDepth Bit depth of the image. Optional.
	/// \param interlaceMethod libpng interlace method to be used. Optional.
	/// \param compressionMethod libpng compression method to be used. Optional.
	/// \param filterType libpng filter type to be used. Optional.
	/// \exception Throws <tt>std::runtime_error</tt> in case of failure.
	static void Write(
		std::ostream& out,
		uint32_t width, uint32_t height,
		unsigned char const* data,
		int colorType = PNG_COLOR_TYPE_RGB,
		uint32_t bitDepth = 8,
		int interlaceMethod = PNG_INTERLACE_NONE,
		int compressionMethod = PNG_COMPRESSION_TYPE_BASE,
		int filterType = PNG_FILTER_TYPE_BASE
	) {

		// Pointers
		png_struct* png_ptr = NULL;
		png_info* info_ptr = NULL;

		// Cleanup code
		auto Clean = [png_ptr, info_ptr]() {
			if(info_ptr != NULL) png_free_data(png_ptr, info_ptr, PNG_FREE_ALL, -1);
			if(png_ptr != NULL) png_destroy_write_struct((png_structpp)&png_ptr, (png_infopp)NULL);
		};

		// Check stream state
		if(out.bad()) {
			Clean();
			throw std::runtime_error("PngIO::Write: ostream is bad.");
		}

		// Create png write struct
		png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
		if(png_ptr == NULL) {
			Clean();
			throw std::runtime_error("PngIO::Write: png_create_write_struct failed.");
		}

		// Create png info struct
		info_ptr = png_create_info_struct(png_ptr);
		if(info_ptr == NULL) {
			Clean();
			throw std::runtime_error("PngIO::Write: png_create_info_struct failed.");
		}

		// Set error handler
		if(setjmp(png_jmpbuf(png_ptr))) {
			Clean();
			throw std::runtime_error("PngIO::Write: some libpng function failed and triggered longjmp.");
		}

		// Init IO
		WriteStruct ws = { &out };
		png_set_write_fn(png_ptr, &ws, WriteToStream, FlushStream);

		// Write header
		png_set_IHDR(png_ptr, info_ptr, width, height, bitDepth, colorType, interlaceMethod, compressionMethod, filterType);

		// Write information
		png_write_info(png_ptr, info_ptr);

		// Get samples based on color type
		uint32_t spp = GetSamples(colorType);
		// Get bytes per sample
		uint32_t bps = (uint32_t)(ceil((double)bitDepth / 8.0));
		for(uint64_t y = 0; y < height; y++) {
			png_write_row(png_ptr, const_cast<png_bytep>(&data[y * spp * bps * width]));
		}

		png_write_end(png_ptr, NULL);

		Clean();
	}

	/// \brief Returns the sample count pertaining to a given libpng color type.
	/// \return The sample count.
	/// \param colorType libpng color type.
	static uint32_t GetSamples(int colorType) {
		switch(colorType) {
			case PNG_COLOR_TYPE_GRAY:
				return 1;
			case PNG_COLOR_TYPE_PALETTE:
				return 1;
			case PNG_COLOR_TYPE_RGB:
				return 3;
			case PNG_COLOR_TYPE_RGBA:
				return 4;
			case PNG_COLOR_TYPE_GA:
				return 2;
			default:
				return 0;
		}
	}

private:

	// Internal

	struct ReadStruct {
		std::istream* is;
	};
	struct WriteStruct {
		std::ostream* os;
	};

	static void ReadFromStream(png_structp png_ptr, png_bytep outBytes, png_size_t length) {
		void* io_ptr = png_get_io_ptr(png_ptr);
		if(io_ptr == NULL) return;
		std::istream* in = ((ReadStruct*)io_ptr)->is;

		in->read((char*)outBytes, length);
	}
	static void WriteToStream(png_structp png_ptr, png_bytep inBytes, png_size_t length) {
		void* io_ptr = png_get_io_ptr(png_ptr);
		if(io_ptr == NULL) return;
		std::ostream* in = ((WriteStruct*)io_ptr)->os;

		in->write((char*)inBytes, length);
	}
	static void FlushStream(png_structp png_ptr) {}

};

#endif
