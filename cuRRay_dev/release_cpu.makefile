SRC = ./src
OBJ = ./x64/Release_cpu_linux
OUT = ../x64/Release_cpu_linux

INC = -I. -I./src -I/usr/include -I/usr/local/include -I./redist/tclap-1.2.1/include -I./redist/pngIO
LIB = -L/usr/lib -L/usr/local/lib

CC = g++-5

CC_FLAGS = \
	-m64 -std=c++11 -O2

LINK_FLAGS = \
	-lpthread -lpng -lz -lboost_system -lboost_filesystem -lyaml-cpp

OBJECTS = \
	kerr_newman/frameRenderer.o kerr_newman/raytracer.o kerr_newman/scene.o \
	system/log.o system/program.o \
	main.o

OBJECT_FILES = $(patsubst %,$(OBJ)/%,$(OBJECTS))

all: cuRRay

cuRRay: create_directories $(OBJECT_FILES)
	$(CC) $(CC_FLAGS) $(OBJECT_FILES) -o $(OUT)/cuRRay_cpu $(LINK_FLAGS)

$(OBJ)/%.o: $(SRC)/%.cpp
	-mkdir -p $(dir $@)
	$(CC) $(CC_FLAGS) -c $(INC) $(LIB) $< -o $@

$(OBJ)/%.o: $(SRC)/%.cu
	-mkdir -p $(dir $@)
	$(CC) $(CC_FLAGS) -c -x c++ $(INC) $(LIB) $< -o $@

create_directories:
	-mkdir -p $(OBJ)
	-mkdir -p $(OUT)

clean:
	rm -f -r $(OBJ)
	rm -f -r $(OUT)
