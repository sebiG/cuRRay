//
// This file is part of cuRRay.
//
// (c) 2018 Sébastien Garmier (sebastien.garmier@gmail.com)
//

#ifndef BOUNDED_H
#define BOUNDED_H

#include "stdafx.h"

#include "util/util.h"

#include <iostream>
#include <string>

#ifdef BOUNDED_USE_TCLAP
#include <tclap/ArgTraits.h>
#endif

namespace cuRRay {
namespace Util {

struct Dummy {};

/// \brief Boundary types
enum BoundaryType {
	NONE,
	INCLUSIVE,
	EXCLUSIVE
};

/// \brief Class for bounded values
template<
	typename T,
	BoundaryType lowerType = BoundaryType::NONE,
	T lower = T(0),
	BoundaryType upperType = BoundaryType::NONE,
	T upper = T(0)
>
class Bounded : public Dummy
#ifdef BOUNDED_USE_TCLAP
	, public TCLAP::ValueLike
#endif
{

public:

	/// \brief This type
	typedef Bounded<T, lowerType, lower, upperType, upper> ThisType;

	/// \brief Default Constructor
	Bounded() = default;
	/// \brief Value constructor
	Bounded(T val) :
		m_val(val) {}
	/// \brief String constructor
	Bounded(std::string const& str) {
		std::stringstream ss(str);
		ss >> *this;
	}

	/// \brief Destructor
	~Bounded() = default;

	/// \brief Assignment operator
	void operator=(T const& val) {
		m_val = val;
	}
	/// \brief Cast operator
	operator T() {
		return m_val;
	}

	/// \brief Stream extraction operator
	friend std::istream& operator>>(std::istream& is, ThisType& bounded) {
		auto oldPos = is.tellg();

		is >> bounded.m_val;
		if(is) {
			if(!bounded.IsValid()) {
				is.setstate(ios::failbit);
				is.seekg(oldPos);
			}
		}

		return is;
	}

	/// \brief Stream insertion operator
	friend std::ostream& operator<<(std::ostream& os, ThisType const& bounded) {
		os << bounded.m_val;
		return os;
	}

	/// \brief Checks whether m_val is in bounds
	bool IsValid() const {
		bool low = true;
		bool high = true;

		switch(lowerType) {
			case BoundaryType::EXCLUSIVE:
				low = m_val > lower;
				break;
			case BoundaryType::INCLUSIVE:
				low = m_val >= lower;
				break;
		}

		switch(upperType) {
			case BoundaryType::EXCLUSIVE:
				high = m_val < upper;
				break;
			case BoundaryType::INCLUSIVE:
				high = m_val <= lower;
				break;
		}

		return (low && high);
	}

	/// \brief Returns boundary description
	std::string GetBoundaryDescription() {
		std::stringstream ss;
		if(lowerType == BoundaryType::NONE && upperType == BoundaryType::NONE) return "";

		ss << "must be ";
		
		switch(lowerType) {
			case BoundaryType::EXCLUSIVE:
				ss << "larger than " << lower;
				break;
			case BoundaryType::INCLUSIVE:
				ss << "at least " << lower;
				break;
		}
		if(lowerType != BoundaryType::NONE && upperType != BoundaryType::NONE) ss << " and ";

		switch(upperType) {
			case BoundaryType::EXCLUSIVE:
				ss << "smaller than " << upper;
				break;
			case BoundaryType::INCLUSIVE:
				ss << "at most " << upper;
				break;
		}

		ss << ".";

		return ss.str();
	}

	T m_val;
};

/// \brief Like char, but parses like an int
typedef Bounded<int16_t, BoundaryType::INCLUSIVE, -128, BoundaryType::INCLUSIVE, 127> int8;

/// \brief Like unsigned char, but parses like an int
typedef Bounded<uint16_t, BoundaryType::INCLUSIVE, 0, BoundaryType::INCLUSIVE, 255> uint8;

}
}

#endif