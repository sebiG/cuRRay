//
// This file is part of cuRRay.
//
// (c) 2018 S?bastien Garmier (sebastien.garmier@gmail.com)
//

#ifndef ANGLE_H
#define ANGLE_H

#include "stdafx.h"

#include <iostream>
#include <string>
#include <sstream>

#include "util/math.h"

#include <yaml-cpp/yaml.h>

namespace cuRRay {
namespace Util {

/// \brief Class to hold angles in different units as well as providing conversions between them
class Angle {

public:

	/// \brief Angle units
	enum class Unit {
		DEGREE,
		PI_RAD,
		RADIAN
	};

	const static Unit DEFAULT_UNIT = Unit::DEGREE;

	/// \brief Angle in degrees
	double m_deg;
	/// \brief Angle in radians times pi
	double m_piRad;
	/// \brief Angle in radians
	double m_rad;

	/// \brief The unit with which the angle was loaded
	Unit m_originalUnit;

	/// \brief Default Constructor
	Angle() :
		m_rad(0),
		m_deg(0),
		m_originalUnit(DEFAULT_UNIT) {
	};
	/// \brief Value constructor
	Angle(double value, Unit unit = DEFAULT_UNIT) {
		SetValue(value, unit);
	}
	/// \brief Destructor
	~Angle() = default;

	/// \brief Double assignment
	void operator=(double const& d) {
		SetValue(d, DEFAULT_UNIT);
	}

	/// \brief Addition operator
	Angle operator+(Angle const& value) const {
		Angle angle;
		angle.m_originalUnit = m_originalUnit;
		angle.m_deg = m_deg + value.m_deg;
		angle.m_rad = m_rad + value.m_rad;
		angle.m_piRad = m_piRad + value.m_piRad;
		return angle;
	}
	/// \brief Addition assignment operator
	Angle& operator+=(Angle const& value) {
		m_deg += value.m_deg;
		m_rad += value.m_rad;
		m_piRad += value.m_piRad;
		return *this;
	}
	/// \brief Subtraction operator
	Angle operator-(Angle const& value) const {
		Angle angle;
		angle.m_originalUnit = m_originalUnit;
		angle.m_deg = m_deg - value.m_deg;
		angle.m_rad = m_rad - value.m_rad;
		angle.m_piRad = m_piRad - value.m_piRad;
		return angle;
	}
	/// \brief Subtraction assignment operator
	Angle& operator-=(Angle const& value) {
		m_deg -= value.m_deg;
		m_rad -= value.m_rad;
		m_piRad -= value.m_piRad;
		return *this;
	}
	/// \brief Multiplication operator
	Angle operator*(double const& value) const {
		Angle angle;
		angle.m_originalUnit = m_originalUnit;
		angle.m_deg = m_deg * value;
		angle.m_rad = m_rad * value;
		angle.m_piRad = m_piRad * value;
		return angle;
	}
	/// \brief Inverse multiplication operator
	friend Angle operator*(double const& d, Angle const& a) {
		Angle angle;
		angle.m_originalUnit = a.m_originalUnit;
		angle.m_deg = a.m_deg * d;
		angle.m_rad = a.m_rad * d;
		angle.m_piRad = a.m_piRad * d;
		return angle;
	}
	/// \brief Multiplication assignment operator
	Angle& operator*=(Angle const& value) {
		m_deg *= value.m_deg;
		m_rad *= value.m_rad;
		m_piRad *= value.m_piRad;
		return *this;
	}
	/// \brief Division operator
	Angle operator/(double const& value) const {
		Angle angle;
		angle.m_originalUnit = m_originalUnit;
		angle.m_deg = m_deg / value;
		angle.m_rad = m_rad / value;
		angle.m_piRad = m_piRad / value;
		return angle;
	}
	/// \brief Division assignment operator
	Angle& operator/=(Angle const& value) {
		m_deg /= value.m_deg;
		m_rad /= value.m_rad;
		m_piRad /= value.m_piRad;
		return *this;
	}

	/// \brief Sets values of angle
	void SetValue(double value, Unit unit) {
		m_originalUnit = unit;
		if(unit == Unit::DEGREE) {
			m_deg = value;
			m_piRad = value / 180.0;
			m_rad = value * MATH_PI / 180.0;
		} else if(unit == Unit::PI_RAD) {
			m_deg = value * 180.0;
			m_piRad = value;
			m_rad = value * MATH_PI;
		} else {
			m_deg = value * 180.0 / MATH_PI;
			m_piRad = value / MATH_PI;
			m_rad = value;
		}
	}

	/// \brief Parse from string
	///
	/// Accepted formats are:
	///   180			(degrees)
	///   180 deg		(degrees)
	///   1 pi			(radians times pi)
	///   3.145 rad     (radians)
	///
	/// \param s The string
	/// \return Success indicator
	bool Parse(std::string const& s) {
		std::stringstream ss(s);

		// Read number first
		double v;
		ss >> v;
		if(!ss) {
			return false;
		}

		// Default unit
		Unit unit = DEFAULT_UNIT;

		// Check whether whitespaces with "pi" follow
		std::string u;
		ss >> u;

		if(u == "pi") {
			unit = Unit::PI_RAD;
		} else if(u == "rad") {
			unit = Unit::RADIAN;
		} else if(u != "deg" && !u.empty()) {
			return false;
		}

		// Set value
		SetValue(v, unit);

		return true;
	}

	/// \brief String stream extraction
	/// The stream extraction is only defined for string streams, as the entire stream
	/// will be consumed.
	/// \param is The input string stream
	/// \param a The angle
	/// \return The input string stream
	friend std::istringstream& operator>>(std::istringstream& is, Angle& a) {
		if(!a.Parse(is.str())) {
			is.setstate(std::ios::badbit);
		}
		return is;
	}

	/// \brief Prints angle in original units
	/// \param os The output stream
	/// \param a The angle
	/// \return The output stream
	friend std::ostream& operator<<(std::ostream& os, Angle const& a) {
		if(a.m_originalUnit == Unit::DEGREE) {
			os << a.m_deg << " deg";
		} else if(a.m_originalUnit == Unit::PI_RAD) {
			os << a.m_piRad << " pi";
		} else {
			os << a.m_rad << " rad";
		}

		return os;
	}

	/// \brief Reads an angle from a YAML node
	/// \param node The node
	/// \return The parsed angle
	/// \exception YAML::BadConversion In case of bad conversion
	static Angle ParseYAML(YAML::Node const& node) {
		Angle angle;
		
		if(!angle.Parse(node.as<std::string>())) {
			throw YAML::BadConversion(node.Mark());
		}

		return angle;
	}

};

}
}

#endif
