//
// This file is part of cuRRay.
//
// (c) 2018 S?bastien Garmier (sebastien.garmier@gmail.com)
//

#ifndef ANIMATED_H
#define ANIMATED_H

#include "stdafx.h"

#include <string>
#include <sstream>

#include <yaml-cpp/yaml.h>

namespace cuRRay {
namespace Util {

/// \brief Animation type
enum AnimationType {
	SINGLE,
	LINEAR
};

/// \brief Class for animated values
template<typename T>
struct Animated {

	/// \brief The animation type
	AnimationType m_type;

	/// \brief The value when the type is single 
	T m_singleValue;

	/// \brief The values when the type is linear
	struct {
		/// \brief The starting value
		T m_start;
		/// \brief The end value
		T m_end;
	} m_linear;

	/// \brief Default constructor
	Animated() = default;

	/// \brief Constructor
	Animated(T const& value) {
		m_type = AnimationType::SINGLE;
		m_singleValue = value;
	}

	/// \brief Assignment operator
	void operator=(T const& value) {
		m_type = AnimationType::SINGLE;
		m_singleValue = value;
	}

	/// \brief Animates according to the parameter t
	/// \param t The animation parameter (0 to 1)
	/// \return The animated value
	T Animate(double t) const {
		switch(m_type) {
			case AnimationType::LINEAR:
				return LinearAnimate(t);
			default:
				return m_singleValue;
		}
	}

	/// \brief Lienarly interpolates the value from start to end according to the parameter t
	/// \param t The interpolation parameter (0 to 1)
	/// \return The interpolated values
	T LinearAnimate(double t) const {
		return m_linear.m_start + (m_linear.m_end - m_linear.m_start) * t;
	}

	/// \brief Stream insertion operator
	friend std::ostream& operator<<(std::ostream& os, Animated const& a) {
		switch(a.m_type) {
			case AnimationType::SINGLE:
				os << a.m_singleValue;
				break;
			case AnimationType::LINEAR:
				os << "[ linear, " << a.m_linear.m_start << ", " << a.m_linear.m_end << " ]";
				break;
		}
		return os;
	}

	/// \brief Parses the animated from YAML
	/// \param node The node
	/// \return The parsed animation
	/// \exception YAML::BadConversion in case of bad conversions
	static Animated ParseYAML(YAML::Node const& node) {
		Animated animated;

		std::istringstream ss;

		if(node.IsScalar()) {
			animated.m_type = AnimationType::SINGLE;
			ss.str(node.as<std::string>());
			if(!(ss >> animated.m_singleValue)) {
				throw YAML::BadConversion(node.Mark());
			}
		} else if(node.IsSequence()) {
			if(node.size() == 3) {
				if(node[0].as<std::string>() == "linear") {
					animated.m_type = AnimationType::LINEAR;
					ss.str(node[1].as<std::string>());
					if(!(ss >> animated.m_linear.m_start)) {
						throw YAML::BadConversion(node.Mark());
					}
					ss.clear();
					ss.str(node[2].as<std::string>());
					if(!(ss >> animated.m_linear.m_end)) {
						throw YAML::BadConversion(node.Mark());
					}
				} else {
					throw YAML::BadConversion(node.Mark());
				}
			} else {
				throw YAML::BadConversion(node.Mark());
			}
		} else {
			throw YAML::BadConversion(node.Mark());
		}

		return animated;
	}

};

}
}

#endif
