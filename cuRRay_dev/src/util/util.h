//
// This file is part of cuRRay.
//
// (c) 2018 S?bastien Garmier (sebastien.garmier@gmail.com)
//

#ifndef UTIL_H
#define UTIL_H

#include "stdafx.h"

#include <yaml-cpp/yaml.h>

#include <string>
#include <codecvt>
#include <locale>
#include <iostream>

#define S1(x) #x
#define S2(x) S1(x)

/// \def DebugCodeLocation()
/// \brief Uses __FILE__ and __LINE__ to produce string with information of current code location.
#define DebugCodeLocation __FILE__ ", l:" S2(__LINE__)

namespace cuRRay {

/// \breif Util namespace
/// Used to give the bare function calls a context
namespace Util {

/// \brief String converter
static std::wstring_convert<std::codecvt_utf8<wchar_t>> stringConverter;

/// \brief Removes leading whitespaces in string
static std::string LTrim(std::string const& str) {
	return str.substr(str.find_first_not_of(" \t"));
}

/// \brief Removes trailing whitespaces in string
static std::string RTrim(std::string const& str) {
	return str.substr(0, str.find_last_not_of(" \t") + 1);
}

/// \brief Converts object to string using std::stringstream
///
/// \param obj Object to convert
/// \return String representation of object
template<typename T>
static std::string ToString(T obj) {
	std::stringstream ss;
	ss << obj << std::flush;
	return ss.str();
}

/// \brief Parses object from string using std::stringstream
///
/// \param str String to parse
/// \param obj %Output object
/// \return Tells, whether parsing was successful
template<typename T>
static bool FromString(std::string const& str, T* obj) {
	std::stringstream ss;
	ss.str(str);

	ss >> *obj;

	return !ss.fail();
}

/// \brief Parses hex object from string using std::stringstream
///
/// \param str String to parse
/// \param obj %Output object
/// \return Tells, whether parsing was successful
template<typename T>
static bool HexFromString(std::string const& str, T* obj) {
	std::stringstream ss;
	ss.str(str);

	ss >> std::hex;

	ss >> *obj;

	return !ss.fail();
}

/// \brief Prints a YAML exception
/// \param message The message to display
/// \param mark The YAML mark where the exception occured
static std::string PrintYAMLException(std::string const& message, YAML::Mark const& mark = YAML::Mark::null_mark()) {
	std::stringstream ss;
	ss << message;
	if(!mark.is_null()) {
		ss << " (line " << mark.line << ")";
	}
	return ss.str();
}

/// \brief Reads a 

/// \brief Handles exceptions which occured in multiple threads
/// This rethrows all exceptions combined into one
/// \param exceptions The exceptions. First is the thread id, second is the exception itself.
static void HandleThreadExceptions(std::map<int, std::runtime_error> const& exceptions, char const* codeLocation) {
	if(!exceptions.empty()) {
		std::stringstream ss;

		ss << exceptions.size() << " runtime_error(s) occured in different threads." << std::endl;
		ss << "At: " << codeLocation << std::endl;

		for(auto& i : exceptions) {
			std::string error = i.second.what();

			ss << "  In thread " << i.first << ":" << std::endl;
			while(true) {
				std::string s = ss.str();

				auto pos = error.find('\n');
				ss << "  " << error.substr(0, pos) << std::endl;

				if(pos == std::string::npos) break;
				error = error.substr(pos + 1);
			}
		}

		throw std::runtime_error(ss.str());
	}
}

/// @name CUDA util
/// @{

#ifdef USE_CUDA

#ifndef __CUDACC__
#define __launch_bounds__(a,b)
#endif

/// \brief Formats a cudaError
/// \param error The error code
/// \param function The name of the function where the error happend
/// \param codeLocation A string describing where the error happend in code
/// \return The formatted string
static std::string CUDAError(cudaError error, char const* function, char const* codeLocation) {
	std::stringstream ss;
	ss << function << " failed with error code ";
	ss << error << " (" << cudaGetErrorName(error) << ")." << std::endl;
	ss << "At: " << codeLocation << "." << std::endl;

	return ss.str();
}

#endif

/// @}

}
}

#endif
