//
// This file is part of cuRRay.
//
// (c) 2018 S?bastien Garmier (sebastien.garmier@gmail.com)
//

#ifndef COLOR_H
#define COLOR_H

#include "stdafx.h"

#include <yaml-cpp/yaml.h>

namespace cuRRay {
namespace Util {

/// \brief RGB color
struct Color {

	/// \brief Reads a color from a yaml node
	/// \param node The node
	/// \return The parsed color
	/// \exception YAML::BadConversion In the case of a bad conversion
	static Color ParseYAML(YAML::Node const& node) {
		Color color;

		if(node.IsSequence() && node.size() == 3) {
			uint64_t red = node[0].as<uint64_t>();
			uint64_t green = node[1].as<uint64_t>();
			uint64_t blue = node[2].as<uint64_t>();

			if(red > 255 || green > 255 || blue > 255) {
				throw YAML::BadConversion(node.Mark());
			}

			color.m_red = (uint8_t)red;
			color.m_green = (uint8_t)green;
			color.m_blue = (uint8_t)blue;
		} else {
			throw YAML::BadConversion(node.Mark());
		}

		return color;
	}

	uint8_t m_red;
	uint8_t m_green;
	uint8_t m_blue;
};

}
}

#endif
