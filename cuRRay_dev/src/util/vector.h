//
// This file is part of cuRRay.
//
// (c) 2018 Sébastien Garmier (sebastien.garmier@gmail.com)
//

#ifndef VECTOR_H
#define VECTOR_H

#include "stdafx.h"

#include <cassert>
#include <limits>
#include <cmath>
#include <ostream>

/// \file vector.h
/// \brief This vector library was originally conceived for Brane (see https://sebastiengarmier.ch/brane)

namespace cuRRay {
namespace Util {

// Cardinal angles

enum CardinalAngle {
	DEG_0,
	DEG_90,
	DEG_180,
	DEG_270
};

static CardinalAngle CWCardinalRotation(CardinalAngle angle) {
	switch(angle) {
		case DEG_0:
			return DEG_270;
		case DEG_90:
			return DEG_0;
		case DEG_180:
			return DEG_90;
		case DEG_270:
			return DEG_180;
		default:
			return DEG_270;
	}
}

static CardinalAngle CCWCardinalRotation(CardinalAngle angle) {
	switch(angle) {
		case DEG_0:
			return DEG_90;
		case DEG_90:
			return DEG_180;
		case DEG_180:
			return DEG_270;
		case DEG_270:
			return DEG_0;
		default:
			return DEG_90;
	}
}

static CardinalAngle OppositeCardinalAngle(CardinalAngle angle) {
	switch(angle) {
		case DEG_0:
			return DEG_180;
		case DEG_90:
			return DEG_270;
		case DEG_180:
			return DEG_0;
		case DEG_270:
			return DEG_90;
		default:
			return DEG_0;
	}
}

static CardinalAngle MirrorCardinalAngle(CardinalAngle angle) {
	switch(angle) {
		case DEG_0:
			return DEG_0;
		case DEG_90:
			return DEG_270;
		case DEG_180:
			return DEG_180;
		case DEG_270:
			return DEG_90;
		default:
			return DEG_0;
	}
}

static int32_t CardinalSin(CardinalAngle angle) {
	switch(angle) {
		case DEG_0:
			return 0;
		case DEG_90:
			return 1;
		case DEG_180:
			return 0;
		case DEG_270:
			return -1;
		default:
			return 0;
	}
}

static int32_t CardinalCos(CardinalAngle angle) {
	switch(angle) {
		case DEG_0:
			return 1;
		case DEG_90:
			return 0;
		case DEG_180:
			return -1;
		case DEG_270:
			return 0;
		default:
			return 1;
	}
}

#ifndef USE_ONLY_VECTOR4

// Vector2<T>

template<typename T>
class Vector2 {

public:

	union {
		struct {
			union {
				T m_1;
				T a;
				T x;
			};
			union {
				T m_2;
				T b;
				T y;
			};
		};
		T m_raw[2];
	};

	Vector2() : Vector2(0, 0) {};
	Vector2(T x, T y) : x(x), y(y) {}
	Vector2(Vector2<T> const& vector) {
		x = vector.x;
		y = vector.y;
	}
	Vector2(Vector2<T> const&& vector) {
		x = vector.x;
		y = vector.y;
	}
	Vector2(CardinalAngle angle) {
		switch(angle) {
			case DEG_0:
				x = T(1); y = T(0);
				break;
			case DEG_90:
				x = T(0); y = T(1);
				break;
			case DEG_180:
				x = T(-1); y = T(0);
				break;
			case DEG_270:
				x = T(0); y = T(-1);
				break;
		}
	}

	template<typename U>
	operator Vector2<U>() const {
		return Vector2<U>((U)x, (U)y);
	}

	Vector2<T>& operator=(Vector2<T> const& vector) {
		x = vector.x;
		y = vector.y;
		return *this;
	}
	Vector2<T>& operator=(Vector2<T> const&& vector) {
		x = vector.x;
		y = vector.y;
		return *this;
	}

	bool operator==(Vector2<T> const& vector) const {
		return (std::abs(x - vector.x) < std::numeric_limits<T>::epsilon()
			&& std::abs(y - vector.y) < std::numeric_limits<T>::epsilon());
	}
	bool operator!=(Vector2<T> const& vector) const {
		return (std::abs(x - vector.x) > std::numeric_limits<T>::epsilon()
			|| std::abs(y - vector.y) > std::numeric_limits<T>::epsilon());
	}

	Vector2<T> operator+(Vector2<T> const& vector) const {
		return{ x + vector.x, y + vector.y };
	}
	Vector2<T> operator+(Vector2<T> const&& vector) const {
		return{ x + vector.x, y + vector.y };
	}
	Vector2<T> operator-(Vector2<T> const& vector) const {
		return{ x - vector.x, y - vector.y };
	}
	Vector2<T> operator*(Vector2<T> const& vector) const {
		return{ x * vector.x, y * vector.y };
	}
	Vector2<T> operator/(Vector2<T> const& vector) const {
		return{ x / vector.x, y / vector.y };
	}
	Vector2<T> operator-() const {
		return{ -x, -y };
	}

	Vector2<T>& operator+=(Vector2<T> const& vector) {
		x += vector.x;
		y += vector.y;
		return *this;
	}
	Vector2<T>& operator-=(Vector2<T> const& vector) {
		x -= vector.x;
		y -= vector.y;
		return *this;
	}
	Vector2<T>& operator*=(Vector2<T> const& vector) {
		x *= vector.x;
		y *= vector.y;
		return *this;
	}
	Vector2<T>& operator/=(Vector2<T> const& vector) {
		x /= vector.x;
		y /= vector.y;
		return *this;
	}

	Vector2<T> operator+(T const& scalar) const {
		return{ x + scalar, y + scalar };
	}
	Vector2<T> operator-(T const& scalar) const {
		return{ x - scalar, y - scalar };
	}
	Vector2<T> operator*(T const& scalar) const {
		return{ scalar * x, scalar * y };
	}
	Vector2<T> operator/(T const& scalar) const {
		return{ x / scalar, y / scalar };
	}

	Vector2<T>& operator+=(T const& scalar) {
		x += scalar;
		y += scalar;
		return *this;
	}
	Vector2<T>& operator-=(T const& scalar) {
		x -= scalar;
		y -= scalar;
		return *this;
	}
	Vector2<T>& operator*=(T const& scalar) {
		x *= scalar;
		y *= scalar;
		return *this;
	}
	Vector2<T>& operator/=(T const& scalar) {
		x /= scalar;
		y /= scalar;
		return *this;
	}

	T Length() const {
		return (T)std::sqrt(x * x + y * y);
	}
	T Length2() const {
		return x * x + y * y;
	}
	Vector2<T> Normalize() const {
		T len = Length();
		return{ x / len, y / len };
	}
	Vector2<T> Normal() const {
		return{ -y, x }; // Rotate by 90° counterclockwise
	}

	T Dot(Vector2<T> const& vector) const {
		return x * vector.x + y * vector.y;
	}
	T Cross(Vector2<T> const& vector) const {
		return x * vector.y - y * vector.x;
	}

	operator T*() {
		return (T*)&x;
	}
	operator T const*() const {
		return (T const*)&x;
	}

	friend Vector2<T> operator+(T const& scalar, Vector2<T> const& vector) {
		return{ scalar + vector.x, scalar + vector.y };
	}
	friend Vector2<T> operator-(T const& scalar, Vector2<T> const& vector) {
		return{ scalar - vector.x, scalar - vector.y };
	}
	friend Vector2<T> operator*(T const& scalar, Vector2<T> const& vector) {
		return{ scalar * vector.x, scalar * vector.y };
	}
	friend Vector2<T> operator/(T const& scalar, Vector2<T> const& vector) {
		return{ scalar / vector.x, scalar / vector.y };
	}
	friend std::ostream& operator<<(std::ostream& stream, Vector2<T> const& vector) {
		stream << "[" << vector.x << "," << vector.y << "]";
		return stream;
	}
};

// Typedefs for Vector2<T>

typedef Vector2<float> Vector2_float;
typedef Vector2<double> Vector2_double;

// Vector3<T>

template<typename T>
class Vector3 {

public:

	union {
		struct {
			union {
				T m_1;
				T a;
				T x;
			};
			union {
				T m_2;
				T b;
				T y;
			};
			union {
				T m_3;
				T c;
				T z;
			};
		};
		T m_raw[3];
	};

	Vector3() : Vector3(0, 0, 0) {};
	Vector3(T x, T y, T z) : x(x), y(y), z(z) {}
	Vector3(Vector3<T> const& vector) {
		x = vector.x;
		y = vector.y;
		z = vector.z;
	}
	Vector3(Vector3<T> const&& vector) {
		x = vector.x;
		y = vector.y;
		z = vector.z;
	}

	template<typename U>
	operator Vector3<U>() const {
		return Vector3<U>((U)x, (U)y, (U)z);
	}

	Vector3<T>& operator=(Vector3<T> const& vector) {
		x = vector.x;
		y = vector.y;
		z = vector.z;
		return *this;
	}
	Vector3<T>& operator=(Vector3<T> const&& vector) {
		x = vector.x;
		y = vector.y;
		z = vector.z;
		return *this;
	}

	bool operator==(Vector3<T> const& vector) const {
		return (std::abs(x - vector.x) < std::numeric_limits<T>::epsilon()
			&& std::abs(y - vector.y) < std::numeric_limits<T>::epsilon()
			&& std::abs(z - vector.z) < std::numeric_limits<T>::epsilon());
	}
	bool operator!=(Vector3<T> const& vector) const {
		return (std::abs(x - vector.x) > std::numeric_limits<T>::epsilon()
			|| std::abs(y - vector.y) > std::numeric_limits<T>::epsilon()
			|| std::abs(z - vector.z) > std::numeric_limits<T>::epsilon());
	}

	Vector3<T> operator+(Vector3<T> const& vector) const {
		return{ x + vector.x, y + vector.y, z + vector.z };
	}
	Vector3<T> operator-(Vector3<T> const& vector) const {
		return{ x - vector.x, y - vector.y, z - vector.z };
	}
	Vector3<T> operator*(Vector3<T> const& vector) const {
		return{ x * vector.x, y * vector.y, z * vector.z };
	}
	Vector3<T> operator/(Vector3<T> const& vector) const {
		return{ x / vector.x, y / vector.y, z / vector.z };
	}
	Vector3<T> operator-() const {
		return{ -x, -y, -z };
	}

	Vector3<T>& operator+=(Vector3<T> const& vector) {
		x += vector.x;
		y += vector.y;
		z += vector.z;
		return *this;
	}
	Vector3<T>& operator-=(Vector3<T> const& vector) {
		x -= vector.x;
		y -= vector.y;
		z -= vector.z;
		return *this;
	}
	Vector3<T>& operator*=(Vector3<T> const& vector) {
		x *= vector.x;
		y *= vector.y;
		z *= vector.z;
		return *this;
	}
	Vector3<T>& operator/=(Vector3<T> const& vector) {
		x /= vector.x;
		y /= vector.y;
		z /= vector.z;
		return *this;
	}

	Vector3<T> operator+(T const& scalar) const {
		return{ x + scalar, y + scalar, z + scalar };
	}
	Vector3<T> operator-(T const& scalar) const {
		return{ x - scalar, y - scalar, z - scalar };
	}
	Vector3<T> operator*(T const& scalar) const {
		return{ scalar * x, scalar * y, z * scalar };
	}
	Vector3<T> operator/(T const& scalar) const {
		return{ x / scalar, y / scalar, z / scalar };
	}

	Vector3<T>& operator+=(T const& scalar) {
		x += scalar;
		y += scalar;
		z += scalar;
		return *this;
	}
	Vector3<T>& operator-=(T const& scalar) {
		x -= scalar;
		y -= scalar;
		z -= scalar;
		return *this;
	}
	Vector3<T>& operator*=(T const& scalar) {
		x *= scalar;
		y *= scalar;
		z *= scalar;
		return *this;
	}
	Vector3<T>& operator/=(T const& scalar) {
		x /= scalar;
		y /= scalar;
		z /= scalar;
		return *this;
	}

	T Length() const {
		return (T)std::sqrt(x * x + y * y + z * z);
	}
	T Length2() const {
		return x * x + y * y + z * z;
	}
	Vector3<T> Normalize() const {
		T len = Length();
		return{ x / len, y / len, z / len };
	}

	T Dot(Vector3<T> const& vector) const {
		return x * vector.x + y * vector.y + z * vector.z;
	}
	Vector3<T> Cross(Vector3<T> const& vector) const {
		return{ y * vector.z - z * vector.y,
			z * vector.x - x * vector.z,
			x * vector.y - y * vector.x };
	}

	operator T*() {
		return (T*)&x;
	}
	operator T const*() const {
		return (T const*)&x;
	}

	friend Vector3<T> operator+(T const& scalar, Vector3<T> const& vector) {
		return{ scalar + vector.x, scalar + vector.y, scalar + vector.z };
	}
	friend Vector3<T> operator-(T const& scalar, Vector3<T> const& vector) {
		return{ scalar - vector.x, scalar - vector.y, scalar - vector.z };
	}
	friend Vector3<T> operator*(T const& scalar, Vector3<T> const& vector) {
		return{ scalar * vector.x, scalar * vector.y, scalar * vector.z };
	}
	friend Vector3<T> operator/(T const& scalar, Vector3<T> const& vector) {
		return{ scalar / vector.x, scalar / vector.y, scalar / vector.z };
	}
	friend std::ostream& operator<<(std::ostream& stream, Vector3<T> const& vector) {
		stream << "[" << vector.x << "," << vector.y << "," << vector.z << "]";
		return stream;
	}
};

// Typedefs for Vector3<T>

typedef Vector3<float> Vector3_float;
typedef Vector3<double> Vector3_double;

#endif

/// \brief Vector4<T>
///
/// This vector class can be used both in host and device code
template<typename T>
class Vector4 {

public:

	union {
		struct {
			union {
				T m_0;
				T a;
				T x;
			};
			union {
				T m_1;
				T b;
				T y;
			};
			union {
				T m_2;
				T c;
				T z;
			};
			union {
				T m_3;
				T d;
				T w;
			};
		};
		T m_raw[4];
	};

	CUDA_HOST_DEVICE Vector4() = default;
	CUDA_HOST_DEVICE Vector4(T x, T y, T z, T w) : x(x), y(y), z(z), w(w) {}
	CUDA_HOST_DEVICE Vector4(Vector4<T> const& vector) {
		x = vector.x;
		y = vector.y;
		z = vector.z;
		w = vector.w;
	}
	CUDA_HOST_DEVICE Vector4(Vector4<T> const&& vector) {
		x = vector.x;
		y = vector.y;
		z = vector.z;
		w = vector.w;
	}

	template<typename U>
	CUDA_HOST_DEVICE operator Vector4<U>() const {
		return Vector4<U>((U)x, (U)y, (U)z, (U)w);
	}

	CUDA_HOST_DEVICE Vector4<T>& operator=(Vector4<T> const& vector) {
		x = vector.x;
		y = vector.y;
		z = vector.z;
		w = vector.w;
		return *this;
	}
	CUDA_HOST_DEVICE Vector4<T>& operator=(Vector4<T> const&& vector) {
		x = vector.x;
		y = vector.y;
		z = vector.z;
		w = vector.w;
		return *this;
	}

	bool operator==(Vector4<T> const& vector) const {
		return (std::abs(x - vector.x) < std::numeric_limits<T>::epsilon()
			&& std::abs(y - vector.y) < std::numeric_limits<T>::epsilon()
			&& std::abs(z - vector.z) < std::numeric_limits<T>::epsilon()
			&& std::abs(w - vector.w) < std::numeric_limits<T>::epsilon());
	}
	bool operator!=(Vector4<T> const& vector) const {
		return (std::abs(x - vector.x) > std::numeric_limits<T>::epsilon()
			|| std::abs(y - vector.y) > std::numeric_limits<T>::epsilon()
			|| std::abs(z - vector.z) > std::numeric_limits<T>::epsilon()
			|| std::abs(w - vector.w) > std::numeric_limits<T>::epsilon());
	}

#ifdef USE_CUDA
	__device__ bool CUDAEquals(Vector4<T> const& vector) const {
		return (x == vector.x
			|| y == vector.y
			|| z == vector.z
			|| w == vector.w);
	}
	__device__ bool CUDANotEquals(Vector4<T> const& vector) const {
		return (x != vector.x
			|| y != vector.y
			|| z != vector.z
			|| w != vector.w);
	}
#endif

	CUDA_HOST_DEVICE Vector4<T> operator+(Vector4<T> const& vector) const {
		return{ x + vector.x, y + vector.y, z + vector.z, w + vector.w };
	}
	CUDA_HOST_DEVICE Vector4<T> operator-(Vector4<T> const& vector) const {
		return{ x - vector.x, y - vector.y, z - vector.z, w - vector.w };
	}
	CUDA_HOST_DEVICE Vector4<T> operator*(Vector4<T> const& vector) const {
		return{ x * vector.x, y * vector.y, z * vector.z, w * vector.w };
	}
	CUDA_HOST_DEVICE Vector4<T> operator/(Vector4<T> const& vector) const {
		return{ x / vector.x, y / vector.y, z / vector.z, w / vector.w };
	}
	CUDA_HOST_DEVICE Vector4<T> operator-() const {
		return{ -x, -y, -z, -w };
	}

	CUDA_HOST_DEVICE Vector4<T>& operator+=(Vector4<T> const& vector) {
		x += vector.x;
		y += vector.y;
		z += vector.z;
		w += vector.w;
		return *this;
	}
	CUDA_HOST_DEVICE Vector4<T>& operator-=(Vector4<T> const& vector) {
		x -= vector.x;
		y -= vector.y;
		z -= vector.z;
		w -= vector.w;
		return *this;
	}
	CUDA_HOST_DEVICE Vector4<T>& operator*=(Vector4<T> const& vector) {
		x *= vector.x;
		y *= vector.y;
		z *= vector.z;
		w *= vector.w;
		return *this;
	}
	CUDA_HOST_DEVICE Vector4<T>& operator/=(Vector4<T> const& vector) {
		x /= vector.x;
		y /= vector.y;
		z /= vector.z;
		w /= vector.w;
		return *this;
	}

	CUDA_HOST_DEVICE Vector4<T> operator+(T const& scalar) const {
		return{ x + scalar, y + scalar, z + scalar, w + scalar };
	}
	CUDA_HOST_DEVICE Vector4<T> operator-(T const& scalar) const {
		return{ x - scalar, y - scalar, z - scalar, w - scalar };
	}
	CUDA_HOST_DEVICE Vector4<T> operator*(T const& scalar) const {
		return{ scalar * x, scalar * y, z * scalar, w * scalar };
	}
	CUDA_HOST_DEVICE Vector4<T> operator/(T const& scalar) const {
		return{ x / scalar, y / scalar, z / scalar, w / scalar };
	}

	CUDA_HOST_DEVICE Vector4<T>& operator+=(T const& scalar) {
		x += scalar;
		y += scalar;
		z += scalar;
		w += scalar;
		return *this;
	}
	CUDA_HOST_DEVICE Vector4<T>& operator-=(T const& scalar) {
		x -= scalar;
		y -= scalar;
		z -= scalar;
		w -= scalar;
		return *this;
	}
	CUDA_HOST_DEVICE Vector4<T>& operator*=(T const& scalar) {
		x *= scalar;
		y *= scalar;
		z *= scalar;
		w *= scalar;
		return *this;
	}
	CUDA_HOST_DEVICE Vector4<T>& operator/=(T const& scalar) {
		x /= scalar;
		y /= scalar;
		z /= scalar;
		w /= scalar;
		return *this;
	}

	CUDA_HOST_DEVICE T Length() const {
		return (T)sqrt(x * x + y * y + z * z + w * w);
	}
	CUDA_HOST_DEVICE T LorentzLength2() const {
		return -m_0 * m_0 + m_1 * m_1 + m_2 * m_2 + m_3 * m_3;
	}
	CUDA_HOST_DEVICE T Length2() const {
		return x * x + y * y + z * z + w * w;
	}
	CUDA_HOST_DEVICE Vector4<T> Normalize() const {
		T len = Length();
		return{ x / len, y / len, z / len, w / len };
	}

	CUDA_HOST_DEVICE Vector4<T> CartRotX(T angle) const {
		T s, c;
#ifdef __CUDA_ARCH__
		sincos(angle, &s, &c);
#else
		s = std::sin(angle);
		c = std::cos(angle);
#endif
		return Vector4<T>(
			m_0,
			m_1,
			c * m_2 - s * m_3,
			s * m_2 + c * m_3
		);
	}
	CUDA_HOST_DEVICE Vector4<T> CartRotY(T angle) const {
		T s, c;
#ifdef __CUDA_ARCH__
		sincos(angle, &s, &c);
#else
		s = std::sin(angle);
		c = std::cos(angle);
#endif
		return Vector4<T>(
			m_0,
			c * m_1 + s * m_3,
			m_2,
			-s * m_1 + c * m_3
		);
	}
	CUDA_HOST_DEVICE Vector4<T> CartRotZ(T angle) const {
		T s, c;
#ifdef __CUDA_ARCH__
		sincos(angle, &s, &c);
#else
		s = std::sin(angle);
		c = std::cos(angle);
#endif
		return Vector4<T>(
			m_0,
			c * m_1 - s * m_2,
			s * m_1 + c * m_2,
			m_3
		);
	}

	CUDA_HOST_DEVICE T Dot(Vector4<T> const& vector) const {
		return x * vector.x + y * vector.y + z * vector.z + w * vector.w;
	}

	CUDA_HOST_DEVICE operator T*() {
		return (T*)&x;
	}
	CUDA_HOST_DEVICE operator T const*() const {
		return (T const*)&x;
	}

	CUDA_HOST_DEVICE friend Vector4<T> operator+(T const& scalar, Vector4<T> const& vector) {
		return{ scalar + vector.x, scalar + vector.y, scalar + vector.z, scalar + vector.w };
	}
	CUDA_HOST_DEVICE friend Vector4<T> operator-(T const& scalar, Vector4<T> const& vector) {
		return{ scalar - vector.x, scalar - vector.y, scalar - vector.z, scalar - vector.w };
	}
	CUDA_HOST_DEVICE friend Vector4<T> operator*(T const& scalar, Vector4<T> const& vector) {
		return{ scalar * vector.x, scalar * vector.y, scalar * vector.z, scalar * vector.w };
	}
	CUDA_HOST_DEVICE friend Vector4<T> operator/(T const& scalar, Vector4<T> const& vector) {
		return{ scalar / vector.x, scalar / vector.y, scalar / vector.z, scalar / vector.w };
	}
	friend std::ostream& operator<<(std::ostream& stream, Vector4<T> const& vector) {
		stream << "[" << vector.x << "," << vector.y << "," << vector.z << "," << vector.w << "]";
		return stream;
	}
};

// Typedefs for vector4<T>

typedef Vector4<float> Vector4_float;
typedef Vector4<double> Vector4_double;

}
}

#endif