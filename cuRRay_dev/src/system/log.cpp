#include "system/log.h"

#include "util/util.h"

#include <ctime>
#include <csignal>
#include <sstream>
#include <chrono>
#include <cstdlib>
#include <boost/filesystem.hpp>

using namespace std;
using namespace boost::filesystem;

namespace cuRRay {

Log* this_log = nullptr;

Log::Log(string const& logPath, bool showLogInCout, bool keepLogFile, bool verbose) {
	this_log = this;

	m_showLogInCout = showLogInCout;
	m_keepLogFile = keepLogFile;
	m_verbose = verbose;

	m_logPath = logPath;

	// Signal handler
	signal(SIGABRT, Log::OnAbort);

	// Redirect cout to console on windows
#ifdef _WIN64
	/*if(GetConsoleWindow() != NULL) {
		AllocConsole();
		freopen_s(&p_new_stdout, "CONOUT$", "w", stdout);
		freopen_s(&p_new_stderr, "CONOUT$", "w", stderr);
	}*/
#endif

	// Create log directory
	path logDirectory(m_logPath);
	if(!exists(logDirectory)) {
		if(!create_directories(logDirectory)) {
			// Critical abort
			Crash("Unable to create log folder " + logDirectory.string());
		}
	}

	// Create logfile
	path logfilePath = logDirectory / "log.txt";
	m_logFile.open(logfilePath.c_str(), ios::trunc);
	if(!(m_logFile.is_open() && m_logFile.good())) {
		// Critical abort
		Crash("Unable to create log file " + logfilePath.string());
	}
}

Log::~Log() {
	// Redirect cout on Windows
#ifdef _WIN64
	/*if(p_new_stdout != nullptr) {
		fclose(p_new_stdout);
		fclose(p_new_stderr);
	}*/
#endif

	// Close logfile
	if(m_logFile.is_open()) {
		m_logFile.close();
	}

	// Rename logfile if it should be kept
	if(m_keepLogFile) {
		path logFilePath = path(m_logPath) / "log.txt";
		path newLogFilePath = path(m_logPath) / (string("log-") + Timestamp() + string(".txt"));
		rename(logFilePath, newLogFilePath);
	}
}

const char* Log::HLINE = "--------------------------------------------------------------";

string Log::Timestamp() {
	ostringstream oss;

	chrono::system_clock::time_point now = std::chrono::system_clock::now();
	chrono::system_clock::duration tp = now.time_since_epoch();
	tp -= chrono::duration_cast<std::chrono::seconds>(tp);

	time_t now_t = chrono::system_clock::to_time_t(now);
	tm now_tm;
#ifdef _WIN64
	localtime_s(&now_tm, &now_t);
#else
	localtime_r(&now_t, &now_tm);
#endif

	oss << now_tm.tm_year + 1900 << "-" << now_tm.tm_mon + 1 << "-" << now_tm.tm_mday << "-" << now_tm.tm_hour << "_" << now_tm.tm_min << "_" << now_tm.tm_sec << std::flush;

	return oss.str();
}

void Log::Crash(string const& reason) {
	stringstream error;

	error << endl;
	error << HLINE << endl;
	error << HLINE << endl << endl;

	error << "Error Occured !" << endl << endl;

	error << reason << endl;
	
	error << HLINE << endl;
	error << HLINE << endl;

	if(!(m_logFile.is_open() && m_logFile.good())) {
#ifdef _WIN64
		MessageBox(NULL, (wstring(L"No logfile available!\n\n") + Util::stringConverter.from_bytes(error.str())).c_str(), L"An error occured", MB_ICONERROR);
#endif
	} else {
		*this <= error.str() <= endl;
	}

	abort();
}

void Log::HandleError() {
	// Print error message
	if(!(m_logFile.is_open() && m_logFile.good())) {
#ifdef _WIN64
		MessageBox(NULL, (wstring(L"No logfile available!\n\n") + L"cuRRay2 terminated unexpectedly.").c_str(), L"An error occured", MB_ICONERROR);
#endif
	} else {
		*this << "cuRRay2 terminated unexpectedly." << endl;
		m_logFile.close();
	}

	// Rename logfile
	path logFilePath = path(m_logPath) / "log.txt";
	if(exists(logFilePath)) { // Keep log because it's a crash
		path newLogFilePath = path(m_logPath) / (string("crash_dump-") + Timestamp() + string(".txt"));
		rename(logFilePath, newLogFilePath);
	}
}

void Log::OnAbort(int signal) {
	if(signal == SIGABRT) {
		if(this_log != nullptr) {
			this_log->HandleError();
		}
		_exit(ReturnValue::SIG_ABORT); // Return sigabrt error code
	} else {
		_exit(ReturnValue::SIG_UNKNOWN); // Return general error code
	}
}

}
