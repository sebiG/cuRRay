//
// This file is part of cuRRay.
//
// (c) 2018 Sébastien Garmier (sebastien.garmier@gmail.com)
//

#ifndef PROGRAM_H
#define PROGRAM_H

#include "stdafx.h"

#include <tclap/CmdLine.h>

#include <string>

#include "system/log.h"
#include "kerr_newman/scene.h"
#include "kerr_newman/frameRenderer.h"
#ifdef USE_CUDA
#include "gpu/gpuManager.h"
#endif

/// \breif cuRRay namespace
namespace cuRRay {

/// \brief Main class
class Program {

public:

	enum ReturnValues {
		SUCCESS = 0,
		FAIL = 1
	};
	
	/// \brief Default config file
	const static std::string DEFAULT_CONFIG_FILE; // config.yaml
	
	/// \brief Constructor
	Program();
	/// \brief Destructor
	~Program() = default;

#ifdef USE_CUDA
	/// \brief The cuRRay banner
	const std::string BANNER = R"(
                         ____     ____
          _______  __   / __ \   / __ \____ ___  __
         / ___/ / / /  / /_/ /  / /_/ / __ `/ / / /
        / /__/ /_/ /  / _, _/  / _, _/ /_/ / /_/ /
        \___/\__,_/  /_/ |_|  /_/ |_|\__,_/\__, /
                                          /____/
         CUDA(R)  Relativistic     Raytracer
                  Version  2.0

                       ,
             (c) 2018 Sebastien Garmier
)";
#else
	/// \brief The cuRRay cpu banner
	const std::string BANNER = R"(
                         ____     ____
          _______  __   / __ \   / __ \____ ___  __
         / ___/ / / /  / /_/ /  / /_/ / __ `/ / / /
        / /__/ /_/ /  / _, _/  / _, _/ /_/ / /_/ /
        \___/\__,_/  /_/ |_|  /_/ |_|\__,_/\__, /
                                          /____/
         CUDA(R)  Relativistic     Raytracer
              Version 2.0, CPU only

                       ,
             (c) 2018 Sebastien Garmier
)";
#endif

	/// \brief Runs the program
	///
	/// \param argc Argument count
	/// \param argv Arguments
	int Run(int argc, char** argv);

private:

	/// \brief Initializes default settings
	void InitializeDefaultSettings();
	/// \brief Parses the command line
	void ParseCommandLine(int argc, char** argv);
	/// \brief Loads configuration
	void LoadConfig();
	/// \brief Applies the all settings (except GPU settings, which are applied directly)
	void ApplySettings();

	/// \brief Output wrapper for TCLAP
	class MyOutput : public TCLAP::StdOutput {
	public:

		/// \brief Constructor
		MyOutput(Program* program, Log& log);

		/// \brief Prints help
		virtual void usage(TCLAP::CmdLineInterface& c);
		/// \brief Prints error
		virtual void failure(TCLAP::CmdLineInterface& c, TCLAP::ArgException& e) override;

	private:

		Program* o_program;
		Log& r_log;
	};

	Log m_log;
#ifdef USE_CUDA
	GPUManager m_gpuManager;
#endif

	KerrNewman::Scene m_scene;

	/// \brief Settings
	struct {
		template<typename T>
		struct Value {
			bool m_set;
			T m_val;
		};

		Value<std::string> m_sceneFile;
		Value<std::string> m_configFile;
		Value<std::string> m_outputDirectory;
		Value<std::string> m_outputType;

		Value<uint32_t> m_xRes;
		Value<uint32_t> m_yRes;
		Value<uint32_t> m_frames;
		Value<uint32_t> m_cpuThreadCount;

		Value<bool> m_info;
		Value<bool> m_verbose;
		Value<bool> m_quiet;
		Value<bool> m_autoAccept;

		Value<double> m_raytracerStepSizeMultiplier;
		Value<double> m_raytracerHorizonEpsilon;
	} m_settings;
};

}

#endif