//
// This file is part of cuRRay.
//
// (c) 2018 S?bastien Garmier (sebastien.garmier@gmail.com)
//

#include "system/program.h"
#include "util/util.h"

#include "kerr_newman/raytracer.h"

#include <yaml-cpp/yaml.h>
#include <boost/filesystem.hpp>

using namespace std;
using namespace boost;
using namespace boost::filesystem;

namespace cuRRay {

//----------------------------------------------------------------------
// Program::MyOutput
//----------------------------------------------------------------------

Program::MyOutput::MyOutput(Program* program, Log& log) :
	o_program(program),
	r_log(log)
{

}

void Program::MyOutput::usage(TCLAP::CmdLineInterface& c) {
	r_log <= endl;

	// Print banner
	r_log <= Log::HLINE <= endl;
	r_log <= o_program->BANNER <= endl;
	r_log <= Log::HLINE <= endl;

	r_log <= endl;

	// Call base
	TCLAP::StdOutput::usage(c);
}

void Program::MyOutput::failure(TCLAP::CmdLineInterface& c, TCLAP::ArgException& e) {
	r_log <= endl;

	// Print banner
	r_log <= Log::HLINE <= endl;
	r_log <= o_program->BANNER <= endl;
	r_log <= Log::HLINE <= endl;

	r_log <= endl;

	// Call base
	TCLAP::StdOutput::failure(c, e);
}

//----------------------------------------------------------------------
// Program
//----------------------------------------------------------------------

const string Program::DEFAULT_CONFIG_FILE = "config.yml";

Program::Program() :
	m_log("log"),
#ifdef USE_CUDA
	m_gpuManager(m_log),
#endif
	
	m_scene(m_log)
{
	// Set raytracer log
	KerrNewman::Raytracer::Init(&m_log);
}

int Program::Run(int argc, char** argv) {

	try {

		// Initialize default settings
		InitializeDefaultSettings();

		// Parse command line
		ParseCommandLine(argc, argv);

#ifdef USE_CUDA
		// Init GPU manager
		m_gpuManager.Init(3, 0);
#endif
		
		// Load config
		LoadConfig();

		// Apply settings
		ApplySettings();

		// Print banner
		m_log <= endl;
		m_log <= Log::HLINE <= endl;
		m_log <= BANNER <= endl;
		m_log <= Log::HLINE <= endl;
		m_log <= endl;

#ifdef USE_CUDA
		// Print GPU info if needed
		if(m_settings.m_info.m_val) {
			m_gpuManager.PrintGPUInformation();
		}
#endif

		// Check if scene file was specified
		if(!m_settings.m_sceneFile.m_val.empty()) {

			// Load scene
			if(!m_scene.LoadSceneFile(m_settings.m_sceneFile.m_val)) {
				return ReturnValues::FAIL;
			}

			// Warnings
			if(!m_settings.m_outputDirectory.m_set) {
				m_log <= "Warning: -o (--output_dir) was not set." <= endl;
				m_log <= "No frames will be rendered." <= endl <= endl;
				m_log <= Log::HLINE <= endl <= endl;
			}
			if(m_settings.m_frames.m_set && m_settings.m_frames.m_val > 0) {
				if(!(m_settings.m_xRes.m_set && m_settings.m_yRes.m_set)) {
					m_log <= "Warning: -f (--frames) was set, but -x (--x_res) and/or" <= endl;
					m_log <= "-y (--y_res) were not. No frames will be rendered." <= endl <= endl;
					m_log <= Log::HLINE <= endl <= endl;
				}
			} else {
				if(m_settings.m_xRes.m_set ^ m_settings.m_yRes.m_set) {
					m_log <= "Warning: only one of either -x (--x_res) or -y (--y_res)" <= endl;
					m_log <= "was set. No frames will be rendered." <= endl <= endl;
					m_log <= Log::HLINE <= endl <= endl;
				}
			}

			// Configure scene
			m_scene.ConfigureFromCmd(
				m_settings.m_outputDirectory.m_val,
				m_settings.m_outputType.m_val,
				m_settings.m_frames.m_val,
				m_settings.m_xRes.m_val,
				m_settings.m_yRes.m_val
			);

			// Ask user for approval
			if(!m_settings.m_autoAccept.m_val) {
				if(!m_scene.RequestUserApproval()) {
					return ReturnValues::SUCCESS;
				}
			}
			m_log <= Log::HLINE <= endl <= endl;

			// Create output directory
			if(!exists(m_settings.m_outputDirectory.m_val)) {
				if(!create_directories(m_settings.m_outputDirectory.m_val)) {
					throw runtime_error("Unable to create output directory '" + m_settings.m_outputDirectory.m_val + "'.");
				}
			}

			// Output scene information
			if(m_scene.m_outputSceneInformation) {
				m_scene.OutputSceneInformation(m_settings.m_outputDirectory.m_val + "/info.txt");
			}

			// Run frame renderer if needed
			if(m_scene.m_renderFrames) {
#ifdef USE_CUDA
				KerrNewman::FrameRenderer frameRenderer(
					m_log,
					m_scene,
					path(m_settings.m_outputDirectory.m_val).remove_trailing_separator().string(),
					m_gpuManager
				);
				frameRenderer.RunCUDA();
#else
				KerrNewman::FrameRenderer frameRenderer(
					m_log,
					m_scene,
					path(m_settings.m_outputDirectory.m_val).remove_trailing_separator().string()
				);
				frameRenderer.RunCPU(m_settings.m_cpuThreadCount.m_val);
#endif
			}

		} else {
			m_log <= "No scene file specified, no raytracing will be performed." <= endl;
		}

	} catch(runtime_error const& e) {
		m_log.Crash("Uncaught std::runtime_error: " + string(e.what()));
	}

	// Return success
	return ReturnValues::SUCCESS;

}

void Program::InitializeDefaultSettings() {
	// Initialize cmd settings
	m_settings.m_sceneFile = { false, "" };
	m_settings.m_configFile = { false, "" };
	m_settings.m_outputDirectory = { false, "" };
	m_settings.m_outputType = { false, "icrm" };

	m_settings.m_xRes = { false, 0 };
	m_settings.m_yRes = { false, 0 };
	m_settings.m_frames = { false, 1 };
	m_settings.m_cpuThreadCount = { false, 4 };

	m_settings.m_info = { false, false };
	m_settings.m_verbose = { false, false };
	m_settings.m_quiet = { false, false };
	m_settings.m_autoAccept = { false, false };

	m_settings.m_raytracerHorizonEpsilon = { false, 0.5E-2 };
	m_settings.m_raytracerStepSizeMultiplier = { false, 1.0 / 32.0 };

	// Initialize log
	m_log.m_verbose = false;
	m_log.m_showLogInCout = true;
	m_log.m_keepLogFile = false;

	// Initialize raytracer (this is rather redundant)
	KerrNewman::Raytracer::m_horizonEpsilon = 0.5E-2;
	KerrNewman::Raytracer::m_stepSizeMultiplier = 1.0 / 32.0;
}

void Program::ParseCommandLine(int argc, char** argv) {

	try {

		TCLAP::CmdLine cmd("", ' ', "2.0");

		TCLAP::ValueArg<std::string> sceneFileArg(
			"s",
			"scene_file",
			"The scene configuration file.",
			false,
			"",
			"scene file"
			);
		TCLAP::ValueArg<std::string> configFileArg(
			"c",
			"config",
			"The cuRRay configuration file. Uses " + DEFAULT_CONFIG_FILE + " by default.",
			false,
			"",
			"configuration file"
			);
		TCLAP::ValueArg<std::string> outputArg(
			"o",
			"output_dir",
			"The output directory.",
			false,
			"",
			"output directory"
			);
		TCLAP::ValueArg<std::string> outputTypeArg(
			"t",
			"output_type",
			"The output type (can contain 'i', 'c', 'r', and 'd').",
			false,
			"",
			"output type"
			);

		TCLAP::ValueArg<uint32_t> xArg(
			"x",
			"x_res",
			"The x resolution of the frames. Must be larger than zero.",
			false,
			0,
			"width"
			);
		TCLAP::ValueArg<uint32_t> yArg(
			"y",
			"y_res",
			"The y resolution of the frames. Must be larger than zero.",
			false,
			0,
			"height"
			);
		TCLAP::ValueArg<uint32_t> framesArg(
			"f",
			"frames",
			"The amount of frames to render.",
			false,
			1,
			"frames count"
			);

#ifdef USE_CUDA
		TCLAP::SwitchArg infoArg(
			"i",
			"info",
			"Shows information about installed CUDA GPUs.",
			false
			);
#endif

		TCLAP::ValueArg<bool> verboseArg(
			"v",
			"verbose",
			"Enables or disables verbose logging. This overrides the setting found in the config file.",
			false,
			false,
			"true/false"
			);
		TCLAP::ValueArg<bool> quietArg(
			"q",
			"quiet",
			"Enables or disables log output to the console. This overrides the setting found in the config file.",
			false,
			false,
			"true/false"
			);
		TCLAP::ValueArg<bool> autoAcceptArg(
			"a",
			"auto_accept",
			"Automatically accepts user confirmation and immediately runs the raytracer.",
			false,
			false,
			"true/false"
			);

		// Create command line
		MyOutput myOutput(this, m_log);
		cmd.setOutput(&myOutput);

		cmd.add(autoAcceptArg);
		cmd.add(quietArg);
		cmd.add(verboseArg);
#ifdef USE_CUDA
		cmd.add(infoArg);
#endif
		cmd.add(outputTypeArg);
		cmd.add(configFileArg);
		cmd.add(framesArg);
		cmd.add(yArg);
		cmd.add(xArg);
		cmd.add(outputArg);
		cmd.add(sceneFileArg);

		// Parse command line
		cmd.parse(argc, argv);

		try {
			// Fetch command line values and configure cmd settings
			if(sceneFileArg.isSet()) {
				m_settings.m_sceneFile = { true, sceneFileArg.getValue() };
			}
			if(configFileArg.isSet()) {
				m_settings.m_configFile = { true, configFileArg.getValue() };
			}
			if(outputArg.isSet()) {
				m_settings.m_outputDirectory = { true, outputArg.getValue() };
			}
			if(outputTypeArg.isSet()) {
				m_settings.m_outputType = { true, outputTypeArg.getValue() };
				for(char c : m_settings.m_outputType.m_val) {
					if(string("icrd").find(c) == string::npos) {
						throw TCLAP::ArgParseException(outputTypeArg.toString() + " must only contain 'i', 'c', 'r', and 'd'.", outputTypeArg.toString());
					}
				}
			}
			if(xArg.isSet()) {
				m_settings.m_xRes = { true, xArg.getValue() };
				if(m_settings.m_xRes.m_val == 0) {
					throw TCLAP::ArgParseException(xArg.toString() + " must be larger than zero.", xArg.toString());
				}
			}
			if(yArg.isSet()) {
				m_settings.m_yRes = { true, yArg.getValue() };
				if(m_settings.m_yRes.m_val == 0) {
					throw TCLAP::ArgParseException(yArg.toString() + " must be larger than zero.", yArg.toString());
				}
			}
			if(framesArg.isSet()) {
				m_settings.m_frames = { true, framesArg.getValue() };
			}
#ifdef USE_CUDA
			if(infoArg.isSet()) {
				m_settings.m_info = { true, infoArg.getValue() };
			}
#endif
			if(verboseArg.isSet()) {
				m_settings.m_verbose = { true, verboseArg.getValue() };
			}
			if(quietArg.isSet()) {
				m_settings.m_quiet = { true, quietArg.getValue() };
			}
			if(autoAcceptArg.isSet()) {
				m_settings.m_autoAccept = { true, autoAcceptArg.getValue() };
			}
		} catch(TCLAP::ArgParseException& e) {
			try {
				// Catch custom exceptions
				myOutput.failure(cmd, e);
			} catch(TCLAP::ExitException& ee) {
				exit(ee.getExitStatus());
			}
		}

	} catch(TCLAP::ArgException const& e) {
		// This should not happen
		m_log.Crash("TCLAP::ArgException: " + e.error() + " for argument " + e.argId());
	}

}

void Program::LoadConfig() {

	YAML::Node config;

	// If config was specified in command line and exists, try loading from there
	if(m_settings.m_configFile.m_set) {
		try {
			config = YAML::LoadFile(m_settings.m_configFile.m_val);
			goto loaded;
		} catch(YAML::ParserException const& e) {
			m_log <= "Could not parse config file " <= m_settings.m_configFile.m_val <= ":" <= endl;
			m_log <= "  " <= Util::PrintYAMLException(e.msg, e.mark) <= endl;
			m_log <= "  Falling back to default config file (" <= DEFAULT_CONFIG_FILE <= ")." <= endl;
		} catch(YAML::BadFile const&) {
			m_log <= "Could not load config file " <= m_settings.m_configFile.m_val <= "." <= endl;
			m_log <= "  Falling back to default config file (" <= DEFAULT_CONFIG_FILE <= ")." <= endl;;
		}
	}

	// Else load default file
	try {
		config = YAML::LoadFile(DEFAULT_CONFIG_FILE);
		goto loaded;
	} catch(YAML::ParserException const& e) {
		m_log <= "Could not parse default config file " <= DEFAULT_CONFIG_FILE <= ":" <= endl;
		m_log <= "  " <= Util::PrintYAMLException(e.msg, e.mark) <= endl;
	} catch(YAML::BadFile const&) {
		m_log <= "Could not load default config file " <= DEFAULT_CONFIG_FILE <= "." <= endl;
	}

	// If no config was loaded thus far, exit
	goto loadNone;

loaded:
	// Config was loaded, parse
	{
		try {
#ifdef USE_CUDA
			// Load GPU settings
			//
			// These settings are directly applied to the GPU manager
			if(config["gpus"]) {
				YAML::Node const& gpus = config["gpus"];
				if(gpus.IsMap()) {
					for(auto& gpu : gpus) {
						int id = gpu.first.as<int>();

						// Check whether gpu exists
						if(id == m_gpuManager.GetGPUCount()) {
							m_log <= Util::PrintYAMLException(
								"Config file: There is no GPU with id " + Util::ToString(id) +" (see cuRRay -i).",
								gpu.first.Mark()
							) <= endl;
							continue;
						}

						YAML::Node const& gpuSettings = gpu.second;

						// Enable flag
						if(gpuSettings["enabled"]) {
							if(gpuSettings["enabled"].as<bool>()) {
								m_gpuManager.EnableGPU(id);
							}
						}
					}
				} else {
					m_log <= Util::PrintYAMLException("Config file: 'gpus' must be a map.", gpus.Mark()) <= endl;
				}
			}
#endif

			// Load CPU settings
			if(config["cpu"]) {
				YAML::Node const& cpu = config["cpu"];
				if(cpu.IsMap()) {
					
					// Thread count
					if(cpu["threads"]) {
						if(!m_settings.m_cpuThreadCount.m_set) {
							m_settings.m_cpuThreadCount = { true, cpu["threads"].as<uint32_t>() };
						}
					}

				} else {
					m_log <= Util::PrintYAMLException("Config file: 'cpu' must be a map.", cpu.Mark()) <= endl;
				}
			}

			// Load log settings
			if(config["log"]) {
				YAML::Node const& log = config["log"];
				if(log.IsMap()) {
					
					// Verbose flag
					if(log["verbose"]) {
						if(!m_settings.m_verbose.m_set) {
							m_settings.m_verbose = { true, log["verbose"].as<bool>() };
						}
					}

					// Quiet flag
					if(log["quiet"]) {
						if(!m_settings.m_quiet.m_set) {
							m_settings.m_quiet = { true, log["quiet"].as<bool>() };
						}
					}

					// Keep logfile
					if(log["keep_log"]) {
						m_log.m_keepLogFile = log["keep_log"].as<bool>();
					}

				} else {
					m_log <= Util::PrintYAMLException("Config file: 'log' must be a map.", log.Mark()) <= endl;
				}
			}

			// Load raytracer settings
			if(config["raytracer"]) {
				YAML::Node const& raytracer = config["raytracer"];
				if(raytracer.IsMap()) {

					// Horizon epsilon
					if(raytracer["horizon_epsilon"]) {
						if(!m_settings.m_raytracerHorizonEpsilon.m_set) {
							m_settings.m_raytracerHorizonEpsilon = { true, raytracer["horizon_epsilon"].as<double>() };
						}
					}

					// Step size multiplier
					if(raytracer["step_multiplier"]) {
						if(!m_settings.m_raytracerStepSizeMultiplier.m_set) {
							m_settings.m_raytracerStepSizeMultiplier = { true, raytracer["step_multiplier"].as<double>() };
						}
					}
					
				} else {
					m_log <= Util::PrintYAMLException("Config file: 'raytracer' must be a map.", raytracer.Mark()) <= endl;
				}
			}

			// Load various settings
			{
				// Tells whether user confirmation is automatically accepted
				if(config["auto_accept"]) {
					if(!m_settings.m_autoAccept.m_set) {
						m_settings.m_autoAccept = { true, config["auto_accept"].as<bool>() };
					}
				}
			}

		} catch(YAML::RepresentationException const& e) {
			m_log <= "Unexpected YAML in config file:" <= endl;
			m_log <= "  " <= Util::PrintYAMLException(e.msg, e.mark) <= endl;
			m_log <= "  Parsing failed." <= endl;
		}
	}

	// Return
	return;

loadNone:
	m_log <= "No config file loaded." <= endl;
}

void Program::ApplySettings() {
	// Configure log
	m_log.m_verbose = m_settings.m_verbose.m_val;
	m_log.m_showLogInCout = !m_settings.m_quiet.m_val;

	// Configure raytracer
	KerrNewman::Raytracer::m_horizonEpsilon = m_settings.m_raytracerHorizonEpsilon.m_val;
	KerrNewman::Raytracer::m_stepSizeMultiplier = m_settings.m_raytracerStepSizeMultiplier.m_val;
}

}
