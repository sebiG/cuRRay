//
// This file is part of cuRRay.
//
// (c) 2018 Sébastien Garmier (sebastien.garmier@gmail.com)
//

#ifndef LOG_H
#define LOG_H

#include "stdafx.h"

#include <iostream>
#include <fstream>
#include <string>
#include <thread>
#include <map>

namespace cuRRay {

/// \brief Logging class
class Log {

public:

	enum ReturnValue {
		SIG_ABORT = -1,
		SIG_UNKNOWN = -2
	};

	/// \brief Constructor
	Log(std::string const& logPath, bool showLogInCout = true, bool keepLogFile = false, bool verbose = false);
	/// \brief Destructor
	~Log();

	/// \brief A horizontal line made of minus signs
	const static char* HLINE;

	/// \brief Enables or disables output to <tt>std::cout</tt>
	bool m_showLogInCout;
	/// \brief Controls whether logfiles should be kept around
	/// Crash logs are always saved
	bool m_keepLogFile;
	/// \brief Enables verbose log output
	bool m_verbose;
	/// \brief Get log folder path
	std::string GetLogPath() const { return m_logPath; }
	/// \brief Get current log file path
	std::string GetLogFilePath() const { return m_logFilePath; }

	/// \brief Create timestamp
	std::string Timestamp();

	/// \brief Allows custom prefixes for all outputs from specific threads
	std::map<std::thread::id, std::string> m_threadPrefixes;

	/// \brief Returns prefix for current thread
	std::string ThreadPrefix() {
		auto id = std::this_thread::get_id();
		if(m_threadPrefixes.find(id) != m_threadPrefixes.end()) {
			return "[" + m_threadPrefixes[id] + "] ";
		}
		return "";
	}

	/// \brief Outputs obj to log file and/or to std::cout
	template<typename T>
	Log& operator<< (T const& obj) {
		if(m_verbose) {
			if(m_showLogInCout) std::cout << obj;
			m_logFile << obj;
		}
		return *this;
	}
	/// \brief Outputs modifier pfun to log file and/or std::cout
	Log& operator<< (std::ostream& (*pfun)(std::ostream&)) {
		if(m_verbose) {
			if(m_showLogInCout) pfun(std::cout);
			pfun(m_logFile);
		}
		return *this;
	}

	/// \brief Forces output of obj to log file and/or to std::cout
	template<typename T>
	Log& operator<= (T const& obj) {
		if(m_showLogInCout) std::cout << obj;
		m_logFile << obj;
		return *this;
	}
	/// \brief Forces output of obj to log file and/or std::cout
	Log& operator<= (std::ostream& (*pfun)(std::ostream&)) {
		if(m_showLogInCout) pfun(std::cout);
		pfun(m_logFile);
		return *this;
	}

	/// \brief Ends the program and creates a crash report
	///
	/// \param reason The crash reason
	void Crash(std::string const& reason);

private:

	void HandleError();
	static void OnAbort(int signal);

	static Log* instance;

	std::string m_logPath;
	std::string m_logFilePath;
	std::ofstream m_logFile;

#ifdef _WIN64
	FILE* p_new_stdout;
	FILE* p_new_stderr;
#endif

};

extern Log* this_log;

}

#endif