//
// This file is part of cuRRay.
//
// (c) 2018 Sébastien Garmier (sebastien.garmier@gmail.com)
//

#ifndef STDAFX_H
#define STDAFX_H

// Use only vector 4 (makes compilation faster)
#define USE_ONLY_VECTOR4



#if USE_CUDA
// CUDA
#include <cuda_runtime_api.h>

// Function attribute
#define CUDA_HOST_DEVICE __host__ __device__

#else

// Empty function attribute
#define CUDA_HOST_DEVICE

#endif






// Windows
#ifdef _WIN64
#define UNICODE
#define NOMINMAX
#include <Windows.h>
#endif

// C++ standard
#include <cstdint>

#endif