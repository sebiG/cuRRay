//
// This file is part of cuRRay.
//
// (c) 2018 S?bastien Garmier (sebastien.garmier@gmail.com)
//

#include "gpu/gpuManager.h"
#include "util/util.h"
#include "system/log.h"

#include <cmath>
#include <stdexcept>
#include <algorithm>
#include <functional>

using namespace std;

namespace cuRRay {

GPUManager::GPUManager(Log& log) :
	r_log(log)
{

}

void GPUManager::Init(int requiredComputeMajor, int requiredComputeMinor) {

	m_requiredComputeMajor = requiredComputeMajor;
	m_requiredComputeMinor = requiredComputeMinor;

	// CUDA status
	cudaError status;

	// Get driver version
	status = cudaDriverGetVersion(&m_driverVersion);
	if(status) throw runtime_error(Util::CUDAError(status, "cudaDriverGetVersion", DebugCodeLocation));

	// Get device count
	status = cudaGetDeviceCount(&m_gpuCount);
	if(status) throw runtime_error(Util::CUDAError(status, "cudaGetDeviceCount", DebugCodeLocation));
	if(m_gpuCount <= 0) {
		r_log <= "WARNING: No CUDA device detected." <= endl;
	}
	m_gpuInformation.resize(m_gpuCount);

	// Fetch GPU information
	for(int i = 0; i < m_gpuCount; i++) {
		GPUInformation gpuInformation;

		gpuInformation.m_id = i;

		status = cudaGetDeviceProperties(&gpuInformation.m_deviceProperties, i);
		if(status) throw runtime_error(Util::CUDAError(status, "cudaGetDeviceProperties", DebugCodeLocation));

		// Check if device has a high enough compute capability
		gpuInformation.m_canBeUsed = true;
		if(gpuInformation.m_deviceProperties.major < m_requiredComputeMajor) {
			gpuInformation.m_canBeUsed = false;
		} else if(gpuInformation.m_deviceProperties.major == m_requiredComputeMajor) {
			if(gpuInformation.m_deviceProperties.minor < m_requiredComputeMinor) {
				gpuInformation.m_canBeUsed = false;
			}
		}

		// Print warning if TDR is enabled
		if(gpuInformation.m_deviceProperties.kernelExecTimeoutEnabled) {
			r_log <= "WARNING: GPU #" <= i <= " has kernel execution timeout enabled. Cosnider turning it off as this will probably cause problems." <= endl;
		}

		// Add GPU info
		m_gpuInformation[i] = gpuInformation;
	}

	// Create GPUs
	for(auto& i : m_gpuInformation) {
		if(i.m_canBeUsed) {
			// Add enable flag
			m_gpuEnabled.insert({ i.m_id, false });
		}
	}
}

void GPUManager::PrintGPUInformation() {

	r_log <= "CUDA GPU information:" <= endl <= endl;

	r_log <= "Driver version: " <= m_driverVersion <= endl;
	r_log <= "Required compute capability: " <= m_requiredComputeMajor <= "." <= m_requiredComputeMinor <= endl <= endl;
	
	for(auto const& i : m_gpuInformation) {
		r_log <= "Device #" <= i.m_id <= endl <= endl;
		r_log <= "Can be used:                      " <= ((i.m_canBeUsed) ? "Yes" : "No") <= endl;

		r_log <= endl;

		cudaDeviceProp const& prop = i.m_deviceProperties;

		r_log <= "Name:                             " <= prop.name <= endl;
		r_log <= "Compute capability:               " <= prop.major <= "." <= prop.minor <= endl;
		r_log <= "Clock rate:                       " <= prop.clockRate <= endl;
		r_log <= "Device copy overlap:              " <= ((prop.deviceOverlap) ? "enabled" : "disabled") <= endl;
		r_log <= "Kernel execution timeout:         " <= ((prop.kernelExecTimeoutEnabled) ? "enabled" : "disabled") <= endl;
		r_log <= "Total global memory:              " <= prop.totalGlobalMem <= endl;
		r_log <= "Total const. memory:              " <= prop.totalConstMem <= endl;
		r_log <= "Max. memory pitch:                " <= prop.memPitch <= endl;
		r_log <= "Texure alignment:                 " <= prop.textureAlignment <= endl;

		r_log <= endl;

		r_log <= "Multiprocessor count:             " <= prop.multiProcessorCount <= endl;
		r_log <= "Max. threads per multiprocessor:  " <= prop.maxThreadsPerMultiProcessor <= endl;
		r_log <= "Shared memory per multiprocessor: " <= prop.sharedMemPerBlock <= endl;
		r_log <= "Registers per multiprocessor:     " <= prop.regsPerBlock <= endl;
		r_log <= "Threads per warp:                 " <= prop.warpSize <= endl;
		r_log <= "Max. threads per block:           " <= prop.maxThreadsPerBlock <= endl;
		r_log <= "Max. block dimensions:            ("
			<= prop.maxThreadsDim[0] <= ","
			<= prop.maxThreadsDim[1] <= ","
			<= prop.maxThreadsDim[2] <= ")" <= endl;
		r_log <= "Max. grid dimensions:             ("
			<= prop.maxGridSize[0] <= ","
			<= prop.maxGridSize[1] <= ","
			<= prop.maxGridSize[2] <= ")" <= endl;
		r_log <= endl;

		r_log <= Log::HLINE <= endl <= endl;
	}
}

GPUInformation const& GPUManager::GetGPUInformation(int id) {
	if(id < 0 || id >= m_gpuCount) {
		throw runtime_error("At: " DebugCodeLocation ": There is no GPU with id " + Util::ToString(id) + ".");
	}
	return m_gpuInformation[id];
}

bool GPUManager::IsGPUEnabled(int id) {
	if(m_gpuEnabled.find(id) == m_gpuEnabled.end()) {
		throw runtime_error("At: " DebugCodeLocation ": There is no usable GPU with id " + Util::ToString(id) + ".");
	}
	return m_gpuEnabled.at(id);
}

void GPUManager::EnableGPU(int id) {
	if(m_gpuEnabled.find(id) == m_gpuEnabled.end()) {
		throw runtime_error("At: " DebugCodeLocation ": There is no usable GPU with id " + Util::ToString(id) + ".");
	}

	// If already enabled, return
	if(m_gpuEnabled.at(id)) return;

	// Enable
	m_gpuEnabled.at(id) = true;
}

void GPUManager::DisableGPU(int id) {
	if(m_gpuEnabled.find(id) != m_gpuEnabled.end()) {
		throw runtime_error("At: " DebugCodeLocation ": There is no usable GPU with id " + Util::ToString(id) + ".");
	}

	// If already disabled, return
	if(!m_gpuEnabled.at(id)) return;

	// Disable
	m_gpuEnabled.at(id) = false;
}

vector<int> GPUManager::GetEnabledGPUs() {
	vector<int> returnValue;

	for(auto& i : m_gpuEnabled) {
		if(i.second) {
			returnValue.push_back(i.first);
		}
	}

	return returnValue;
}

void GPUManager::ComputeLaunchParameters(
	int id,
	uint32_t threadCount,
	uint32_t registersPerThread,
	dim3* blockSize,
	dim3* gridSize
) {

	// Make sure to print avery line in one go
	stringstream ss;

	// Log
	if(r_log.m_verbose) {
		ss << r_log.ThreadPrefix() << "Calculating launch metrics..." << endl << endl;
		ss << "thread count: " << threadCount << endl;
		ss << "registers per thread: " << registersPerThread << endl;
	}

	// This holds for all devices where compute >= 3.0
	const uint32_t warpAllocGranularity = 4;
	const uint32_t regAllocGranularity = 256;

	cudaDeviceProp const& prop = GetGPUInformation(id).m_deviceProperties;

	// Registers consumed by warp
	uint32_t registersPerWarp = registersPerThread * prop.warpSize;
	registersPerWarp = (uint32_t)ceilf((registersPerWarp) / (float)regAllocGranularity) * regAllocGranularity;

	// Max warps per SM
	// The goal is to get there
	uint32_t maxWarpsPerSM = min(
		(uint32_t)prop.regsPerMultiprocessor / registersPerWarp,
		(uint32_t)(prop.maxThreadsPerMultiProcessor / prop.warpSize)
	);
	maxWarpsPerSM -= maxWarpsPerSM % warpAllocGranularity;

	// Maximum logical warps per block
	uint32_t maxLogicalWarpsPerBlock = min(
		min(
			(uint32_t)(prop.regsPerBlock / registersPerWarp),
			(uint32_t)(prop.maxThreadsPerBlock / prop.warpSize)
		),
		BLOCK_SIZE_HARD_LIMIT / prop.warpSize
	);
	maxLogicalWarpsPerBlock -= maxLogicalWarpsPerBlock % warpAllocGranularity;

	// Log
	if(r_log.m_verbose) {
		ss << "max logical warps per block: " << maxLogicalWarpsPerBlock << endl;
		ss << "registers per warp: " << registersPerWarp << endl;
		ss << "max warps per SM: " << maxWarpsPerSM << endl;
	}

	if(maxLogicalWarpsPerBlock >= maxWarpsPerSM) {
		// All warps of a SM fit one block (unlikely)
		blockSize->x = maxWarpsPerSM * prop.warpSize;
		blockSize->y = 1;
		blockSize->z = 1;
	} else {
		// From here on, the logical max warps per block is guaranteed to be smaller than the physical limit set by max warps per SM

		// Minimum required amounts of blocks to fill SM
		uint32_t minRequiredBlocksForSM = (uint32_t)ceilf((float)maxWarpsPerSM / (float)maxLogicalWarpsPerBlock); // This is already aligned with the warp alloc granularity

		// Log
		if(r_log.m_verbose) {
			ss << "min required blocks to fill SM: " << minRequiredBlocksForSM << endl;
			ss << "trying different amounts of blocks per SM:" << endl;
		}

		// Try out different amounts (2,3,4,5) of blocks per SM and choose best
		uint32_t quotientWarpCounts[4];
		uint32_t quotientOccupancy[4];
		uint32_t maxOcc = 0;
		for(uint32_t i = 0; i < 4; i++) {
			uint32_t quot = i + 2;

			if(minRequiredBlocksForSM > quot) {
				// Use maximum block size
				quotientWarpCounts[i] = maxLogicalWarpsPerBlock;
				quotientOccupancy[i] = quot * quotientWarpCounts[i];
			} else {
				quotientWarpCounts[i] = maxWarpsPerSM / quot;
				quotientWarpCounts[i] -= quotientWarpCounts[i] % warpAllocGranularity;
				quotientOccupancy[i] = quot * quotientWarpCounts[i];
			}

			if(quotientOccupancy[i] > quotientOccupancy[maxOcc]) {
				maxOcc = i;
			}

			// Log
			if(r_log.m_verbose) {
				ss << "  " << quot << " :: threads per block: " << quotientWarpCounts[i] * prop.warpSize << ", occupancy: " << quotientOccupancy[i] << endl;
			}
		}

		// Set block size
		blockSize->x = quotientWarpCounts[maxOcc] * prop.warpSize;
		blockSize->y = 1;
		blockSize->z = 1;
	}

	// Calculate grid size
	gridSize->x = (uint32_t)ceil((float)threadCount / (float)blockSize->x);
	gridSize->y = 1;
	gridSize->z = 1;

	// Log
	if(r_log.m_verbose) {
		ss << "final block size: " << blockSize->x << ", " << blockSize->y << ", " << blockSize->z << endl;
		ss << "final grid size: " << gridSize->x << ", " << gridSize->y << ", " << gridSize->z << endl;
		ss << "number of threads launched: " << gridSize->x * blockSize->x << endl << endl;

		ss << r_log.ThreadPrefix() << "Done calculating launch metrics." << endl << endl;
		ss << Log::HLINE << endl << endl;

		r_log << ss.str();
	}

	// Old code
	/*
	// From register usage and hard limits, determine how many threads a block can maximally have.
	uint32_t maxBlockSize = min(
		(uint32_t)prop.maxThreadsPerBlock,
		min(
		(uint32_t)prop.regsPerMultiprocessor / registersPerThread,
		min(
		(uint32_t)prop.regsPerBlock / registersPerThread,
		BLOCK_SIZE_HARD_LIMIT
		)
		)
	);
	// Round down to next warp-granularity boundary
	if(maxBlockSize % (warpAllocGranularity * prop.warpSize) > 0) {
		maxBlockSize = (maxBlockSize / (warpAllocGranularity * prop.warpSize)) * warpAllocGranularity * prop.warpSize;
	}

	// Registers consumed by warp
	uint32_t registersPerWarp = registersPerThread * prop.warpSize;
	registersPerWarp = (uint32_t)ceil((float)registersPerWarp / (float)regAllocGranularity) * regAllocGranularity;

	// Max achievable warps with current use of registers
	// The goal is to get there
	uint32_t maxWarpsPerSM = min(
		(uint32_t)prop.regsPerMultiprocessor / registersPerWarp,
		(uint32_t)prop.maxThreadsPerMultiProcessor / prop.warpSize
	);
	if(maxWarpsPerSM % warpAllocGranularity > 0) {
		maxWarpsPerSM = (maxWarpsPerSM / warpAllocGranularity) * warpAllocGranularity;
	}

	// Log
	if(r_log.m_verbose) {
		ss << "max block size: " << maxBlockSize << endl;
		ss << "registers per warp: " << registersPerWarp << endl;
		ss << "max warps per SM: " << maxWarpsPerSM << endl;
	}
	
	if(maxBlockSize / prop.warpSize >= maxWarpsPerSM) {
		// All warps fit one block (quite unlikely)
		blockSize->x = maxWarpsPerSM * prop.warpSize;
		blockSize->y = 1;
		blockSize->z = 1;
	} else {
		uint32_t maxWarpAllocations = maxWarpsPerSM / warpAllocGranularity;
		uint32_t maxWarpsPerBlock = maxBlockSize / prop.warpSize;

		uint32_t minRequiredBlocks = (uint32_t)ceilf((float)maxWarpAllocations / (float)(maxWarpsPerBlock / warpAllocGranularity));

		// Log
		if(r_log.m_verbose) {
			ss << "min required blocks: " << minRequiredBlocks << endl;
			ss << "trying different amounts of blocks per SM:" << endl;
		}

		// Try out different quotients (2,3,4,5) and choose best
		uint32_t quotientBlockSizes[4];
		uint32_t quotientOccupancy[4];
		uint32_t maxOcc = 0;
		for(uint32_t i = 0; i < 4; i++) {
			uint32_t quot = i + 2;

			if(minRequiredBlocks > quot) {
				// Use maximum block size
				quotientBlockSizes[i] = maxBlockSize;
				quotientOccupancy[i] = quot * quotientBlockSizes[i] / prop.warpSize;
			} else {
				// If enough warps are unused, ignore granularity
				if(maxWarpsPerSM <= prop.maxThreadsPerMultiProcessor / prop.warpSize - quot * warpAllocGranularity) {
					quotientBlockSizes[i] = maxWarpAllocations * warpAllocGranularity / quot * prop.warpSize;
				} else {
					quotientBlockSizes[i] = maxWarpAllocations / quot * warpAllocGranularity * prop.warpSize;
				}
				quotientOccupancy[i] = quot * quotientBlockSizes[i] / prop.warpSize;
			}

			if(quotientOccupancy[i] > quotientOccupancy[maxOcc]) {
				maxOcc = i;
			}

			// Log
			if(r_log.m_verbose) {
				ss << "  " << quot << " : " << quotientBlockSizes[i] << ", " << quotientOccupancy[i] << endl;
			}
		}

		blockSize->x = quotientBlockSizes[maxOcc];
		blockSize->y = 1;
		blockSize->z = 1;
	}
	
	// Calculate grid size
	gridSize->x = (uint32_t)ceil((float)threadCount / (float)blockSize->x);
	gridSize->y = 1;
	gridSize->z = 1;

	// Log
	if(r_log.m_verbose) {
		ss << "final block size: " << blockSize->x << ", " << blockSize->y << ", " << blockSize->z << endl;
		ss << "final grid size: " << gridSize->x << ", " << gridSize->y << ", " << gridSize->z << endl;
		ss << "number of threads launched: " << gridSize->x * blockSize->x << endl << endl;

		ss << r_log.ThreadPrefix() << "Done calculating launch metrics." << endl << endl;
		ss << Log::HLINE << endl << endl;

		r_log << ss.str();
	}
	*/
}

}
