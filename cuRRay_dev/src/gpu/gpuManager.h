//
// This file is part of cuRRay.
//
// (c) 2018 Sébastien Garmier (sebastien.garmier@gmail.com)
//

#ifndef GPU_MANAGER_H
#define GPU_MANAGER_H

#include "stdafx.h"

#include <string>
#include <vector>
#include <map>

namespace cuRRay {

class Log;

/// \brief Holds information abaout a gpu
struct GPUInformation {

	/// \brief Device id
	int m_id;

	/// \brief CUDA device properties
	cudaDeviceProp m_deviceProperties;

	/// \brief Tells whether the device has a high enough compute capability
	bool m_canBeUsed;

};

/// \brief Manages GPUs
class GPUManager {

public:

	/// \brief Constructor
	GPUManager(Log& log);
	/// \brief Destructor
	~GPUManager() = default;

	/// \brief Initializes the manager
	void Init(int requiredComputeMajor, int requiredComputeMinor);

	/// \brief Prints gpu information
	void PrintGPUInformation();

	/// \brief Returns driver version
	/// \return Driver version
	int GetDriverVersion() { return m_driverVersion; }
	/// \brief Returns minimal compute cabability major
	/// \return Minimal compute capability major
	int GetRequiredComputeMajor() { return m_requiredComputeMajor; }
	/// \brief Returns minimal compute cabability minor
	/// \return Minimal compute capability minor
	int GetRequiredComputeMinor() { return m_requiredComputeMinor; }

	/// \brief Returns GPU count
	/// \return GPU count
	int GetGPUCount() { return m_gpuCount; }
	/// \brief Returns GPU information
	/// \param id The GPU id
	/// \return The GPU information
	/// \exception runtime_error If id doesn't exist
	GPUInformation const& GetGPUInformation(int id);
	/// \brief Check if GPU is enabled
	/// \param The GPU id
	/// \return Enabled flag
	bool IsGPUEnabled(int id);
	/// \brief Enables GPU
	void EnableGPU(int id);
	/// \brief Disables GPU
	void DisableGPU(int id);

	/// \brief Returns all currently enabled GPUs
	/// \return Vector containing all enabled GPUs
	std::vector<int> GetEnabledGPUs();

	/// \brief Computes optimal launch parameters
	///
	/// \param id The id of the gpu
	/// \param threadCount The amount of threads to execute in total
	/// \param registersPerThread The amount of registers used by thread
	/// \param blockSize Pointer to where the block size will be stored
	/// \param gridSize pointer to where the grid size will be stored
	void ComputeLaunchParameters(
		int id,
		uint32_t threadCount,
		uint32_t registersPerThread,
		dim3* blockSize,
		dim3* gridSize
		);

	/// \brief Upper hard limit for block sizes to be used in launch bounds
	///
	/// This number was computed using the CUDA occupancy calculator
	const static uint32_t BLOCK_SIZE_HARD_LIMIT = 768;

private:

	Log& r_log;

	int m_driverVersion;
	int m_requiredComputeMajor;
	int m_requiredComputeMinor;

	int m_gpuCount;
	std::vector<GPUInformation> m_gpuInformation;
	std::map<int, bool> m_gpuEnabled;

};

}

#endif