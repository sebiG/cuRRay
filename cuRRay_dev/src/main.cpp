//
// This file is part of cuRRay.
//
// (c) 2018 Sébastien Garmier (sebastien.garmier@gmail.com)
//

#include <tclap/CmdLine.h>

#include <iostream>

#include "system/program.h"

using namespace std;
using namespace cuRRay;

/// \brief Exit handler
/// Used for waiting after the program terminates
void Exit() {
	//cin.get();
}

/// \brief Main function
///
/// \param argc Argument count
/// \param argv Arguments
int main(int argc, char** argv) {

	atexit(Exit);

	Program program;
	return program.Run(argc, argv);

}