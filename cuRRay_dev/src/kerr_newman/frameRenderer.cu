//
// This file is part of cuRRay.
//
// (c) 2018 Sébastien Garmier (sebastien.garmier@gmail.com)
//

// This file can be compiled both with nvcc and usual c++ compilers

#include "kerr_newman/frameRenderer.h"

#include "util/util.h"
#include "util/math.h"
#include "util/matrix.h"
#include "util/vector.h"

#include <pngIO/pngIO.h>

#include <chrono>

#ifdef USE_CUDA
#include <math_functions.h>
#include <math_constants.h>
#include <device_launch_parameters.h>
#endif

using namespace std;

namespace cuRRay {

using namespace Util;

namespace KerrNewman {

// CUDA
#ifdef USE_CUDA

__constant__ SceneData d_sceneData[1];
__constant__ SceneSphereData d_sceneSphereData[MAX_SCENE_SPHERES];
__constant__ uint32_t d_frameSize[2];
__constant__ double d_horizonEpsilon[1];
__constant__ double d_stepSizeMultiplier[1];

__global__ void TestConstantMemoryKernel() {

	SceneData& scene = d_sceneData[0];

	printf("constant memory test:\n\n");

	printf("frame size:\n");
	printf("  %d, %d\n", d_frameSize[0], d_frameSize[1]);
	printf("\n");

	printf("metric:\n");
	printf("  m: %f\n", scene.m_metric.m);
	printf("  a: %f\n", scene.m_metric.a);
	printf("  q: %f\n", scene.m_metric.q);
	printf("  horizon radius: %f\n", scene.m_metric.m_horizonRadius);
	printf("\n");

	printf("observer:\n");
	printf("  r: %f\n", scene.m_observer.r);
	printf("  theta: %f\n", scene.m_observer.theta);
	printf("  phi: %f\n", scene.m_observer.phi);
	printf("  screen right: %f\n", scene.m_observer.m_screenRight);
	printf("  screen top: %f\n", scene.m_observer.m_screenTop);
	printf("  yaw: %f\n", scene.m_observer.m_yaw);
	printf("  pitch: %f\n", scene.m_observer.m_pitch);
	printf("  roll: %f\n", scene.m_observer.m_roll);
	printf("\n");

	if(scene.m_hasAccretion) {
		printf("accretion disc:\n");
		printf("  color1: %d, %d, %d\n", scene.m_accretion.m_color1.m_red, scene.m_accretion.m_color1.m_green, scene.m_accretion.m_color1.m_blue);
		printf("  color2: %d, %d, %d\n", scene.m_accretion.m_color2.m_red, scene.m_accretion.m_color2.m_green, scene.m_accretion.m_color2.m_blue);
		printf("  resolution: %d, %d\n", scene.m_accretion.m_res1, scene.m_accretion.m_res2);
		printf("  yaw: %f\n", scene.m_accretion.m_yaw);
		printf("  radius: %f, %f\n", scene.m_accretion.m_innerRadius, scene.m_accretion.m_outerRadius);
		printf("\n");
	}

	printf("scene boundary: %f\n", scene.m_sceneBoundary);
	printf("\n");

	for(unsigned i = 0; i < scene.m_spheresCount; i++) {

		SceneSphereData& object = d_sceneSphereData[i];

		printf("sphere %d:\n", i);

		printf("  size: %d, %d\n", object.m_res1, object.m_res2);
		printf("  color: %d, %d, %d\n", object.m_color.m_red, object.m_color.m_green, object.m_color.m_blue);
		printf("  r: %f\n", object.r);
		printf("  theta: %f\n", object.theta);
		printf("  phi: %f\n", object.phi);
		printf("  theta: %f\n", object.theta);
		printf("  radius: %f\n", object.m_radius);

		printf("  yaw: %f\n", object.m_yaw);
		printf("  pitch: %f\n", object.m_pitch);
		printf("  roll: %f\n", object.m_roll);
		printf("\n");

	}

}

__global__ void
//__launch_bounds__(GPUManager::BLOCK_SIZE_HARD_LIMIT)
RaytraceKernel(
	ParticleData const* initialPhotonData,
	PixelData* pixelData,
	Vector4_double* skyData
) {

	// If thread is padding, return
	uint32_t n = blockIdx.x * blockDim.x + threadIdx.x;
	if(n >= d_frameSize[0] * d_frameSize[1]) return;

	// Run raytracer
	FrameRenderer::Raytrace(
		n,
		initialPhotonData,
		pixelData,
		skyData,
		d_sceneData,
		d_sceneSphereData,
		d_horizonEpsilon[0],
		d_stepSizeMultiplier[0]
	);
}

#endif // #ifdef USE_CUDA

// FrameRenderer

#ifdef USE_CUDA
FrameRenderer::FrameRenderer(Log& log, Scene& scene, string const& outputDirectory, GPUManager& gpuManager) :
	r_log(log),
	r_scene(scene),
	m_outputDirectory(outputDirectory),
	r_gpuManager(gpuManager)
{}
#else
FrameRenderer::FrameRenderer(Log& log, Scene& scene, std::string const& outputDirectory) :
	r_log(log),
	r_scene(scene),
	m_outputDirectory(outputDirectory)
{}
#endif

CUDA_HOST_DEVICE
void FrameRenderer::Raytrace(
	uint32_t n,
	ParticleData const* initialPhotonData,
	PixelData* pixelData,
	Vector4_double* skyData,
	SceneData const* scene,
	SceneSphereData const* spheres,
	double horizonEpsilon,
	double stepSizeMultiplier
) {

	// Shortcut to global memory locations
	ParticleData const& initialPhoton = initialPhotonData[n];
	PixelData& pixel = pixelData[n];
	Vector4_double& skyVector = skyData[n];

	// Create photon working data
	ParticleData photon;
	photon.pos = initialPhoton.pos;
	photon.vel = initialPhoton.vel;

	// Create and compute metric components and Christoffel symbols
	MetricComponents g;
	ChristoffelSymbols c;
	Raytracer::Calculate_g_c(
		scene,
		photon.pos.m_1,
		photon.pos.m_2,
		&g, &c
		);

	// Test
	/*
	printf("(%d)\n 00: %f\n 11: %f\n 22: %f\n 33: %f\n", n, g.g00, g.g11, g.g22, g.g33);
	printf("(%d)\n 1_00: %f\n 1_11: %f\n 1_22: %f\n 1_33: %f\n 2_12: %f\n 2_33: %f\n", n, c.c1_00, c.c1_11, c.c1_22, c.c1_33, c.c2_12, c.c2_33);
	*/

	// Calculate energy and angular momentum parameters
	double E, L;
	Raytracer::Calculate_E_L(&g, photon.vel.m_0, photon.vel.m_3, &E, &L);

	// Calculate frequency scale for later
	double initialFrequencyScale = Raytracer::CalculateStationaryFrequencyScale(&g, photon.vel);

	// set redshift to default value (corresponds to no redshift computed)
	pixel.m_redshift = -1.0;

	// Numerical error
	double errorSum = 0.0;
	double error2Sum = 0.0;

	// Counter
	uint32_t stepCount = 0;

	// Cartesian position
	Vector4_double cartPos = Raytracer::BLToCartesian(photon.pos, scene->m_metric.a);

	// Test
	/*
	printf("(%d) initial pos.: %f, %f, %f\n", n, initialPhoton.pos.m_1, initialPhoton.pos.m_2, initialPhoton.pos.m_3);
	printf("(%d) initial vel.: %f, %f, %f, %f\n", n, initialPhoton.vel.m_0, initialPhoton.vel.m_1, initialPhoton.vel.m_2, initialPhoton.vel.m_3);
	printf("(%d) initial error: %f\n", n, Raytracer::CalculateNullError(&g, initialPhoton.vel));

	Raytracer::Calculate_v0_v3(&g, b, &photon.vel.m_0, &photon.vel.m_3);
	printf("(%d) test vel.: %f, %f, %f, %f\n", n, photon.vel.m_0, photon.vel.m_1, photon.vel.m_2, photon.vel.m_3);
	printf("(%d) test error: %f\n", n, Raytracer::CalculateNullError(&g, photon.vel));
	*/

	// Integrate
	while(true) {

		// Test
		/*
		printf("(%d) vel.: %f, %f, %f, %f\n", n, photon.vel.m_0, photon.vel.m_1, photon.vel.m_2, photon.vel.m_3);
		*/

		// Calculate step size
		double h = Raytracer::CalculateStepSize(photon.vel, stepSizeMultiplier);

		// Store old position
		Vector4_double lastPos = photon.pos;
		Vector4_double lastCartPos = cartPos;

		// Perform integration step
		Raytracer::RK4_Step(
			scene,
			&g, &c,
			&photon,
			E, L,
			h
		);
		cartPos = Raytracer::BLToCartesian(photon.pos, scene->m_metric.a);

		// Increase count
		stepCount++;

		// Calculate error
		double error = Raytracer::CalculateNullError(&g, photon.vel);
		errorSum += error;
		error2Sum += error * error;

		// Test if invalid
		if((photon.pos.m_2 < 0.0) | (photon.pos.m_2 > MATH_PI) | (photon.pos.m_1 < 0.0)) {
			pixel.m_color = scene->m_errorColor;
			pixel.m_source = SpecialPixelSource::SOURCE_ERROR;
			goto endTrace;
		}

		// Test if out of bounds
		if(photon.pos.m_1 > scene->m_sceneBoundary) {
			pixel.m_color = scene->m_skyColor;
			pixel.m_source = SpecialPixelSource::SOURCE_BOUNDARY;

			// Calculate skybox data
			if(scene->m_hasSkyMap) {
				skyVector = Raytracer::Vector_BLToCartesian(
					photon.vel,
					photon.pos,
					scene->m_metric.a
				);
				skyVector.m_0 = 0.0;
				skyVector = skyVector.Normalize();
			}

			goto endTrace;
		}

		// Test if too close to the horizon
		if(photon.pos.m_1 - horizonEpsilon * scene->m_metric.m_horizonRadius < scene->m_metric.m_horizonRadius) {
			pixel.m_color = scene->m_horizonColor;
			pixel.m_source = SpecialPixelSource::SOURCE_HORIZON;
			goto endTrace;
		}

		// Accretion collision detection
		if(scene->m_hasAccretion) {
			double param = (MATH_PI_OVER_TWO - lastPos.m_2) / (photon.pos.m_2 - lastPos.m_2);
			double impactR = lastPos.m_1 + (photon.pos.m_1 - lastPos.m_1) * param;
			double impactPhi = lastPos.m_3 + (photon.pos.m_3 - lastPos.m_3) * param;
			if(
				((photon.pos.m_2 - MATH_PI_OVER_TWO) * (lastPos.m_2 - MATH_PI_OVER_TWO) <= 0.0)
				& (impactR > scene->m_accretion.m_innerRadius)
				& (impactR < scene->m_accretion.m_outerRadius)
				) {
				// The disk was hit

				// Find what side of the disk was hit
				Color const* c = (photon.pos.m_2 > MATH_PI_OVER_TWO) ? &scene->m_accretion.m_color1 : &scene->m_accretion.m_color2;

				// Calculate radial sector
				bool rSector = (int32_t)floor((impactR - scene->m_accretion.m_innerRadius) / (scene->m_accretion.m_outerRadius - scene->m_accretion.m_innerRadius) * (double)scene->m_accretion.m_res1) % 2 == 0;
				bool phiSector = (int32_t)floor((impactPhi - scene->m_accretion.m_yaw) / MATH_TWO_PI * (double)scene->m_accretion.m_res2) % 2 == 0;
				if(rSector ^ phiSector) {
					// Use gray
					pixel.m_color = { 127, 127, 127 };
				} else {
					// Use color
					pixel.m_color = *c;
				}

				// Set pixel source
				pixel.m_source = SpecialPixelSource::SOURCE_ACCRETION;

				goto endTrace;
			}
		}

		// Spheres collision detection
		if(scene->m_spheresCount > 0) {
			for(uint32_t i = 0; i < scene->m_spheresCount; i++) {
				SceneSphereData const& sphere = spheres[i];

				// x = p - a
				Vector4_double x = Vector4_double{ 0.0, sphere.x, sphere.y, sphere.z } -lastCartPos;
				// ab = b - a
				Vector4_double ab = cartPos - lastCartPos;

				// Remove time components
				x.m_0 = 0.0;
				ab.m_0 = 0.0;

				double x_ab = x.Dot(ab);
				double x2 = x.Length2();
				double ab2 = ab.Length2();

				// Calculate intersection time
				double t = (x_ab - sqrt(x_ab * x_ab - ab2 * (x2 - sqr(sphere.m_radius)))) / ab2;

				if(
					(t >= 0.0)
					& (t <= 1.0)
					) {
					// The sphere has been hit

					// Calculate intersection point in local coordinates
					// intersection = a + t * ab - p = t * ab - (p - a) = t * ab - x
					Vector4_double intersection = t * ab - x;

					// Apply rotations
					intersection = intersection.CartRotZ(-sphere.m_yaw + sphere.phi);
					intersection = intersection.CartRotY(-sphere.m_pitch + MATH_PI_OVER_TWO - sphere.theta);
					intersection = intersection.CartRotX(-sphere.m_roll);

					// Get longitude
					bool longSector = (int32_t)floor(atan2(intersection.m_2, intersection.m_1) / MATH_TWO_PI * (double)sphere.m_res2) % 2 == 0;
					bool latSector = (int32_t)floor(acos(intersection.m_3 / sphere.m_radius) / MATH_PI * (double)sphere.m_res1) % 2 == 0;
					if(longSector ^ latSector) {
						// Use gray
						pixel.m_color = { 127, 127, 127 };
					} else {
						// Use color
						pixel.m_color = sphere.m_color;
					}
					pixel.m_source = i;

					goto endTrace;
				}
			}
		}
	}

endTrace:

	// Calculate redshift if outside of static limit
	if(photon.pos.m_1 > scene->m_metric.m + sqrt(sqr(scene->m_metric.m) - sqr(scene->m_metric.q) - sqr(scene->m_metric.a) * sqr(cos(photon.pos.m_2)))) {
		pixel.m_redshift = initialFrequencyScale / Raytracer::CalculateStationaryFrequencyScale(&g, photon.vel);
	}

	// Set step count
	pixel.m_stepCount = stepCount;

	// Calculate average error
	pixel.m_errorAverage = errorSum / (double)stepCount;
	pixel.m_errorStandardDeviation = sqrt((
		error2Sum
		- 2.0 * pixel.m_errorAverage * errorSum
		+ (double)stepCount * pixel.m_errorAverage * pixel.m_errorAverage
	) / (double)stepCount);

	// Test
	/*
	printf("(%d) error: %f\n", n, pixel.m_error);
	printf("%d, %d\n", stepCount, pixel.m_source);
	*/
}

// CUDA-only functions
#ifdef USE_CUDA

void FrameRenderer::RunCUDA() {

	// Load sky map
	if(r_scene.m_hasSkyMap) {
		LoadSkyMap(r_scene.m_skyMap.m_imageFile);
	}

	// Get available devices
	vector<int> availableDevices = r_gpuManager.GetEnabledGPUs();
	if(availableDevices.empty()) {
		r_log <= "Warning: No enabled GPU available for CUDA-accelerated frame rendering. No frames will be rendered." <= endl;
		return;
	}

	// Start at the beginning
	m_nextFrame = 0;

	// Create threads for all GPUs
	vector<thread> threads;
	threads.reserve(availableDevices.size());

	map<int, runtime_error> threadExceptions;
	mutex exceptionMutex;

	for(auto& i : availableDevices) {
		threads.push_back(thread([this, &threadExceptions, &exceptionMutex](int deviceID) {
			
			try {

				this->ThreadCUDA(deviceID);

			} catch(runtime_error& e) {
				lock_guard<mutex> lock(exceptionMutex);
				threadExceptions.insert({ deviceID, e });
			}

		}, i));
	}

	// Wait for all threads to finish
	for(auto& i : threads) {
		if(i.joinable()) i.join();
	}

	// Handle all thread exceptions
	Util::HandleThreadExceptions(threadExceptions, DebugCodeLocation);

	// Destroy sky map
	if(r_scene.m_hasSkyMap) {
		DestroySkyMap();
	}
}

void FrameRenderer::ThreadCUDA(int deviceID) {
	cudaError status;

	// Create thread prefix for log
	r_log.m_threadPrefixes.insert({ this_thread::get_id(), "dev. " + Util::ToString(deviceID) });

	// Set current device for this thread
	status = cudaSetDevice(deviceID);
	if(status) throw runtime_error(Util::CUDAError(status, "cudaSetDevice", DebugCodeLocation));

	// Memory
	SceneData sceneData;
	SceneSphereData sceneSphereData[MAX_SCENE_SPHERES];
	ParticleData* h_initialPhotonData;
	PixelData* h_pixelData;
	Vector4_double* h_skyData;

	ParticleData* d_initialPhotonData;
	PixelData* d_pixelData;
	Vector4_double* d_skyData;

	// Allocate host memory
	{
		// Initial velocity vectors
		status = cudaMallocHost(
			(void**)&h_initialPhotonData,
			r_scene.m_pixelCount * sizeof(ParticleData)
		);
		if(status) throw runtime_error(Util::CUDAError(status, "cudaMallocHost", DebugCodeLocation));

		// Pixel data
		status = cudaMallocHost(
			(void**)&h_pixelData,
			r_scene.m_pixelCount * sizeof(PixelData)
		);
		if(status) throw runtime_error(Util::CUDAError(status, "cudaMallocHost", DebugCodeLocation));

		// Sky data
		status = cudaMallocHost(
			(void**)&h_skyData,
			r_scene.m_pixelCount * sizeof(Vector4_double)
		);
		if(status) throw runtime_error(Util::CUDAError(status, "cudaMallocHost", DebugCodeLocation));
	}

	// Allocate global device memory
	{
		// Initial photon data
		status = cudaMalloc(
			(void**)&d_initialPhotonData,
			r_scene.m_pixelCount * sizeof(ParticleData)
		);
		if(status) throw runtime_error(Util::CUDAError(status, "cudaMalloc", DebugCodeLocation));

		// Pixel data
		status = cudaMalloc(
			(void**)&d_pixelData,
			r_scene.m_pixelCount * sizeof(PixelData)
		);
		if(status) throw runtime_error(Util::CUDAError(status, "cudaMalloc", DebugCodeLocation));

		// Sky data
		status = cudaMalloc(
			(void**)&d_skyData,
			r_scene.m_pixelCount * sizeof(Vector4_double)
		);
		if(status) throw runtime_error(Util::CUDAError(status, "cudaMalloc", DebugCodeLocation));
	}

	while(true) {

		// Start time mesurement
		auto startTime = chrono::high_resolution_clock::now();

		// Create frame data
		uint32_t frameNumber = GetNextFrame();
		if(frameNumber >= r_scene.m_frameCount) {
			break;
		}

		r_scene.CreateFrameData(
			frameNumber,
			&sceneData,
			sceneSphereData
		);

		// Copy data to constant device memory
		{
			// Scene data
			status = cudaMemcpyToSymbol(d_sceneData, &sceneData, sizeof(SceneData), 0, cudaMemcpyHostToDevice);
			if(status) throw runtime_error(Util::CUDAError(status, "cudaMemcpyToSymbol", DebugCodeLocation));

			// Scene objects
			status = cudaMemcpyToSymbol(d_sceneSphereData, sceneSphereData, MAX_SCENE_SPHERES * sizeof(SceneSphereData), 0, cudaMemcpyHostToDevice);
			if(status) throw runtime_error(Util::CUDAError(status, "cudaMemcpyToSymbol", DebugCodeLocation));

			// Frame size
			uint32_t frameSize[2] = { r_scene.m_frameWidth, r_scene.m_frameHeight };
			status = cudaMemcpyToSymbol(d_frameSize, frameSize, 2 * sizeof(uint32_t), 0, cudaMemcpyHostToDevice);
			if(status) throw runtime_error(Util::CUDAError(status, "cudaMemcpyToSymbol", DebugCodeLocation));

			// Horizon epsilon
			status = cudaMemcpyToSymbol(d_horizonEpsilon, &Raytracer::m_horizonEpsilon, sizeof(double), 0, cudaMemcpyHostToDevice);
			if(status) throw runtime_error(Util::CUDAError(status, "cudaMemcpyToSymbol", DebugCodeLocation));

			// Step size multiplier
			status = cudaMemcpyToSymbol(d_stepSizeMultiplier, &Raytracer::m_stepSizeMultiplier, sizeof(double), 0, cudaMemcpyHostToDevice);
			if(status) throw runtime_error(Util::CUDAError(status, "cudaMemcpyToSymbol", DebugCodeLocation));
		}

		// Test constant memory
		/*
		TestConstantMemoryKernel <<< 1, 1 >>>();
		status = cudaGetLastError();
		if(status) throw runtime_error(Util::CUDAError(status, "cudaGetLastError (TestConstantMemory)", DebugCodeLocation));
		status = cudaDeviceSynchronize();
		if(status) throw runtime_error(Util::CUDAError(status, "cudaDeviceSynchronize (TestConstantMemory)", DebugCodeLocation));
		*/

		// Create initial velocity vectors
		CreateInitialVelocites(&sceneData, h_initialPhotonData);
		
		// Copy initial photon data to global device memory
		status = cudaMemcpy(
			d_initialPhotonData,
			h_initialPhotonData,
			r_scene.m_pixelCount * sizeof(ParticleData),
			cudaMemcpyHostToDevice
			);
		if(status) throw runtime_error(Util::CUDAError(status, "cudaMemcpy", DebugCodeLocation));

		// Raytrace
		{
			// Compute launch parameters
			dim3 blockSize;
			dim3 gridSize;
			r_gpuManager.ComputeLaunchParameters(
				deviceID,
				r_scene.m_frameWidth * r_scene.m_frameHeight,
				Raytrace_RegCount,
				&blockSize, &gridSize
			);

			// Launch kernel
			RaytraceKernel <<< gridSize, blockSize >>> (d_initialPhotonData, d_pixelData, d_skyData);
			status = cudaGetLastError();
			if(status) throw runtime_error(Util::CUDAError(status, "cudaGetLastError (Raytrace)", DebugCodeLocation));
			status = cudaDeviceSynchronize();
			if(status) throw runtime_error(Util::CUDAError(status, "cudaDeviceSynchronize (Raytrace)", DebugCodeLocation));
		}

		// Copy data to host
		{
			// Pixel data
			status = cudaMemcpy(
				h_pixelData,
				d_pixelData,
				r_scene.m_pixelCount * sizeof(PixelData),
				cudaMemcpyDeviceToHost
			);
			if(status) throw runtime_error(Util::CUDAError(status, "cudaMemcpy", DebugCodeLocation));

			// Sky data
			status = cudaMemcpy(
				h_skyData,
				d_skyData,
				r_scene.m_pixelCount * sizeof(Vector4_double),
				cudaMemcpyDeviceToHost
			);
			if(status) throw runtime_error(Util::CUDAError(status, "cudaMemcpy", DebugCodeLocation));
		}

		// Calculate skymap if needed
		if(r_scene.m_hasSkyMap) {
			ApplySkyMap(h_pixelData, h_skyData);
		}

		// Output
		WriteFrameOutput(frameNumber, h_pixelData);

		// Stop time mesurement
		auto endTime = chrono::high_resolution_clock::now();
		uint64_t microseconds = chrono::duration_cast<chrono::microseconds>(endTime - startTime).count();
		r_log <= (r_log.ThreadPrefix() + "frame " + ToString(frameNumber) + " took " + ToString((double)microseconds / 1000000.0) + "s\n");
	}

	// Free memory
	{
		// Free host memory
		status = cudaFreeHost(h_initialPhotonData);
		if(status) throw runtime_error(Util::CUDAError(status, "cudaFreeHost", DebugCodeLocation));

		status = cudaFreeHost(h_pixelData);
		if(status) throw runtime_error(Util::CUDAError(status, "cudaFreeHost", DebugCodeLocation));

		status = cudaFreeHost(h_skyData);
		if(status) throw runtime_error(Util::CUDAError(status, "cudaFreeHost", DebugCodeLocation));

		// Free global device memory
		status = cudaFree(d_initialPhotonData);
		if(status) throw runtime_error(Util::CUDAError(status, "cudaFree", DebugCodeLocation));

		status = cudaFree(d_pixelData);
		if(status) throw runtime_error(Util::CUDAError(status, "cudaFree", DebugCodeLocation));

		status = cudaFree(d_skyData);
		if(status) throw runtime_error(Util::CUDAError(status, "cudaFree", DebugCodeLocation));
	}

	// Close device
	status = cudaDeviceReset();
	if(status) throw runtime_error(Util::CUDAError(status, "cudaDeviceReset", DebugCodeLocation));

	// Remove thread prefix from log
	r_log.m_threadPrefixes.erase(this_thread::get_id());
}

#endif // #ifdef USE_CUDA

void FrameRenderer::RunCPU(uint32_t threadCount) {

	// Load sky map
	if(r_scene.m_hasSkyMap) {
		LoadSkyMap(r_scene.m_skyMap.m_imageFile);
	}

	// Start at the beginning
	m_nextFrame = 0;

	// Memory
	SceneData sceneData;
	SceneSphereData sceneSphereData[MAX_SCENE_SPHERES];
	ParticleData* initialPhotonData = nullptr;
	PixelData* pixelData = nullptr;
	Vector4_double* skyData = nullptr;

	// Allocate memory
	{
		initialPhotonData = new ParticleData[r_scene.m_pixelCount];
		if(initialPhotonData == nullptr) {
			throw runtime_error("Memory allocation failed. At: " DebugCodeLocation ".\n");
		}

		pixelData = new PixelData[r_scene.m_pixelCount];
		if(pixelData == nullptr) {
			throw runtime_error("Memory allocation failed. At: " DebugCodeLocation ".\n");
		}

		skyData = new Vector4_double[r_scene.m_pixelCount];
		if(skyData == nullptr) {
			throw runtime_error("Memory allocation failed. At: " DebugCodeLocation ".\n");
		}
	}

	// Create threads
	vector<thread> threads;
	threads.reserve(threadCount);

	map<int, runtime_error> threadExceptions;
	mutex exceptionMutex;

	// Loop through every frame
	while(true) {
		// Start time mesurement
		auto startTime = chrono::high_resolution_clock::now();

		// Create frame data
		uint32_t frameNumber = GetNextFrame();
		if(frameNumber >= r_scene.m_frameCount) {
			break;
		}

		r_scene.CreateFrameData(
			frameNumber,
			&sceneData,
			sceneSphereData
		);

		// Create initial velocity vectors
		CreateInitialVelocites(&sceneData, initialPhotonData);

		// Start threads
		for(uint32_t i = 0; i < threadCount; i++) {
			threads.push_back(thread([&](uint32_t threadId) {

				try {

					this->ThreadCPU(threadId, threadCount, initialPhotonData, pixelData, skyData, &sceneData, sceneSphereData);
				
				} catch(runtime_error const& e) {
					lock_guard<mutex> lock(exceptionMutex);
					threadExceptions.insert({ threadId, e });
				}

			}, i));
		}

		// Wait for threads to finish
		for(auto& i : threads) {
			if(i.joinable()) i.join();
		}

		// Handle all thread exceptions
		Util::HandleThreadExceptions(threadExceptions, DebugCodeLocation);

		// Calculate sky map if needed
		if(r_scene.m_hasSkyMap) {
			ApplySkyMap(pixelData, skyData);
		}
		
		// Output
		WriteFrameOutput(frameNumber, pixelData);

		// Stop time mesurement
		auto endTime = chrono::high_resolution_clock::now();
		uint64_t microseconds = chrono::duration_cast<chrono::microseconds>(endTime - startTime).count();
		r_log <= ("frame " + ToString(frameNumber) + " took " + ToString((double)microseconds / 1000000.0) + "s\n");
	}

	// Free memory
	{
		delete[] initialPhotonData;
		delete[] pixelData;
		delete[] skyData;
	}

	// Destroy sky map
	if(r_scene.m_hasSkyMap) {
		DestroySkyMap();
	}
}

void FrameRenderer::ThreadCPU(
	uint32_t offset,
	uint32_t threadCount,
	ParticleData const* initialPhotonData,
	PixelData* pixelData,
	Vector4_double* skyData,
	SceneData const* scene,
	SceneSphereData const* sceneSpheres
) {

	// Create thread prefix for log
	r_log.m_threadPrefixes.insert({ this_thread::get_id(), "thread " + Util::ToString(offset) });

	for(uint32_t n = offset; n < r_scene.m_pixelCount; n += threadCount) {
		// Run raytracer
		FrameRenderer::Raytrace(
			n,
			initialPhotonData,
			pixelData,
			skyData,
			scene,
			sceneSpheres,
			Raytracer::m_horizonEpsilon,
			Raytracer::m_stepSizeMultiplier
		);
	}

}

void FrameRenderer::CreateInitialVelocites(
	SceneData const* sceneData,
	ParticleData* initialPhotonData
) {

	// Compute transformation from BL to local Lorentz coordinates
	MetricComponents g;
	Matrix4_double transformToLocal;
	Matrix4_double transformFromLocal;

	Raytracer::Calculate_g(sceneData, sceneData->m_observer.r, sceneData->m_observer.theta, &g);
	Raytracer::ComputeLocalLorentzFrame(&g, &transformToLocal, &transformFromLocal);

	for(uint32_t y = 0; y < r_scene.m_frameHeight; y++) {
		for(uint32_t x = 0; x < r_scene.m_frameWidth; x++) {
			CreateInitialVelocity(
				sceneData,
				transformFromLocal,
				x, y,
				&initialPhotonData[y * r_scene.m_frameWidth + x]
			);
		}
	}

	// Test the four photons in the corners
	if(r_log.m_verbose) {
		stringstream ss;

		ss << r_log.ThreadPrefix() << "Testing some initial velocites:" << endl << endl;
		ss << "  top left    :" << initialPhotonData[0].vel;
		ss << ", error: " << Raytracer::CalculateNullError(&g, initialPhotonData[0].vel) << endl;
		ss << "  top right   :" << initialPhotonData[r_scene.m_frameWidth - 1].vel;
		ss << ", error: " << Raytracer::CalculateNullError(&g, initialPhotonData[r_scene.m_frameWidth - 1].vel) << endl;
		ss << "  bottom left :" << initialPhotonData[r_scene.m_pixelCount - r_scene.m_frameWidth].vel;
		ss << ", error: " << Raytracer::CalculateNullError(&g, initialPhotonData[r_scene.m_pixelCount - r_scene.m_frameWidth].vel) << endl;
		ss << "  bottom right:" << initialPhotonData[r_scene.m_pixelCount - 1].vel;
		ss << ", error: " << Raytracer::CalculateNullError(&g, initialPhotonData[r_scene.m_pixelCount - 1].vel) << endl << endl;
		ss << r_log.ThreadPrefix() << "Done testing." << endl << endl;
		ss << Log::HLINE << endl << endl;

		r_log << ss.str();
	}
}

void FrameRenderer::CreateInitialVelocity(
	SceneData const* sceneData,
	Matrix4_double const& transformFromLocal,
	uint32_t x, uint32_t y,
	ParticleData* initialPhotonData
) {

	// Set photon start position
	initialPhotonData->pos = Vector4_double(
		0.0,
		sceneData->m_observer.r,
		sceneData->m_observer.theta,
		sceneData->m_observer.phi
	);

	// Calculate interpolation factors
	double t_x = 0.5;
	double t_y = 0.5;
	if(r_scene.m_frameWidth > 1) {
		t_x = (double)x / (double)(r_scene.m_frameWidth - 1);
	}
	if(r_scene.m_frameHeight > 1) {
		t_y = (double)y / (double)(r_scene.m_frameHeight - 1);
	}

	// Create 3d velocity vector by linearly interpolating the screen boundaries
	Vector4_double v = Vector4_double(
		1.0,
		-1.0, // inwards
		-sceneData->m_observer.m_screenTop + 2.0 * sceneData->m_observer.m_screenTop * t_y, // down is towards increasing theta 
		-sceneData->m_observer.m_screenRight + 2.0 * sceneData->m_observer.m_screenRight * t_x // right is towards increasing phi
	);

	// Normalize spatial part and make v null
	{
		double norm = sqrt(v.m_1*v.m_1 + v.m_2*v.m_2 + v.m_3*v.m_3);
		v.m_1 /= norm;
		v.m_2 /= norm;
		v.m_3 /= norm;
		v.m_0 = 1.0;
	}

	// Apply rotations
	v = v.CartRotX(-sceneData->m_observer.m_roll);
	v = v.CartRotZ(-sceneData->m_observer.m_pitch);
	v = v.CartRotY(-sceneData->m_observer.m_yaw);

	if(sceneData->m_observer.theta <= 0) {
		// Observer sits above the north pole

		// Calculate angle of our vector from the positive y-axis
		double angle = 0.0;
		if(v.m_2 != 0.0 || v.m_3 != 0.0) {
			angle = atan2(v.m_3, v.m_2);
		}

		// Correct v
		v = v.CartRotX(-angle);

		// Correct photon start position
		initialPhotonData->pos.m_3 += angle;
		if(initialPhotonData->pos.m_3 < 0.0) {
			initialPhotonData->pos.m_3 += 2.0 * MATH_PI;
		}
		if(initialPhotonData->pos.m_3 >= 2.0 * MATH_PI) {
			initialPhotonData->pos.m_3 -= 2.0 * MATH_PI;
		}

		// Transform initial velocity vector into BL coordinates
		v = transformFromLocal | v;

		// Correct theta velocity if it is leading out of bounds
		v.m_2 = abs(v.m_2);

	} else if(sceneData->m_observer.theta >= MATH_PI) {

		// Observer sits above the south pole

		// Calculate angle of our vector from the positive y-axis
		double angle = 0.0;
		if(v.m_2 != 0.0 || v.m_3 != 0.0) {
			angle = MATH_PI - atan2(v.m_3, v.m_2);
		}

		// Correct v
		v = v.CartRotX(angle);

		// Correct photon start position
		initialPhotonData->pos.m_3 += angle;
		if(initialPhotonData->pos.m_3 < 0.0) {
			initialPhotonData->pos.m_3 += 2.0 * MATH_PI;
		}
		if(initialPhotonData->pos.m_3 >= 2.0 * MATH_PI) {
			initialPhotonData->pos.m_3 -= 2.0 * MATH_PI;
		}

		// Transform initial velocity vector into BL coordinates
		v = transformFromLocal | v;

		// Correct theta velocity if it is leading out of bounds
		v.m_2 = -abs(v.m_2);

	} else {

		// Observer does not sit at a coordinate singularity

		// Transform initial velocity vector into BL coordinates
		v = transformFromLocal | v;

	}

	// Store initial velocity
	initialPhotonData->vel = v;

	// Test
	/*
	MetricComponents g;
	Raytracer::Calculate_g(sceneData, sceneData->m_observer.r, sceneData->m_observer.theta, &g);
	printf("(%d, %d) Length: %f\n", x, y, g.g00*v.m_0*v.m_0 + g.g11*v.m_1*v.m_1 + g.g22*v.m_2*v.m_2 + g.g33*v.m_3*v.m_3 + 2.0 * g.g03*v.m_0*v.m_3);
	*/
}

void FrameRenderer::Legacy_CreateInitialVelocity(
	SceneData const* sceneData,
	MetricComponents const* g,
	uint32_t x, uint32_t y,
	ParticleData* initialPhotonData
) {

	// Set photon start position
	initialPhotonData->pos = Vector4_double(
		0.0,
		sceneData->m_observer.r,
		sceneData->m_observer.theta,
		sceneData->m_observer.phi
		);

	// Calculate interpolation factors
	double t_x = 0.5;
	double t_y = 0.5;
	if(r_scene.m_frameWidth > 1) {
		t_x = (double)x / (double)(r_scene.m_frameWidth - 1);
	}
	if(r_scene.m_frameHeight > 1) {
		t_y = (double)y / (double)(r_scene.m_frameHeight - 1);
	}

	// Create 3d velocity vector by linearly interpolating the screen boundaries
	Vector4_double v = Vector4_double(
		1.0,
		1.0, // inwards
		sceneData->m_observer.m_screenRight - 2.0 * sceneData->m_observer.m_screenRight * t_x,
		sceneData->m_observer.m_screenTop - 2.0 * sceneData->m_observer.m_screenTop * t_y
	);

	// Normalize spatial part
	{
		double norm = sqrt(v.m_1*v.m_1 + v.m_2*v.m_2 + v.m_3*v.m_3);
		v.m_1 /= norm;
		v.m_2 /= norm;
		v.m_3 /= norm;
	}

	// Apply rotations
	v = v.CartRotX(sceneData->m_observer.m_roll);
	v = v.CartRotY(sceneData->m_observer.m_pitch);
	v = v.CartRotZ(sceneData->m_observer.m_yaw);

	// Get observer position in cartesian coordinates
	Vector4_double w = Raytracer::BLToCartesian(
		{ 0.0, sceneData->m_observer.r, sceneData->m_observer.theta, sceneData->m_observer.phi },
		sceneData->m_metric.a
	);

	// Get position of velocity vector tip in cartesian coordinates
	w += v;

	// Transform tip to BL coordinates
	w = Raytracer::CartesianToBL(w, sceneData->m_metric.a);

	// Store initial velocity vector
	initialPhotonData->vel = w - initialPhotonData->pos;

	// Scale time component
	double g03_v3 = 2.0 * g->g03 * initialPhotonData->vel.m_3;
	initialPhotonData->vel.m_0 = (
		-g03_v3
		- sqrt(
			g03_v3 * g03_v3 - 4.0 * g->g00 *
			(g->g11 * initialPhotonData->vel.m_1*initialPhotonData->vel.m_1 + g->g22 * initialPhotonData->vel.m_2*initialPhotonData->vel.m_2 + g->g33 * initialPhotonData->vel.m_3*initialPhotonData->vel.m_3)
		)
		) / (2.0 * g->g00);
}

void FrameRenderer::LoadSkyMap(string const& skyMap) {

	// Open file stream
	ifstream is(skyMap, ios::binary);
	if(!is.is_open()) {
		throw runtime_error("Cannot open file " + skyMap + " for reading.");
	}

	int colorType;
	uint32_t channels;
	uint32_t bitDepth;
	
	try {
		// Read PNG file
		PngIO::Read(is, &m_skyMapWidth, &m_skyMapHeight, &colorType, &channels, &bitDepth, &p_skyMapData);
	} catch(runtime_error const& e) {
		string error = "Error while reading PNG file " + skyMap + ":\n" + e.what();
		throw runtime_error(error.c_str());
	}

	if(colorType != PNG_COLOR_TYPE_RGB || channels != 3 || bitDepth != 8) {
		throw runtime_error("The sky map file must be a 24 bit RGB color PNG.");
	}
}

void FrameRenderer::DestroySkyMap() {
	delete[] p_skyMapData;
}

void FrameRenderer::ApplySkyMap(PixelData* pixelData, Util::Vector4_double const* skyData) {
	
	// Loop through every pixel
	for(uint32_t y = 0; y < r_scene.m_frameHeight; y++) {
		for(uint32_t x = 0; x < r_scene.m_frameWidth; x++) {
			
			// Shortcuts
			PixelData& pixel = pixelData[y * r_scene.m_frameWidth + x];
			Vector4_double const& skyVector = skyData[y * r_scene.m_frameWidth + x];

			// Skip all pixel not part of the sky
			if(pixel.m_source != SpecialPixelSource::SOURCE_BOUNDARY) {
				continue;
			}

			// Get celestial angles
			double azimuth = atan2(skyVector.m_2, skyVector.m_1);
			double zenith = acos(skyVector.m_3); // Already normalized
			if(azimuth < 0.0) azimuth += MATH_TWO_PI;

			// Test
			/*
			cout << "(" << x << "," << y << "): " << skyVector << endl;
			cout << "(" << x << "," << y << "): az: " << azimuth / MATH_PI << " pi, alt: " << altitude << " pi" << endl;
			*/

			// Get pixel coordinates
			double xPixel = (double)m_skyMapWidth * (1.0 - azimuth / MATH_TWO_PI);
			double yPixel = (double)m_skyMapHeight * (zenith / MATH_PI);

			int32_t xPixel1 = (int32_t)floor(xPixel);
			int32_t yPixel1 = (int32_t)floor(yPixel);
			int32_t xPixel2 = xPixel1 + 1; if(xPixel2 >= m_skyMapWidth) xPixel2 = 0;
			int32_t yPixel2 = yPixel1 + 1; if(yPixel2 >= m_skyMapHeight) yPixel2 = 0;

			double xFrac = xPixel - (double)xPixel1;
			double yFrac = yPixel - (double)yPixel1;

			double r[] = {
				p_skyMapData[3 * (yPixel1 * m_skyMapWidth + xPixel1)],
				p_skyMapData[3 * (yPixel1 * m_skyMapWidth + xPixel2)],
				p_skyMapData[3 * (yPixel2 * m_skyMapWidth + xPixel1)],
				p_skyMapData[3 * (yPixel2 * m_skyMapWidth + xPixel2)]
			};
			double g[] = {
				p_skyMapData[3 * (yPixel1 * m_skyMapWidth + xPixel1) + 1],
				p_skyMapData[3 * (yPixel1 * m_skyMapWidth + xPixel2) + 1],
				p_skyMapData[3 * (yPixel2 * m_skyMapWidth + xPixel1) + 1],
				p_skyMapData[3 * (yPixel2 * m_skyMapWidth + xPixel2) + 1]
			};
			double b[] = {
				p_skyMapData[3 * (yPixel1 * m_skyMapWidth + xPixel1) + 2],
				p_skyMapData[3 * (yPixel1 * m_skyMapWidth + xPixel2) + 2],
				p_skyMapData[3 * (yPixel2 * m_skyMapWidth + xPixel1) + 2],
				p_skyMapData[3 * (yPixel2 * m_skyMapWidth + xPixel2) + 2]
			};

			double rf[] = {
				(1.0 - xFrac) * r[0] + xFrac * r[1],
				(1.0 - xFrac) * r[2] + xFrac * r[3]
			};
			double gf[] = {
				(1.0 - xFrac) * g[0] + xFrac * g[1],
				(1.0 - xFrac) * g[2] + xFrac * g[3]
			};
			double bf[] = {
				(1.0 - xFrac) * b[0] + xFrac * b[1],
				(1.0 - xFrac) * b[2] + xFrac * b[3]
			};

			double r_ = (1.0 - yFrac) * rf[0] + yFrac * rf[1];
			double g_ = (1.0 - yFrac) * gf[0] + yFrac * gf[1];
			double b_ = (1.0 - yFrac) * bf[0] + yFrac * bf[1];

			/*
			int32_t xPixel = (int32_t)round((double)m_skyMapWidth * (1.0 - azimuth / MATH_TWO_PI));
			int32_t yPixel = (int32_t)round((double)m_skyMapHeight * (zenith / MATH_PI));
			if(xPixel < 0) {
			xPixel += (int32_t)m_skyMapWidth;
			}
			if(xPixel >= (int32_t)m_skyMapWidth) {
			xPixel -= (int32_t)m_skyMapWidth;
			}
			if(yPixel < 0) {
			yPixel = -yPixel;
			}
			if(yPixel >= (int32_t)m_skyMapHeight) {
			yPixel = 2 * m_skyMapHeight - yPixel;
			}

			// Set color
			uint32_t pixelIndex = 3 * (yPixel * m_skyMapWidth + xPixel);
			pixel.m_color.m_red = p_skyMapData[pixelIndex];
			pixel.m_color.m_green = p_skyMapData[pixelIndex + 1];
			pixel.m_color.m_blue = p_skyMapData[pixelIndex + 2];
			*/

			pixel.m_color.m_red = (unsigned char)r_;
			pixel.m_color.m_green = (unsigned char)g_;
			pixel.m_color.m_blue = (unsigned char)b_;

		}
	}

}

void FrameRenderer::WriteFrameOutput(uint32_t frameNumber, PixelData const* pixelData) {

	string colorPNG = m_outputDirectory + "/c_" + Util::ToString(frameNumber) + ".png";
	string redshiftPNG = m_outputDirectory + "/r_" + Util::ToString(frameNumber) + ".png";
	string pixelDataFile = m_outputDirectory + "/" + Util::ToString(frameNumber) + ".csv";

	// Write images
	if(r_scene.m_outputColorImages) {
		WriteImage(colorPNG, pixelData, WriteImageType::COLOR);
	}
	if(r_scene.m_outputRedshiftImages) {
		WriteImage(redshiftPNG, pixelData, WriteImageType::REDSHIFT);
	}
	if(r_scene.m_outputPixelData) {
		WriteFullPixelDataCSV(pixelDataFile, pixelData);
	}
}

void FrameRenderer::WriteImage(
	string const& imageFile,
	PixelData const* pixelData,
	WriteImageType writeType
) {

	// Open filestream
	ofstream os;
	os.open(imageFile, ios::binary);
	if(!os.is_open()) {
		throw runtime_error("Cannot open file " + imageFile + " for writing.");
	}

	try {

		// Start PNG file
		PngIO::PngWriter writer = PngIO::PngWriter(
			&os,
			r_scene.m_frameWidth,
			r_scene.m_frameHeight,
			PNG_COLOR_TYPE_RGB, 8
		);
		writer.StartPng();

		unsigned char* row = new unsigned char[r_scene.m_frameWidth * 3];
		for(uint32_t y = 0; y < r_scene.m_frameHeight; y++) {
			for(uint32_t x = 0; x < r_scene.m_frameWidth; x++) {
				uint32_t pixelIndex = y * r_scene.m_frameWidth + x;
				switch(writeType) {
					case WriteImageType::COLOR:
						row[x * 3] = pixelData[pixelIndex].m_color.m_red;
						row[x * 3 + 1] = pixelData[pixelIndex].m_color.m_green;
						row[x * 3 + 2] = pixelData[pixelIndex].m_color.m_blue;
						break;
					case WriteImageType::REDSHIFT:
						if(pixelData[pixelIndex].m_source == SpecialPixelSource::SOURCE_BOUNDARY || pixelData[pixelIndex].m_source == SpecialPixelSource::SOURCE_ERROR) {
							// Draw all sky pixels black
							row[x * 3] = 0;
							row[x * 3 + 1] = 0;
							row[x * 3 + 2] = 0;
						} else if(pixelData[pixelIndex].m_redshift < 0.0) {
							// Draw all pixel for which redshift could not be computed in green
							row[x * 3] = 0;
							row[x * 3 + 1] = 255;
							row[x * 3 + 2] = 0;
						} else {
							// Apply gradient to represent redshift
							double value = atan(pixelData[pixelIndex].m_redshift) * 2.0 / MATH_PI;
							row[x * 3] = 255 - (uint8_t)(255.0 * value);
							row[x * 3 + 1] = 0;
							row[x * 3 + 2] = (uint8_t)(255.0 * value);
							break;
						}
				}
			}
			writer.WriteRow(row);
		}
		delete[] row;

		// End PNG file
		writer.EndPng();

	} catch(runtime_error const& e) {
		string error = "Error while writing PNG file " + imageFile + ":\n" + e.what();
		throw runtime_error(error.c_str());
	}

	// Close stream
	os.close();

}

void FrameRenderer::WriteFullPixelDataCSV(
	std::string const& fileName,
	PixelData const* pixelData
) {

	// Open filestream
	ofstream os;
	os.open(fileName, ios::binary);
	if(!os.is_open()) {
		throw runtime_error("Cannot open file " + fileName + " for writing.");
	}

	// Print header
	os << "x; y; red; green; blue; step count; redshift; error average; error standard deviation" << endl;

	// Output entire data
	for(uint32_t y = 0; y < r_scene.m_frameHeight; y++) {
		for(uint32_t x = 0; x < r_scene.m_frameWidth; x++) {

			// The current pixel
			PixelData const& pixel = pixelData[y * r_scene.m_frameHeight + x];

			// Output pixel coordinates
			os << x << ";" << y << ";";
			// Output color
			os << (int)pixel.m_color.m_red << ";" << (int)pixel.m_color.m_green << ";" << (int)pixel.m_color.m_blue << ";";
			// Output step count
			os << (int)pixel.m_stepCount << ";";
			// Output redshift
			os << pixel.m_redshift << ";";
			// Output error information
			os << pixel.m_errorAverage << ";" << pixel.m_errorStandardDeviation;

			os << endl;

		}
	}

	// Close stream
	os.close();

}

uint32_t FrameRenderer::GetNextFrame() {
	lock_guard<mutex> lock(m_frameMutex);

	uint32_t returnValue = m_nextFrame;
	m_nextFrame++;

	return returnValue;
}

}
}