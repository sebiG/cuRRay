//
// This file is part of cuRRay.
//
// (c) 2018 Sébastien Garmier (sebastien.garmier@gmail.com)
//

// This file can be compiled both with nvcc and usual c++ compilers

#include "kerr_newman/raytracer.h"

#ifdef USE_CUDA
// After <cuda_runtime.h>
#ifndef __CUDACC__
#define __CUDACC__
#endif
#include <device_launch_parameters.h>
#include <math_functions.h>
#include <math_constants.h>
#endif

using namespace std;

namespace cuRRay {

using namespace Util;

namespace KerrNewman {

void Raytracer::Init(Log* log) {
	o_log = log;
}

CUDA_HOST_DEVICE
void Raytracer::Calculate_g(
	SceneData const* sceneData,
	double r,
	double theta,
	MetricComponents* g
) {

	// References to constant memory
	double const& m = sceneData->m_metric.m;
	double const& a = sceneData->m_metric.a;
	double const& q = sceneData->m_metric.q;

	double r2 = r * r;
	double a2 = a * a;
	double q2 = q * q;

	double sinTheta;
	sinTheta = sin(theta);
	double sinTheta2 = sinTheta * sinTheta;

	/*
	// Test: Schwarzschild
	g->g00 = -(1.0 - 2.0 * m / r);
	g->g11 = -1.0 / g->g00;
	g->g22 = r2;
	g->g33 = r2 * sinTheta2;
	g->g03 = 0.0;
	// End test
	*/

	double delta = r2 + a2 + q2 - 2.0 * m * r;										// r^2 + a^2 + q^2 - 2Mr
	double sigma = r2 + a2 * (1.0 - sinTheta2);										// r^2 + a^2 * cos^2

	g->g00 = (a2 * sinTheta2 - delta) / sigma;										// -(delta - a^2 sin^2) / sigma
	g->g11 = sigma / delta;															// sigma / delta
	g->g22 = sigma;																	// sigma
	g->g33 = sinTheta2 * ((r2 + a2) * (r2 + a2) - delta * a2 * sinTheta2) / sigma;	// sin^2 * ((r^2 + a^2)^2 - delta a^2 sin^2) / sigma
	g->g03 = a * sinTheta2 * (delta - r2 - a2) / sigma;								// -(a sin^2 * (r^2 + a^2 - delta)) / sigma;

}

CUDA_HOST_DEVICE
void Raytracer::Calculate_g_c(
	SceneData const* sceneData,
	double r,
	double theta,
	MetricComponents* g,
	ChristoffelSymbols* c
) {

	// First, calculate the metric like in Calculate_g,
	// then calculate the Christoffel symbols

	// References to constant memory
	double const& m = sceneData->m_metric.m;
	double const& a = sceneData->m_metric.a;
	double const& q = sceneData->m_metric.q;

	double r2 = r * r;
	double a2 = a * a;
	double q2 = q * q;

	double r2a2 = r2 + a2;

	double sinTheta, cosTheta;
#ifdef __CUDA_ARCH__
	sincos(theta, &sinTheta, &cosTheta);
#else
	sinTheta = sin(theta);
	cosTheta = cos(theta);
#endif
	double sinTheta2 = sinTheta * sinTheta;
	double cosTheta2 = 1.0 - sinTheta2;
	double sinCosTheta = sinTheta * cosTheta;

	/*
	// Test: Schwarzschild
	g->g00 = -(1.0 - 2.0 * m / r);
	g->g11 = -1.0 / g->g00;
	g->g22 = r2;
	g->g33 = r2 * sinTheta2;
	g->g03 = 0.0;

	memset(c, 0, sizeof(ChristoffelSymbols));

	double rs = 2.0 * m;

	c->c1_00 = m * (r - rs) / (r2 * r);
	c->c1_11 = -m / (r * (r - rs));
	c->c1_22 = -(r - rs);
	c->c1_33 = -(r - rs) * sinTheta2;

	c->c2_12 = 1.0 / r;
	c->c2_33 = -sinCosTheta;
	// End test
	*/
	
	double delta = r2 + a2 + q2 - 2.0 * m * r;
	double sigma = r2 + a2 * cosTheta2;

	double _sigma = 1.0 / sigma;													// 1 / sigma
	double _sigma3 = _sigma * _sigma * _sigma;										// 1 / sigma^3

	// Metric
	g->g00 = (a2 * sinTheta2 - delta) / sigma;										// -(delta - a^2 sin^2) / sigma
	g->g11 = sigma / delta;															// sigma / delta
	g->g22 = sigma;																	// sigma
	g->g33 = sinTheta2 * (r2a2 * r2a2 - delta * a2 * sinTheta2) / sigma;			// sin^2 * ((r^2 + a^2)^2 - delta * a^2 * sin^2) / sigma
	g->g03 = a * sinTheta2 * (delta - r2a2) / sigma;								// -(a sin^2 * (r^2 + a^2 - delta)) / sigma

	// Christoffel symbols
	c->c1_00 = delta * _sigma3 * (m*r2 - q2*r - m*a2*cosTheta2);
	c->c1_11 = (_sigma / delta) * (-m*r2 + q2*r + a2*r + (m - r) * a2 * cosTheta2);
	c->c1_22 = -delta * _sigma * r;
	c->c1_33 = delta * _sigma3 * sinTheta2 * (
		sigma * ((r - m) * a2 * sinTheta2 - 2.0 * r * r2a2)
		+ r * (r2a2 * r2a2 - delta * a2 * sinTheta2)
	);

	c->c1_03 = delta * _sigma3 * a * sinTheta2 * (-m*r2 + q2*r + m*a2*cosTheta2);
	c->c1_12 = -_sigma * a2 * sinCosTheta;

	c->c2_00 = _sigma3 * a2 * sinCosTheta * (q2 - 2.0*m*r);
	c->c2_11 = _sigma / delta * a2 * sinCosTheta;
	c->c2_22 = -_sigma * a2 * sinCosTheta;
	c->c2_33 = _sigma3 * sinCosTheta * (
		r2a2 * (delta * a2 * sinTheta2 - r2a2 * r2a2)
		+ sigma * delta * a2 * sinTheta2
	);

	c->c2_03 = _sigma3 * a * sinCosTheta * (2.0*m*r - q2) * r2a2;
	c->c2_12 = _sigma * r;

}

CUDA_HOST_DEVICE
void Raytracer::Calculate_E_L(
	MetricComponents const* g,
	double v0,
	double v3,
	double* E,
	double* L
) {

	*E = g->g00 * v0 + g->g03 * v3;
	*L = g->g33 * v3 + g->g03 * v0;

}

CUDA_HOST_DEVICE
void Raytracer::Calculate_v0_v3(
	MetricComponents const* g,
	double E, double L,
	double* v0,
	double* v3
) {
	// Old code
	/*
	double den = g->g00 * g->g33 - g->g03 * g->g03;
	*v0 = (-g->g33 - b * g->g03) / den;
	*v3 = (b * g->g00 + g->g03) / den;
	*/

	double den = g->g00 * g->g33 - g->g03 * g->g03;
	*v0 = (E * g->g33 - L * g->g03) / den;
	*v3 = (L * g->g00 - E * g->g03) / den;
}

CUDA_HOST_DEVICE
void Raytracer::Calculate_a1_a2(
	ChristoffelSymbols const* c,
	Util::Vector4_double const& v,
	double* a1,
	double* a2
) {
	double v0_2 = v.m_0 * v.m_0;
	double v1_2 = v.m_1 * v.m_1;
	double v2_2 = v.m_2 * v.m_2;
	double v3_2 = v.m_3 * v.m_3;

	*a1 = -(
		(c->c1_00 * v0_2) + (c->c1_11 * v1_2) + (c->c1_22 * v2_2) + (c->c1_33 * v3_2)
		+ (2.0 * c->c1_03 * v.m_0 * v.m_3)
		+ (2.0 * c->c1_12 * v.m_1 * v.m_2)
		);
	*a2 = -(
		(c->c2_00 * v0_2) + (c->c2_11 * v1_2) + (c->c2_22 * v2_2) + (c->c2_33 * v3_2)
		+ (2.0 * c->c2_03 * v.m_0 * v.m_3)
		+ (2.0 * c->c2_12 * v.m_1 * v.m_2)
		);
}

CUDA_HOST_DEVICE
double Raytracer::CalculateStepSize(
	Util::Vector4_double const& v,
	double stepSizeMultiplier
) {

	double h = fmax(fabs(v.m_0), fabs(v.m_1));
	h = fmax(h, fabs(v.m_2));
	h = fmax(h, fabs(v.m_3));
	h = 1.0 / h;
	h *= stepSizeMultiplier;

	return h;
}

CUDA_HOST_DEVICE
void Raytracer::RK4_Step(
	SceneData const* scene,
	MetricComponents* g,
	ChristoffelSymbols* c,
	ParticleData* particle,
	double E,
	double L,
	double h
) {

	// Summing variables
	Util::Vector4_double posSum = { 0.0, 0.0, 0.0, 0.0 };
	double velSum_1 = 0.0;
	double velSum_2 = 0.0;

	// Substep variables
	Util::Vector4_double X = { 0.0, 0.0, 0.0, 0.0 };
	double U_1 = 0.0;
	double U_2 = 0.0;

	// Helpful shortcut
	double h_ = h / 2.0;

	/*
	printf("\n");
	printf("before:\n  pos: %f, %f, %f, %f\n  vel: %f, %f, %f, %f\n",
		particle->pos.m_0, particle->pos.m_1, particle->pos.m_2, particle->pos.m_3,
		particle->vel.m_0, particle->vel.m_1, particle->vel.m_2, particle->vel.m_3);
	*/

	// First substep
	{
		// This is already done
		/*
		// Evaluate at x
		etric::Calculate_g_c(
			scene, 
			particle->pos.m_1, 
			particle->pos.m_2, 
			g, c
		);*/

		// X.m_0 = t'(x)
		// X.m_3 = phi'(x)
		X.m_0 = particle->vel.m_0;
		X.m_3 = particle->vel.m_3;

		// X.m_1 = r'(x)
		// X.m_2 = theta'(x)
		X.m_1 = particle->vel.m_1;
		X.m_2 = particle->vel.m_2;

		// u_1 = r''(x)
		// u_2 = theta''(x)
		Raytracer::Calculate_a1_a2(
			c,
			X, // = u(x)
			&U_1,
			&U_2
		);
		
		// Sum
		posSum += X;
		velSum_1 += U_1;
		velSum_2 += U_2;
	}

	// Second substep
	{
		// Evaluate at x + h/2 * X
		Raytracer::Calculate_g_c(
			scene,
			particle->pos.m_1 + h_ * X.m_1,
			particle->pos.m_2 + h_ * X.m_2,
			g, c
		);

		// X.m_0 = t'(x + h/2 * X)
		// X.m_3 = phi'(x + h/2 * X)
		Raytracer::Calculate_v0_v3(
			g,
			E, L,
			&X.m_0,
			&X.m_3
		);

		// X.m_1 = r'(x) + h/2 * U_1
		// X.m_2 = theta'(x) + h/2 * U_2
		X.m_1 = particle->vel.m_1 + h_ * U_1;
		X.m_2 = particle->vel.m_2 + h_ * U_2;

		// U_1 = r''(x + h/2 * X, u(x) + h/2 * U)
		// U_2 = theta''(x + h/2 * X, u(x) + h/2 * U)
		Raytracer::Calculate_a1_a2(
			c,
			X, // = u(x) + h/2 * U
			&U_1,
			&U_2
		);

		// Sum
		posSum += 2.0 * X;
		velSum_1 += 2.0 * U_1;
		velSum_2 += 2.0 * U_2;
	}

	// Third substep
	{
		// Evaluate at x + h/2 * X
		Raytracer::Calculate_g_c(
			scene,
			particle->pos.m_1 + h_ * X.m_1,
			particle->pos.m_2 + h_ * X.m_2,
			g, c
		);

		// X.m_0 = t'(x + h/2 * X)
		// X.m_3 = phi'(x + h/2 * X)
		Raytracer::Calculate_v0_v3(
			g,
			E, L,
			&X.m_0,
			&X.m_3
		);

		// X.m_1 = r'(x) + h/2 * U_1
		// X.m_2 = theta'(x) + h/2 * U_2
		X.m_1 = particle->vel.m_1 + h_ * U_1;
		X.m_2 = particle->vel.m_2 + h_ * U_2;

		// U_1 = r''(x + h/2 * X, u(x) + h/2 * U)
		// U_2 = theta''(x + h/2 * X, u(x) + h/2 * U)
		Raytracer::Calculate_a1_a2(
			c,
			X, // = u(x) + h/2 * U
			&U_1,
			&U_2
		);

		// Sum
		posSum += 2.0 * X;
		velSum_1 += 2.0 * U_1;
		velSum_2 += 2.0 * U_2;
	}

	// Fourth substep
	{
		// Evaluate at x + h * X
		Raytracer::Calculate_g_c(
			scene,
			particle->pos.m_1 + h * X.m_1,
			particle->pos.m_2 + h * X.m_2,
			g, c
		);

		// X.m_0 = t'(x + h * X)
		// X.m_3 = phi'(x + h * X)
		Raytracer::Calculate_v0_v3(
			g,
			E, L,
			&X.m_0,
			&X.m_3
		);

		// X.m_1 = r'(x) + h * U_1
		// X.m_2 = theta'(x) + h * U_2
		X.m_1 = particle->vel.m_1 + h * U_1;
		X.m_2 = particle->vel.m_2 + h * U_2;

		// U_1 = r''(x + h * X, u(x) + h * U)
		// U_2 = theta''(x + h * X, u(x) + h * U)
		Raytracer::Calculate_a1_a2(
			c,
			X, // = u(x) + h * U
			&U_1,
			&U_2
		);

		// Sum
		posSum += X;
		velSum_1 += U_1;
		velSum_2 += U_2;
	}

	// Scale sums
	posSum *= (h / 6.0);
	velSum_1 *= (h / 6.0);
	velSum_2 *= (h / 6.0);

	// Advance geodesic, correct under- and overflows
	particle->pos += posSum;
	
	if(particle->pos.m_3 >= 0.0) {
		particle->pos.m_3 = fmod(particle->pos.m_3, 2.0 * MATH_PI);
	} else {
		particle->pos.m_3 = 2.0 * MATH_PI + fmod(particle->pos.m_3, (2.0 * MATH_PI));
	}

	// Compute new metric components and Christoffel symbols
	Raytracer::Calculate_g_c(
		scene,
		particle->pos.m_1,
		particle->pos.m_2,
		g, c
	);

	// Update velocity
	Raytracer::Calculate_v0_v3(g, E, L, &particle->vel.m_0, &particle->vel.m_3);
	particle->vel.m_1 += velSum_1;
	particle->vel.m_2 += velSum_2;

	/*
	printf("final:\n  pos: %f, %f, %f, %f\n  vel: %f, %f, %f, %f\n",
		particle->pos.m_0, particle->pos.m_1, particle->pos.m_2, particle->pos.m_3,
		particle->vel.m_0, particle->vel.m_1, particle->vel.m_2, particle->vel.m_3);
	*/
}

CUDA_HOST_DEVICE
void Raytracer::Euler_Step(
	SceneData const* scene,
	MetricComponents* g,
	ChristoffelSymbols*c,
	ParticleData* particle,
	double E,
	double L,
	double h
) {

	// Compute acceleration
	double a1, a2;
	Raytracer::Calculate_a1_a2(c, particle->vel, &a1, &a2);

	// Integrate acceleration
	particle->vel.m_1 += h * a1;
	particle->vel.m_2 += h * a2;

	// Integrate velocity
	particle->pos.m_0 += h * particle->vel.m_0;
	particle->pos.m_1 += h * particle->vel.m_1;
	particle->pos.m_2 += h * particle->vel.m_2;
	particle->pos.m_3 += h * particle->vel.m_3;

	// Correct under- and overflows
	if(particle->pos.m_3 >= 0.0) {
		particle->pos.m_3 = fmod(particle->pos.m_3, 2.0 * MATH_PI);
	} else {
		particle->pos.m_3 = 2.0 * MATH_PI + fmod(particle->pos.m_3, (2.0 * MATH_PI));
	}

	// Compute new metric components and Christoffel symbols
	Raytracer::Calculate_g_c(
		scene,
		particle->pos.m_1,
		particle->pos.m_2,
		g, c
	);

	// Update velocity
	Raytracer::Calculate_v0_v3(g, E, L, &particle->vel.m_0, &particle->vel.m_3);
}

CUDA_HOST_DEVICE
double Raytracer::CalculateNullError(
	MetricComponents const* g,
	Util::Vector4_double const& v
) {
	double error = (g->g11 * v.m_1 * v.m_1) + (g->g22 * v.m_2 * v.m_2) + (g->g33 * v.m_3 * v.m_3) + (2.0 * g->g03 * v.m_0 * v.m_3);
	error /= (g->g00 * v.m_0 * v.m_0);
	error = fabs(1.0 + error);

	return error;
}

CUDA_HOST_DEVICE
double Raytracer::CalculateStationaryFrequencyScale(
	MetricComponents const* g,
	Util::Vector4_double const& v
) {
	return v.m_0 * sqrt(-g->g00) - v.m_3 * g->g03 / sqrt(-g->g00);
}

CUDA_HOST_DEVICE
Vector4_double Raytracer::CartesianToBL(Vector4_double const& cart, double a) {
	Vector4_double bl;
	bl.m_0 = cart.m_0;

	int zSign = Sign(cart.m_3);

	bl.m_3 = atan2(cart.m_2, cart.m_1); // phi = atan(y / x)

	if((cart.m_1 == 0.0) & (cart.m_2 == 0.0)) {
		// Special case x = y = 0
		bl.m_1 = fabs(cart.m_3);							// r = |z|
		bl.m_2 = (1.0 - (double)zSign) * MATH_PI;	// theta = 0 or pi
	} else {
		// general case
		double x2y2 = cart.m_1*cart.m_1 + cart.m_2*cart.m_2; // x2y2 = x^2 + y^2
		double x2y2z2a2 = x2y2 + cart.m_3*cart.m_3 + a*a;
		double sinTheta2 = 2.0 * x2y2 / (x2y2z2a2 + sqrt(x2y2z2a2 * x2y2z2a2 - 4.0 * a*a * x2y2)); // Alt. quadratic equation: x = (-2c) / (b + sqrt(b^2 - 4ac))
		bl.m_2 = zSign * (zSign * asin(zSign * sqrt(sinTheta2)) - ::signbit(cart.m_3) * MATH_PI);
		bl.m_1 = sqrt(x2y2 / sinTheta2 - a*a);
	}

	return bl;
}

CUDA_HOST_DEVICE
Vector4_double Raytracer::BLToCartesian(Vector4_double const& bl, double a) {
	Vector4_double cart;
	cart.m_0 = bl.m_0;

	double h = hypot(bl.m_1, a); // sqrt(r^2 + a^2)
	double sinTheta, cosTheta;
	double sinPhi, cosPhi;
#ifdef __CUDA_ARCH__
	sincos(bl.m_2, &sinTheta, &cosTheta);
	sincos(bl.m_3, &sinPhi, &cosPhi);
#else
	sinTheta = sin(bl.m_2);
	cosTheta = cos(bl.m_2);
	sinPhi = sin(bl.m_3);
	cosPhi = cos(bl.m_3);
#endif

	cart.m_3 = bl.m_1 * cosTheta; // z = r * cos t
	cart.m_1 = h * sinTheta * cosPhi; // x = sqrt(r^2 + a^2) * sin t * cos p
	cart.m_2 = h * sinTheta * sinPhi; // y = sqrt(r^2 + a^2) * sin t * sin p

	return cart;
}

CUDA_HOST_DEVICE
Vector4_double Raytracer::Vector_BLToCartesian(Vector4_double const& blVec, Vector4_double const& blPos, double a) {
	Vector4_double cart;
	cart.m_0 = blVec.m_0;

	double const& r = blPos.m_1;
	double const& theta = blPos.m_2;
	double const& phi = blPos.m_3;

	double h = hypot(r, a);
	double _h = 1.0 / h;
	double sinTheta, cosTheta;
	double sinPhi, cosPhi;
#ifdef __CUDA_ARCH__
	sincos(theta, &sinTheta, &cosTheta);
	sincos(phi, &sinPhi, &cosPhi);
#else
	sinTheta = sin(theta);
	cosTheta = cos(theta);
	sinPhi = sin(phi);
	cosPhi = cos(phi);
#endif

	// v_x
	cart.m_1 =
		blVec.m_1 * _h * r * sinTheta * cosPhi
		+ blVec.m_2 * h * cosTheta * cosPhi
		- blVec.m_3 * h * sinTheta * sinPhi;
	// v_y
	cart.m_2 =
		blVec.m_1 * _h * r * sinTheta * sinPhi
		+ blVec.m_2 * h * cosTheta * sinPhi
		+ blVec.m_3 * h * sinTheta * cosPhi;
	// v_z
	cart.m_3 = 
		blVec.m_1 * cosTheta
		- blVec.m_2 * r * sinTheta;

	return cart;
}

void Raytracer::ComputeLocalLorentzFrame(
	MetricComponents const* g,
	Matrix4_double* toLocal,
	Matrix4_double* fromLocal
) {

	// Log
	stringstream ss;

	if(o_log != nullptr && o_log->m_verbose) {
		ss << o_log->ThreadPrefix() << "Computing local Lorentz frame..." << endl << endl;
	}

	// Find coordinate transformation between Boyer-Lindquist and local Lorentz coordinates
	//
	// The coordinate transform C transforms vectors from BL coordinates (v_bl) to Lorentz coordinates (v_l):
	//   v_l = C * v_bl,
	//   v_bl = C^-1 * v_l.
	// The metric is transformed from BL coords (g) to Lorentz coordinates (n):
	//   n = (C^-1)^T * g * C^-1,
	//   g = C^T * n * C.
	// C has the form
	//   C = S^-1 * P^-1,
	//   C^1 = P * S.
	//

	// Matrices
	Matrix4_double P = Matrix4_double::CreateIdentity();
	Matrix4_double Pinv = Matrix4_double::CreateIdentity();
	Matrix4_double S = Matrix4_double::CreateIdentity();
	Matrix4_double Sinv = Matrix4_double::CreateIdentity();

	// Eigenvalues of metric (ie.: diagonalized metric)
	double l0, l1, l2, l3;

	// P and P^-1
	//
	// P is the simmilarity matrix of g, when g is interpreted as a matrix.
	// I.e., P is the orthonormal matrix of eigenvectors:
	//
	//   | a1  0  0 d1 |
	//   |  0  b  0  0 |
	//   |  0  0  c  0 |.
	//   | a2  0  0 d2 |
	//
	{
		// Calculate eigenvalues of the metric

		double sqrtTerm = sqrt((g->g00 - g->g33) * (g->g00 - g->g33) + 4.0 * g->g03 * g->g03);
		l0 = 0.5 * (g->g00 + g->g33 - sqrtTerm);
		l1 = g->g11;
		l2 = g->g22;
		l3 = 0.5 * (g->g00 + g->g33 + sqrtTerm);

		// Calculate eigenvectors and store them in D
		P(0, 0) = 1.0;
		P(3, 0) = g->g03 / (l0 - g->g33); // The denominator never reaches zero, as l0 is always negative and g33 always positive

		P(1, 1) = 1.0;
		P(2, 2) = 1.0;

		P(0, 3) = g->g03 / (l3 - g->g00); // The denominator never reaches zero, as l3 is always positive and g00 always negative
		P(3, 3) = 1.0;

		// normalize
		P.ColumnVec(0, P.ColumnVec(0).Normalize());
		P.ColumnVec(3, P.ColumnVec(3).Normalize());

		// Calculate inverse
		Pinv = P.Transpose(); // Same as inverse (P is orthonormal)
	}

	// If the eigenvalue l3 is zero (theta = 0 or theta = pi), replace it by one
	// This still breaks any image generated from the poles
	if(l3 == 0.0) l3 = 1.0;

	// S and S^-1
	//
	// S is a scale matrix.
	// The scales are simply the inverse of the square roots of the absolute eigenvalues.
	// This ensures that vectors are scaled properly by multiplication with S (matrices are sclaed by the square of what vectors are).
	//
	{
		S.Scale(
			sqrt(1.0 / abs(l0)),
			sqrt(1.0 / l1),
			sqrt(1.0 / l2),
			sqrt(1.0 / l3)
			);
		Sinv.Scale(
			sqrt(abs(l0)),
			sqrt(l1),
			sqrt(l2),
			sqrt(l3)
			);
	}

	// Now, we still have the choice of a rotation and Lorentz transformation
	// Turns out we don't need any rotation, as the coordinate systems are already oriented

	// Compute Lorentz transformation
	Matrix4_double L = Matrix4_double::CreateIdentity();
	Matrix4_double Linv = Matrix4_double::CreateIdentity();
	{
		// Completely timelike vector in BL coordinates
		Vector4_double blT(sqrt(1.0 / abs(g->g00)), 0, 0, 0);

		// Compute what this vector looks like in cartesian coordinates using the current transformation
		Vector4_double cartT = (Sinv | Pinv) | blT;

		// The Lorentz transformation has the following shape:
		//
		// |  a   0 0 -ab |
		// |  0   1 0  0  |
		// |  0   0 1  0  |
		// | -ab  0 0  a  |
		double a = cartT.m_0; // t / sqrt(t^2 - z^2), t^2 - z^2 = 1
		double b = cartT.m_3 / cartT.m_0;

		Linv(0, 0) = a;
		Linv(3, 3) = a;
		Linv(0, 3) = -a * b;
		Linv(3, 0) = -a * b;

		L(0, 0) = a;
		L(3, 3) = a;
		L(0, 3) = a * b;
		L(3, 0) = a * b;
	}

	// Set toLocal (C)
	*toLocal = Linv | Sinv | Pinv;

	// Set fromLocal (C^-1)
	*fromLocal = P | S | L;

	// Debug output
	if(o_log != nullptr && o_log->m_verbose) {

		Matrix4_double g_ = Matrix4_double::CreateIdentity();
		g_.At(0, 0) = g->g00;
		g_.At(1, 1) = g->g11;
		g_.At(2, 2) = g->g22;
		g_.At(3, 3) = g->g33;
		g_.At(0, 3) = g->g03;
		g_.At(3, 0) = g->g03;
		ss << "metric:" << endl;
		ss << g_ << endl;
		ss << endl;

		ss << "eigenvalues:" << endl;
		ss << "  " << l0 << endl;
		ss << "  " << l1 << endl;
		ss << "  " << l2 << endl;
		ss << "  " << l3 << endl;
		ss << endl;

		ss << "P:" << endl;
		ss << P << endl;
		ss << endl;

		ss << "S:" << endl;
		ss << S << endl;
		ss << endl;

		ss << "L:" << endl;
		ss << L << endl;
		ss << endl;

		ss << "n = L^T * S * P^-1 * g * P * S * L:" << endl;
		ss << (L | S | Pinv | g_ | P | S | L) << endl;
		ss << endl;

		ss << "C = L^-1 * S^-1 * P^-1:" << endl;
		ss << *toLocal << endl;
		ss << endl;

		ss << "C^-1 = P * S * L:" << endl;
		ss << *fromLocal << endl;
		ss << endl;

		ss << o_log->ThreadPrefix() << "Done computing local Lorentz frame" << endl << endl;
		ss << Log::HLINE << endl << endl;

		*o_log << ss.str();

		// Test
		/*
		Vector4_double t_(sqrt(1.0 / abs(g->g00)), 0, 0, 0);

		cout << t_ << endl;
		cout << (*toLocal | t_) << endl;

		Vector4_double t(1, 0, 0, 0);
		Vector4_double x(0, 1, 0, 0);
		Vector4_double y(0, 0, 1, 0);
		Vector4_double z(0, 0, 0, 1);

		cout << "t: " << (P | S | t) << endl;
		cout << "x: " << (P | S | x) << endl;
		cout << "y: " << (P | S | y) << endl;
		cout << "z: " << (P | S | z) << endl;

		Matrix4_double lorentzMetric;
		lorentzMetric(0, 0) = -1;
		cout << ( P | Sinv | lorentzMetric | Sinv | P) << endl;
		*/
	}


}

Log* Raytracer::o_log = nullptr;

double Raytracer::m_stepSizeMultiplier = 1.0 / 32.0;
double Raytracer::m_horizonEpsilon = 0.5E-2;

}
}
