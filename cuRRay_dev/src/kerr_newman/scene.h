//
// This file is part of cuRRay.
//
// (c) 2018 Sébastien Garmier (sebastien.garmier@gmail.com)
//

#ifndef SCENE_H
#define SCENE_H

#include "stdafx.h"

#include "system/log.h"
#include "util/angle.h"
#include "util/color.h"
#include "util/animated.h"

#include <vector>

#include <yaml-cpp/yaml.h>

namespace cuRRay {

/// \brief The namespace for Kerr-Newman-related code
namespace KerrNewman {

/// \brief Maximum amount of spheres in scene
#define MAX_SCENE_SPHERES 8

/// \brief Scene data
///
/// The structure as well as important sections are aligned to 128 bits.
/// This data will reside in constant GPU memory, as it is the same for the entrire frame
#ifdef _WIN64
__declspec(align(128))
#endif
// CUDA automatically aligns beginning of memory allocations to 128 bit boundaries,
// so no configuration required
struct SceneData {

	struct Metric {
		double m;
		double a;
		double q;
		double m_horizonRadius; // This will be computed at runtime and servers as padding
	}
	m_metric;
	// 128-bit boundary

	struct Observer {
		double r;
		double theta;
		double phi;
		double m_padding1;
		
		double m_screenRight;
		double m_screenTop;

		double m_yaw;
		double m_pitch;
		double m_roll;

		double m_padding2;
	}
	m_observer;
	// 128-bit boundary

	struct Accretion {
		Util::Color m_color1;
		uint8_t m_padding1;

		Util::Color m_color2;
		uint8_t m_padding2;

		uint8_t m_res1;
		uint8_t m_res2;

		uint16_t m_padding3;
		uint32_t m_padding4;

		double m_yaw;

		double m_innerRadius;
		double m_outerRadius;
	}
	m_accretion;
	// 128-bit boundary

	Util::Color m_skyColor;
	uint8_t m_padding1;

	Util::Color m_horizonColor;
	uint8_t m_padding2;

	Util::Color m_errorColor;
	uint8_t m_padding3;

	uint32_t m_padding4;
	// 128-bit boundary

	double m_sceneBoundary;

	uint32_t m_spheresCount;

	uint8_t m_hasAccretion;
	uint8_t m_hasSkyMap;

	uint16_t m_padding6;

}
#ifdef __GNUG__
__attribute__((aligned(128)))
#endif
;

/// \brief Sphere
#ifdef _WIN64
__declspec(align(128))
#endif
struct SceneSphereData {

	Util::Color m_color;
	uint8_t m_padding1;

	uint8_t m_res1;
	uint8_t m_res2;

	uint16_t m_padding2;
	uint32_t m_padding3;

	double r;
	double theta;
	double phi;

	double m_padding4;

	double x;
	double y;
	double z;

	double m_padding5;

	double m_yaw;
	double m_pitch;
	double m_roll;

	double m_radius;
	// 128-bit boundary

}
#ifdef __GNUG__
__attribute__((aligned(128)))
#endif
;

/// \brief Scene in Kerr-Newman spacetime
class Scene {

	//
	// Methods
	//
public:

	/// \brief Constructor
	Scene(Log& log);
	/// \brief Destructor
	~Scene() = default;

	/// \brief Loads scene from file
	/// \param sceneFile The scene file
	bool LoadSceneFile(std::string const& sceneFile);

	/// \brief Configures the scene with information from the command line
	/// \param outputDirectory The output directory
	/// \param frameCount The amount of frames to render. If set to zero, no frames will be rendered.
	/// \param frameWidth The width of the frame. If frameCount = 0, this will be ignored.
	/// \param frameHeight The height of the frame. If frameCount = 0, this will be ignored.
	/// \param outputConfig The output configuration
	void ConfigureFromCmd(
		std::string const& outputDirecory,
		std::string const& outputConfig,
		uint32_t frameCount,
		uint32_t frameWidth,
		uint32_t frameHeight
	);

	/// \brief Asks the user for approval
	/// \return The users response
	bool RequestUserApproval();

	/// \brief Creates scene data and stores it at the memory locations provided
	/// \param sceneData Pointer to where the scene data should be stored. Must be writable by the CPU.
	/// \param sceneObjects Pointer to where the scene sphere data should be stored. Must be writable by the CPU.
	void CreateFrameData(
		uint32_t frameNumber,
		SceneData* sceneData,
		SceneSphereData* sceneSpheres
	);

	/// \brief Outputs scene information
	/// \param fileName The output file name
	void OutputSceneInformation(std::string const& fileName);

	//
	// Members
	//
public:

	std::string m_summaryString;
	std::string m_detailedString;

	bool m_outputSceneInformation;
	bool m_outputColorImages;
	bool m_outputRedshiftImages;
	bool m_outputPixelData;

	bool m_renderFrames;
	uint32_t m_frameWidth;
	uint32_t m_frameHeight;
	uint32_t m_pixelCount;
	uint32_t m_frameCount;

	struct {
		Util::Animated<double> m;
		Util::Animated<double> a;
		Util::Animated<double> q;
	}
	m_metric;

	struct {
		Util::Animated<double> r;
		Util::Animated<Util::Angle> theta;
		Util::Animated<Util::Angle> phi;

		Util::Animated<Util::Angle> m_yaw;
		Util::Animated<Util::Angle> m_pitch;
		Util::Animated<Util::Angle> m_roll;

		Util::Animated<Util::Angle> m_hFov;

		bool m_useAspectRatio;
		Util::Animated<Util::Angle>  m_vFov;
	}
	m_observer;

	struct {
		Util::Color m_color1;
		Util::Color m_color2;
		bool m_calculateColor2;

		uint8_t m_res1;
		uint8_t m_res2;

		Util::Animated<Util::Angle> m_yaw;

		Util::Animated<double> m_innerRadius;
		Util::Animated<double> m_outerRadius;
	} m_accretion;

	struct {
		std::string m_imageFile;
		
		Util::Animated<double> m_boundary;
	} m_skyMap;

	Util::Color m_skyColor;
	Util::Color m_horizonColor;
	Util::Color m_errorColor;

	struct SceneSphere {
		Util::Color m_color;

		uint8_t m_res1;
		uint8_t m_res2;

		Util::Animated<double> r;
		Util::Animated<Util::Angle> theta;
		Util::Animated<Util::Angle> phi;

		Util::Animated<Util::Angle> m_yaw;
		Util::Animated<Util::Angle> m_pitch;
		Util::Animated<Util::Angle> m_roll;

		Util::Animated<double> m_radius;
	};
	std::vector<SceneSphere> m_spheres;

	bool m_hasAccretion;
	bool m_hasSkyMap;

private:

	void CreateSceneStrings();

	Log& r_log;
};

}
}

#endif