//
// This file is part of cuRRay.
//
// (c) 2018 Sébastien Garmier (sebastien.garmier@gmail.com)
//

#ifndef RAYTRACER_H
#define RAYTRACER_H

#include "stdafx.h"

#include "system/log.h"
#include "kerr_newman/scene.h"
#include "util/matrix.h"
#include "util/vector.h"

namespace cuRRay {
namespace KerrNewman {

/// \brief Non-zero Kerr-Newman metric components
struct MetricComponents {

	double g00;
	double g11;
	double g22;
	double g33;
	double g03;

};

/// \brief Non-zero Kerr-Newman Christoffel symbols
struct ChristoffelSymbols {

	double c1_00, c1_11, c1_22, c1_33;
	double c1_03, c1_12;
	double c2_00, c2_11, c2_22, c2_33;
	double c2_03, c2_12;

};

/// \brief Particle data
///
/// Used for initial photon data and during raytracing.
/// Aligned to 128 bits.
#ifdef _WIN64
__declspec(align(128))
#endif
struct ParticleData {

	Util::Vector4_double pos;
	Util::Vector4_double vel;

}
#ifdef __GNUG__
__attribute__((aligned(128)))
#endif
;

/// \brief Returns sign of expression (-1, 0, 1)
#define Sign(x) ((int)::signbit(-(x)) - (int)::signbit((x)))

class Raytracer {

public:

	/// \brief Initializes the raytracer
	/// \param log The log
	static void Init(Log* log);

	/// \brief Horizon epsilon (multiples of the Schwarzschild radius)
	static double m_horizonEpsilon;
	/// \brief Step size multiplier
	static double m_stepSizeMultiplier;

	/// \brief Calculates the metric components
	/// \param r The r coordinate
	/// \param theta The theta coordinate
	/// \param g Where the metric components will be stored
	CUDA_HOST_DEVICE
	static void Calculate_g(
		SceneData const* sceneData,
		double r,
		double theta,
		MetricComponents* g
	);

	/// \brief Calculates metric and Christoffel symbols simultaneously
	/// \param sceneData The scene data
	/// \param r The r coordinate
	/// \param theta The theta coordinate
	/// \param g Where the metric components will be stored
	/// \param c where the Christoffel symbols will be stored
	CUDA_HOST_DEVICE
	static void Calculate_g_c(
		SceneData const* sceneData,
		double r,
		double theta,
		MetricComponents* g,
		ChristoffelSymbols* c
	);

	/// \brief Calculates energy and angular momentum parameter of a particle
	/// \param g The metric components
	/// \param v0 The first derivative of the time coordinate
	/// \param v3 The first derivative of the phi coordinate
	/// \param E Where the energy parameter will be stored
	/// \param L Where the angular momentum parameter will be stored
	CUDA_HOST_DEVICE
	static void Calculate_E_L(
		MetricComponents const* g,
		double v0,
		double v3,
		double* E,
		double* L
	);

	/// \brief Calculates first order derivatives of the position for use in the first order geodesic equations
	/// \param g The metric components
	/// \param E The energy parameter
	/// \param L The andular momentum parameter
	/// \param v0 Where the first derivative of the time coordinate will be stored
	/// \param v3 Where the first derivative of the phi coordinate will be stored
	CUDA_HOST_DEVICE 
	static void Calculate_v0_v3(
		MetricComponents const* g,
		double E,
		double L,
		double* v0,
		double* v3
	);

	/// \brief Calculates second order derivatives of the position for use in the second order geodesic equations
	/// \param c The Christoffel symbols
	/// \param v The velocity of the particle
	/// \param a1 Where the second derivative of the r coordinate will be stored
	/// \param a2 Where the second derivative of the theta coordinate will be stored
	CUDA_HOST_DEVICE
	static void Calculate_a1_a2(
		ChristoffelSymbols const* c,
		Util::Vector4_double const& v,
		double* a1,
		double* a2
	);

	/// \brief Computes step size
	/// \param v The velocity of the particle
	/// \param stepSizeMultiplier The step size multiplier
	/// \return The step size
	CUDA_HOST_DEVICE
	static double CalculateStepSize(
		Util::Vector4_double const& v,
		double stepSizeMultiplier
	);

	/// \brief Performs an RK-4 step
	/// \param scene The scene data
	/// \param g Precomputed metric components. The method will later store the new values there.
	/// \param c Precomputed Christoffel symbols. The method will later store the new values there.
	/// \param particle The state of the particle
	/// \param E The energy parameter of the particle
	/// \param L The angular momentum parameter of the particle
	/// \param h The step size
	CUDA_HOST_DEVICE
	static void RK4_Step(
		SceneData const* scene,
		MetricComponents* g,
		ChristoffelSymbols* c,
		ParticleData* particle,
		double E,
		double L,
		double h
	);

	/// \brief Performs an Euler step
	/// \param scene The scene data
	/// \param g Precomputed metric components. The method will later store the new values there.
	/// \param c Precomputed Christoffel symbols. The method will later store the new values there.
	/// \param particle The state of the particle
	/// \param E The energy parameter of the particle
	/// \param L The angular momentum parameter of the particle
	/// \param h The step size
	CUDA_HOST_DEVICE
	static void Euler_Step(
		SceneData const* scene,
		MetricComponents* g,
		ChristoffelSymbols*c,
		ParticleData* particle,
		double E,
		double L,
		double h
	);

	/// \brief Calculates error of a null geodesic
	/// This error is only an indicator and is not related to the real numerical error.
	/// \param g The metric components
	/// \param v The particle velocity
	/// \return The error
	CUDA_HOST_DEVICE
	static double CalculateNullError(
		MetricComponents const* g,
		Util::Vector4_double const& v
	);

	/// \brief Calculates the frequency scale of a photon as seen by an observer resting relative to the coordinates
	/// \param g The metric components
	/// \param v The velocity of the photon
	CUDA_HOST_DEVICE
	static double CalculateStationaryFrequencyScale(
		MetricComponents const* g,
		Util::Vector4_double const& v
	);

	/// \brief Transforms Pseudo-cartesian coordinates to BL coordinates
	/// \param cart The pseudo-cartesian coordinates
	/// \param a The rotation parameter
	/// \return The BL coordinates
	CUDA_HOST_DEVICE
	static Util::Vector4_double CartesianToBL(Util::Vector4_double const& cart, double a);

	/// \brief Transforms BL coordinates to pseudo-cartesian coordinates
	/// \param bl The BL coordinates
	/// \param a The rotation parameter
	/// \return The pseudo-cartesian coordinates
	CUDA_HOST_DEVICE
	static Util::Vector4_double BLToCartesian(Util::Vector4_double const& bl, double a);

	/// \brief Transforms a BL vector to cartesian coordinates
	/// \param blVec The vector in BL coordinates
	/// \param blPos The position at which the vector resides
	/// \param a The rotation parameter
	/// \return The vector in pseudo-cartesian coordinates
	CUDA_HOST_DEVICE
	static Util::Vector4_double Vector_BLToCartesian(Util::Vector4_double const& blVec, Util::Vector4_double const& blPos, double a);

	/// \brief Computes a transformation matrix to transform between Boyer-Lindquist and local Lorentz coordinates
	/// This is only a host function
	/// \param metric The metric components
	/// \param toLocal Where the transformation matrix from BL to local coordinates will be stored
	/// \param fromLocal Where the transformation matrix from local to BL coordinates will be stored
	static void ComputeLocalLorentzFrame(
		MetricComponents const* g,
		Util::Matrix4_double* toLocal,
		Util::Matrix4_double* fromLocal
	);

private:

	static Log* o_log;

};

}
}

#endif
