//
// This file is part of cuRRay.
//
// (c) 2018 Sébastien Garmier (sebastien.garmier@gmail.com)
//

#ifndef FRAME_RENDERER_H
#define FRAME_RENDERER_H

#include "stdafx.h"

#include "system/log.h"
#include "util/vector.h"
#include "util/util.h"
#include "kerr_newman/scene.h"
#include "kerr_newman/raytracer.h"
#ifdef USE_CUDA
#include "gpu/gpuManager.h"
#endif

#include <thread>
#include <mutex>

namespace cuRRay {
namespace KerrNewman {

/// \brief Special pixel sources (other than spheres)
enum SpecialPixelSource : uint8_t {
	SOURCE_ERROR = 255,
	SOURCE_BOUNDARY = 254,
	SOURCE_HORIZON = 253,
	SOURCE_ACCRETION = 252
};

/// \brief Pixel data
#ifdef _WIN64
__declspec(align(128))
#endif
struct PixelData {

	Util::Color m_color;
	uint8_t m_source;

	uint32_t m_stepCount;

	double m_redshift;
	// 128-bit boundary

	double m_errorAverage;
	double m_errorStandardDeviation;

}
#ifdef __GNUG__
__attribute__((aligned(128)))
#endif
;

#ifdef USE_CUDA


extern __constant__ SceneData d_sceneData[1];
extern __constant__ SceneSphereData d_sceneSphereData[MAX_SCENE_SPHERES];
extern __constant__ uint32_t d_frameSize[2];
extern __constant__ double d_horizonEpsilon[1];
extern __constant__ double d_stepSizeMultiplier[1];

/// \brief Prints scene data
__global__ static void TestConstantMemoryKernel();

const static uint32_t Raytrace_RegCount = 63;

/// \brief Kernel for raytracing
/// \param initialPhotonData Initial photon data
/// \param pixelData Where the pixel data is going to be stored
/// \param skyData Where the sky data is going to be stored
__global__ static void
//__launch_bounds__(GPUManager::BLOCK_SIZE_HARD_LIMIT)
RaytraceKernel(
	ParticleData const* initialPhotonData,
	PixelData* pixelData,
	Util::Vector4_double* skyData
);

#endif // #ifdef USE_CUDA

/// \brief Renders frames
class FrameRenderer {

public:

	/// \brief Constructor
	/// \param log The log
	/// \param scene The scene
	/// \param outputDirectory The output directory
	/// \param gpuManager The GPU manager
#ifdef USE_CUDA
	FrameRenderer(
		Log& log,
		Scene& scene,
		std::string const& outputDirectory,
		GPUManager& gpuManager
	);
#else
	FrameRenderer(
		Log& log,
		Scene& scene,
		std::string const& outputDirectory
	);
#endif
	/// \brief Destructor
	~FrameRenderer() = default;

	/// \brief Function for raytracing
	/// \param n The index of the photon
	/// \param initialPhotonData Initial photon data
	/// \param pixelData Where the pixel data is going to be stored
	/// \param skyData Where the sky data is going to be stored
	/// \param sceneData The scene data
	/// \param sceneSpheres The scene sphere data
	/// \param horizonEpsilon The horizon epsilon
	/// \param stepSizeMultiplier The step size multiplier
	CUDA_HOST_DEVICE
	static void Raytrace(
		uint32_t n,
		ParticleData const* initialPhotonData,
		PixelData* pixelData,
		Util::Vector4_double* skyData,
		SceneData const* scene,
		SceneSphereData const* sceneSpheres,
		double horizonEpsilon,
		double stepSizeMultiplier
	);

#ifdef USE_CUDA

	/// \brief Renders the frame using CUDA
	void RunCUDA();

#endif

	/// \brief Renders the frame using the CPU
	/// \param threadCount The amount of threads to use
	void RunCPU(uint32_t threadCount);

private:

#ifdef USE_CUDA

	/// \brief Thread executed for every enabled GPU
	void ThreadCUDA(int deviceID);
	
#endif

	/// \brief CPU rendering thread
	/// \brief offset The offset in pixels at which the thread is to operate
	/// \brief threadCount The total amount of threads used
	/// \param initialPhotonData Initial photon data
	/// \param pixelData Where the pixel data is going to be stored
	/// \param sceneData The scene data
	/// \param sceneSpheres The scene sphere data
	void ThreadCPU(
		uint32_t offset,
		uint32_t threadCount,
		ParticleData const* initialPhotonData,
		PixelData* pixelData,
		Util::Vector4_double* skyData,
		SceneData const* scene,
		SceneSphereData const* sceneSpheres
	);

	/// \brief Creates initial photon velocites
	/// \param sceneData The scene data
	/// \param initialPhotonData Where the array of initial photon velocites will be stored
	void CreateInitialVelocites(
		SceneData const* sceneData,
		ParticleData* initialPhotonData
	);

	/// \brief Creates initial photon velocity
	/// \param sceneData The scene data
	/// \param transformFromLocal The coordinate transformation from local to global coordinates
	/// \param x The x coordinate of the pixel for which the initial velocity is to be computed
	/// \param y The y coordinate of the pixel for which the initial velocity is to be computed
	/// \param initialPhotonData The memory where the initial data is going to be stored
	void CreateInitialVelocity(
		SceneData const* sceneData,
		Util::Matrix4_double const& transformFromLocal,
		uint32_t x, uint32_t y,
		ParticleData* initalPhotonData
	);

	/// \brief Creates initial photon velocity using the old approach
	/// \param sceneData The scene data
	/// \param g The metric components
	/// \param x The x coordinate of the pixel for which the initial velocity is to be computed
	/// \param y The y coordinate of the pixel for which the initial velocity is to be computed
	/// \param initialPhotonData The memory where the initial data is going to be stored
	void Legacy_CreateInitialVelocity(
		SceneData const* sceneData,
		MetricComponents const* g,
		uint32_t x, uint32_t y,
		ParticleData* initialPhotonData
	);

	/// \brief Loads a skymap into memory
	/// \param file The map PNG file
	void LoadSkyMap(std::string const& file);

	/// \brief Destroys resources allocated for sky map
	void DestroySkyMap();

	/// \brief Applies the sky map to every sky pixel
	/// \param pixelData The pixel data to interpret and modify
	/// \param skyData The sky data
	void ApplySkyMap(PixelData* pixelData, Util::Vector4_double const* skyData);

	/// \brief Writes the output of the frame
	/// \param frameNumber The frame number
	/// \param pixelData
	void WriteFrameOutput(
		uint32_t frameNumber,
		PixelData const* pixelData	
	);

	/// \brief Enum used in WriteImage
	enum WriteImageType {
		COLOR,
		REDSHIFT
	};

	/// \brief Writes image file
	/// \param imageFile The image file
	/// \param pixelData The pixel data
	/// \param writeType The operation to be performed
	void WriteImage(
		std::string const& imageFile,
		PixelData const* pixelData,
		WriteImageType writeType
	);

	/// \brief Writes full pixel data to csv file
	/// \param fileName The CSV file
	/// \param pixelData The pixel data
	void WriteFullPixelDataCSV(
		std::string const& fileName,
		PixelData const* pixelData
	);

	/// \brief Fetches next frame number
	/// The caller must make sure the frame number returned is actually valid
	/// \return The next frame number
	uint32_t GetNextFrame();

	///
	/// Members
	///

	uint32_t m_nextFrame;
	std::mutex m_frameMutex;

	Log& r_log;
	Scene& r_scene;

	std::string m_outputDirectory;

	// Skymap
	unsigned char* p_skyMapData;
	uint32_t m_skyMapWidth;
	uint32_t m_skyMapHeight;

#ifdef USE_CUDA
	GPUManager& r_gpuManager;
#endif

};

}
}

#endif