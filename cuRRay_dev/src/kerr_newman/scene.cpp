//
// This file is part of cuRRay.
//
// (c) 2018 Sébastien Garmier (sebastien.garmier@gmail.com)
//

#include "kerr_newman/scene.h"
#include "kerr_newman/raytracer.h"
#include "util/util.h"
#include "util/math.h"
#include "util/vector.h"

#include <iostream>
#include <fstream>

using namespace std;

namespace cuRRay {

using namespace Util;

namespace KerrNewman {

Scene::Scene(Log& log) :
r_log(log) {

}

bool Scene::LoadSceneFile(string const& sceneFile) {
	
	m_spheres.clear();

	// Log
	r_log << "Loading scene file..." << endl;

	YAML::Node scene;

	try {

		// Load YAML
		scene = YAML::LoadFile(sceneFile);

		// Metric
		if(scene["metric"]) {
			YAML::Node metric = scene["metric"];
			if(metric.IsMap()) {

				// Mass
				if(metric["m"]) {
					m_metric.m = Animated<double>::ParseYAML(metric["m"]);
				} else {
					r_log <= Util::PrintYAMLException("Scene file: cannot find 'm' in 'metric'.", metric.Mark()) <= endl;
					return false;
				}
				// Spin parameter
				if(metric["a"]) {
					m_metric.a = Animated<double>::ParseYAML(metric["a"]);
				} else {
					r_log <= Util::PrintYAMLException("Scene file: cannot find 'a' in 'metric'.", metric.Mark()) <= endl;
					return false;
				}
				// Charge
				if(metric["q"]) {
					m_metric.q = Animated<double>::ParseYAML(metric["q"]);
				} else {
					r_log <= Util::PrintYAMLException("Scene file: cannot find 'q' in 'metric'.", metric.Mark()) <= endl;
					return false;
				}

			} else {
				r_log <= Util::PrintYAMLException("Scene file: 'metric' must be a map.", metric.Mark()) <= endl;
				return false;
			}
		} else {
			r_log <= Util::PrintYAMLException("Scene file: cannot find 'metric'.") <= endl;
			return false;
		}

		// Observer
		if(scene["observer"]) {
			YAML::Node observer = scene["observer"];
			if(observer.IsMap()) {

				// r
				if(observer["r"]) {
					m_observer.r = Animated<double>::ParseYAML(observer["r"]);
				} else {
					r_log <= Util::PrintYAMLException("Scene file: cannot find 'r' in 'observer'.", observer.Mark()) <= endl;
					return false;
				}
				// theta
				if(observer["theta"]) {
					m_observer.theta = Animated<Angle>::ParseYAML(observer["theta"]);
				} else {
					r_log <= Util::PrintYAMLException("Scene file: cannot find 'theta' in 'observer'.", observer.Mark()) <= endl;
					return false;
				}
				// phi
				if(observer["phi"]) {
					m_observer.phi = Animated<Angle>::ParseYAML(observer["phi"]);
				} else {
					r_log <= Util::PrintYAMLException("Scene file: cannot find 'phi' in 'observer'.", observer.Mark()) <= endl;
					return false;
				}

				// yaw
				if(observer["yaw"]) {
					m_observer.m_yaw = Animated<Angle>::ParseYAML(observer["yaw"]);
				} else {
					m_observer.m_yaw = Angle(0.0);
				}
				// pitch
				if(observer["pitch"]) {
					m_observer.m_pitch = Animated<Angle>::ParseYAML(observer["pitch"]);
				} else {
					m_observer.m_pitch = Angle(0.0);
				}
				// roll
				if(observer["roll"]) {
					m_observer.m_roll = Animated<Angle>::ParseYAML(observer["roll"]);
				} else {
					m_observer.m_roll = Angle(0.0);
				}

				// hfov
				if(observer["hfov"]) {
					m_observer.m_hFov = Animated<Angle>::ParseYAML(observer["hfov"]);
				} else {
					r_log <= Util::PrintYAMLException("Scene file: cannot find 'hfov' in 'observer'.", observer.Mark()) <= endl;
					return false;
				}
				// vfov
				if(observer["vfov"]) {
					m_observer.m_vFov = Animated<Angle>::ParseYAML(observer["vfov"]);
					m_observer.m_useAspectRatio = false;
				} else {
					m_observer.m_vFov = Angle(0.0);
					m_observer.m_useAspectRatio = true;
				}

			} else {
				r_log <= Util::PrintYAMLException("Scene file: 'observer' must be a map.", observer.Mark()) <= endl;
				return false;
			}
		} else {
			r_log <= Util::PrintYAMLException("Scene file: cannot find 'observer'.") <= endl;
			return false;
		}

		// Accretion disc
		if(scene["accretion"]) {
			// Enable accretion
			m_hasAccretion = true;

			YAML::Node accretion = scene["accretion"];
			if(accretion.IsMap()) {

				// Primary color
				if(accretion["color1"]) {
					m_accretion.m_color1 = Color::ParseYAML(accretion["color1"]);
				} else {
					r_log <= Util::PrintYAMLException("Scene file: cannot find 'color1' in 'accretion'.", accretion.Mark());
					return false;
				}

				// Secondary color
				if(accretion["color2"]) {
					// Don't calculate color2
					m_accretion.m_calculateColor2 = false;

					m_accretion.m_color2 = Color::ParseYAML(accretion["color2"]);
				} else {
					// Calculate color2 from color1
					m_accretion.m_calculateColor2 = true;

					m_accretion.m_color2.m_red = 255 - m_accretion.m_color1.m_red;
					m_accretion.m_color2.m_green = 255 - m_accretion.m_color1.m_green;
					m_accretion.m_color2.m_blue = 255 - m_accretion.m_color1.m_blue;
				}

				// Resolution
				if(accretion["resolution"]) {
					YAML::Node resolution = accretion["resolution"];
					if(resolution.IsSequence() && resolution.size() == 2) {
						uint64_t res1 = resolution[0].as<uint64_t>();
						uint64_t res2 = resolution[1].as<uint64_t>();

						if(res1 > 255 || res2 > 255) {
							r_log <= Util::PrintYAMLException("Scene file: the values of 'resolution' in 'accretion' must lie between 0 and 255.", resolution.Mark()) <= endl;
							return false;
						}
						m_accretion.m_res1 = (uint8_t)res1;
						m_accretion.m_res2 = (uint8_t)res2;
					} else {
						r_log <= Util::PrintYAMLException("Scene file: 'resolution' in 'accretion' must be a sequence with two elements.", resolution.Mark()) <= endl;
						return false;
					}
				} else {
					r_log <= Util::PrintYAMLException("Scene file: cannot find 'resolution' in 'accretion'.", accretion.Mark()) <= endl;
					return false;
				}

				// yaw
				if(accretion["yaw"]) {
					m_observer.m_yaw = Animated<Util::Angle>::ParseYAML(accretion["yaw"]);
				} else {
					m_accretion.m_yaw = Angle(0.0);
				}

				// size
				if(accretion["radius"]) {
					// Discs require two radii
					YAML::Node radii = accretion["radius"];
					if(radii.IsSequence() && radii.size() == 2) {
						m_accretion.m_innerRadius = Animated<double>::ParseYAML(radii[0]);
						m_accretion.m_outerRadius = Animated<double>::ParseYAML(radii[1]);
					} else {
						r_log <= Util::PrintYAMLException("Scene file: 'radius' in 'accretion' must be a sequence with two elements.", radii.Mark()) <= endl;
						return false;
					}
				} else {
					r_log <= Util::PrintYAMLException("Scene file: cannot find 'radius' in 'accretion'.", accretion.Mark()) <= endl;
					return false;
				}

			} else {
				r_log <= Util::PrintYAMLException("Scene file: 'accretion' must be a map.", accretion.Mark()) <= endl;
				return false;
			}
		} else {
			// Disable accretion
			m_hasAccretion = false;
		}

		// Skybox
		if(scene["skymap"]) {
			// Enable skybox
			m_hasSkyMap = true;

			YAML::Node skybox = scene["skymap"];
			if(skybox.IsMap()) {

				// Image file
				if(skybox["image"]) {
					m_skyMap.m_imageFile = skybox["image"].as<string>();
				} else {
					r_log <= Util::PrintYAMLException("Scene file: cannot find 'image' in 'skymap'.", skybox.Mark()) <= endl;
					return false;
				}

				// Boundary
				if(skybox["boundary"]) {
					m_skyMap.m_boundary = Animated<double>::ParseYAML(skybox["boundary"]);
				} else {
					r_log <= Util::PrintYAMLException("Scene file: cannot find 'boundary' in 'skymap'.", skybox.Mark()) <= endl;
					return false;
				}

			} else {
				r_log <= Util::PrintYAMLException("Scene file: 'skymap' must be a map.", skybox.Mark()) <= endl;
				return false;
			}
		} else {
			// Disable skybox
			m_hasSkyMap = false;
		}

		// Colors
		{
			if(scene["sky_color"]) {
				m_skyColor = Color::ParseYAML(scene["sky_color"]);
			} else {
				m_skyColor = { 0, 0, 0 };
			}

			if(scene["horizon_color"]) {
				m_horizonColor = Color::ParseYAML(scene["horizon_color"]);
			} else {
				m_horizonColor = { 255, 0, 0 };
			}

			if(scene["error_color"]) {
				m_errorColor = Color::ParseYAML(scene["error_color"]);
			} else {
				m_errorColor = { 0, 0, 255 };
			}
		}

		// Objects
		for(auto i = scene.begin(); i != scene.end(); i++) {

			string key = i->first.as<string>();
			if(key == "sphere") {
				SceneSphere s;

				// Check whether maximum has already been reached
				if(m_spheres.size() >= MAX_SCENE_SPHERES) {
					r_log <= Util::PrintYAMLException("Scene file: cannot create more than " + Util::ToString(MAX_SCENE_SPHERES) + " spheres.", i->second.Mark()) <= endl;
					return false;
				}

				YAML::Node sphere = i->second;

				if(sphere.IsMap()) {

					// r
					if(sphere["r"]) {
						s.r = Animated<double>::ParseYAML(sphere["r"]);
					} else {
						r_log <= Util::PrintYAMLException("Scene file: cannot find 'r' in 'sphere'.", sphere.Mark()) <= endl;
						return false;
					}
					// theta
					if(sphere["theta"]) {
						s.theta = Animated<Util::Angle>::ParseYAML(sphere["theta"]);
					} else {
						r_log <= Util::PrintYAMLException("Scene file: cannot find 'theta' in 'sphere'.", sphere.Mark()) <= endl;
						return false;
					}
					// phi
					if(sphere["phi"]) {
						s.phi = Animated<Util::Angle>::ParseYAML(sphere["phi"]);
					} else {
						r_log <= Util::PrintYAMLException("Scene file: cannot find 'phi' in 'sphere'.", sphere.Mark()) <= endl;
						return false;
					}

					// size
					if(sphere["radius"]) {
						s.m_radius = Animated<double>::ParseYAML(sphere["radius"]);
					} else {
						r_log <= Util::PrintYAMLException("Scene file: cannot find 'size' in 'sphere'.", sphere.Mark()) <= endl;
						return false;
					}

					// yaw
					if(sphere["yaw"]) {
						s.m_yaw = Animated<Util::Angle>::ParseYAML(sphere["yaw"]);
					} else {
						s.m_yaw = Angle(0.0);
					}
					// pitch
					if(sphere["pitch"]) {
						s.m_pitch = Animated<Util::Angle>::ParseYAML(sphere["pitch"]);
					} else {
						s.m_pitch = Angle(0.0);
					}
					// roll
					if(sphere["roll"]) {
						s.m_roll = Animated<Util::Angle>::ParseYAML(sphere["roll"]);
					} else {
						s.m_roll = Angle(0.0);
					}

					// resolution
					if(sphere["resolution"]) {
						YAML::Node resolution = sphere["resolution"];
						if(resolution.IsSequence() && resolution.size() == 2) {
							uint64_t res1 = resolution[0].as<uint64_t>();
							uint64_t res2 = resolution[1].as<uint64_t>();
							
							if(res1 > 255 || res2 > 255) {
								r_log <= Util::PrintYAMLException("Scene file: the values of 'resolution' in 'sphere' must lie between 0 and 255.", resolution.Mark()) <= endl;
								return false;
							}
							s.m_res1 = (uint8_t)res1;
							s.m_res2 = (uint8_t)res2;
						} else {
							r_log <= Util::PrintYAMLException("Scene file: 'resolution' in 'sphere' must be a sequence with two elements.", resolution.Mark()) <= endl;
							return false;
						}
					} else {
						r_log <= Util::PrintYAMLException("Scene file: cannot find 'resolution' in 'sphere'.", sphere.Mark()) <= endl;
						return false;
					}

					// Color
					if(sphere["color"]) {
						s.m_color = Color::ParseYAML(sphere["color"]);
					} else {
						r_log <= Util::PrintYAMLException("Scene file: cannot find 'color' in 'sphere'.", sphere.Mark());
						return false;
					}

				} else {
					r_log <= Util::PrintYAMLException("Scene file: 'sphere' must be a map.", sphere.Mark()) <= endl;
					return false;
				}

				// Add sphere
				m_spheres.push_back(s);
			}

		}

	} catch(YAML::ParserException const& e) {

		r_log <= "Could not parse scene file " <= sceneFile <= ":" <= endl;
		r_log <= "  " <= Util::PrintYAMLException(e.msg, e.mark) <= endl;
		return false;

	} catch(YAML::RepresentationException const& e) {

		r_log <= "Unexpected YAML in scene file:" <= endl;
		r_log <= "  " <= Util::PrintYAMLException(e.msg, e.mark) <= endl;
		r_log <= "  Parsing failed." <= endl;
		return false;

	} catch(YAML::BadFile const&) {

		r_log <= "Could not load scene file " <= sceneFile <= "." <= endl;
		return false;

	}

	// Log
	r_log << "Done loading scene file." << endl << endl;
	r_log << Log::HLINE << endl << endl;

	return true;
}

void Scene::ConfigureFromCmd(
	string const& outputDirectory,
	string const& outputConfig,
	uint32_t frameCount,
	uint32_t frameWidth,
	uint32_t frameHeight
) {

	// Output
	m_outputSceneInformation = false;
	m_outputColorImages = false;
	m_outputRedshiftImages = false;
	m_outputPixelData = false;
	for(char c : outputConfig) {
		switch(c) {
			case 'i':
				m_outputSceneInformation = true;
				break;
			case 'c':
				m_outputColorImages = true;
				break;
			case 'r':
				m_outputRedshiftImages = true;
				break;
			case 'd':
				m_outputPixelData = true;
				break;
		}
	}

	// Frame rendering specific
	m_frameCount = frameCount;
	m_frameWidth = frameWidth;
	m_frameHeight = frameHeight;
	m_pixelCount = m_frameWidth * m_frameHeight;

	m_renderFrames = 
		(frameCount > 0)
		&& (frameWidth > 0)
		&& (frameHeight > 0)
		&& (!outputDirectory.empty());

	// Create scene strings
	CreateSceneStrings();
}

bool Scene::RequestUserApproval() {

	// If the log is quiet, return true
	if(!r_log.m_showLogInCout) return true;

	// Print summary
	r_log <= m_summaryString;

	// Ask user
	r_log <= "continue? (y/n): ";

	char c = cin.get();
	r_log.m_showLogInCout = false;
	r_log <= c <= endl;
	r_log.m_showLogInCout = true;

	r_log <= endl;

	return (c == 'y' || c == 'Y');
}

void Scene::CreateFrameData(
	uint32_t frameNumber,
	SceneData* sceneData,
	SceneSphereData* sceneSpheres
) {

	// Animation number
	double t;
	if(m_frameCount == 1) {
		t = 0.0;
	} else {
		t = (double)frameNumber / (double)(m_frameCount - 1);
	}

	// Metric

	sceneData->m_metric.m = m_metric.m.Animate(t);
	sceneData->m_metric.a = m_metric.a.Animate(t);
	sceneData->m_metric.q = m_metric.q.Animate(t);
	// r = m + sqrt(m^2 - a^2 - q^2)
	sceneData->m_metric.m_horizonRadius = 
		sceneData->m_metric.m + sqrt(sqr(sceneData->m_metric.m) - sqr(sceneData->m_metric.a) - sqr(sceneData->m_metric.q));

	// Observer

	sceneData->m_observer.r = m_observer.r.Animate(t);
	sceneData->m_observer.theta = m_observer.theta.Animate(t).m_rad;
	sceneData->m_observer.phi = m_observer.phi.Animate(t).m_rad;

	sceneData->m_observer.m_screenRight = tan(m_observer.m_hFov.Animate(t).m_rad / 2.0);

	// Compute vFov if needed
	if(m_observer.m_useAspectRatio) {
		sceneData->m_observer.m_screenTop = sceneData->m_observer.m_screenRight * (double)m_frameHeight / (double)m_frameWidth;
	} else {
		sceneData->m_observer.m_screenTop = tan(m_observer.m_vFov.Animate(t).m_rad / 2.0);
	}

	sceneData->m_observer.m_yaw = m_observer.m_yaw.Animate(t).m_rad;
	sceneData->m_observer.m_pitch = m_observer.m_pitch.Animate(t).m_rad;
	sceneData->m_observer.m_roll = m_observer.m_roll.Animate(t).m_rad;

	// Accretion disc
	sceneData->m_hasAccretion = (m_hasAccretion) ? 1 : 0;
	if(m_hasAccretion) {
		sceneData->m_accretion.m_color1 = m_accretion.m_color1;
		sceneData->m_accretion.m_color2 = m_accretion.m_color2;
		sceneData->m_accretion.m_res1 = m_accretion.m_res1;
		sceneData->m_accretion.m_res2 = m_accretion.m_res2;
		sceneData->m_accretion.m_yaw = m_accretion.m_yaw.Animate(t).m_rad;
		sceneData->m_accretion.m_innerRadius = m_accretion.m_innerRadius.Animate(t);
		sceneData->m_accretion.m_outerRadius = m_accretion.m_outerRadius.Animate(t);
	}

	// Skybox
	sceneData->m_hasSkyMap = (m_hasSkyMap) ? 1 : 0;

	// Colors
	sceneData->m_skyColor = m_skyColor;
	sceneData->m_horizonColor = m_horizonColor;
	sceneData->m_errorColor = m_errorColor;

	// Objects and scene boundary
	sceneData->m_sceneBoundary = max(sceneData->m_observer.r, sceneData->m_metric.m_horizonRadius * 4.0);

	if(m_hasAccretion) {
		sceneData->m_sceneBoundary = max(sceneData->m_sceneBoundary, sceneData->m_accretion.m_outerRadius);
	}
	if(m_hasSkyMap) {
		sceneData->m_sceneBoundary = max(sceneData->m_sceneBoundary, m_skyMap.m_boundary.Animate(t));
	}

	sceneData->m_spheresCount = (uint32_t)m_spheres.size();
	uint32_t i = 0;
	for(auto& s : m_spheres) {
		sceneSpheres[i].m_color = s.m_color;
		sceneSpheres[i].m_res1 = s.m_res1;
		sceneSpheres[i].m_res2 = s.m_res2;

		sceneSpheres[i].r = s.r.Animate(t);
		sceneSpheres[i].theta = s.theta.Animate(t).m_rad;
		sceneSpheres[i].phi = s.phi.Animate(t).m_rad;

		Util::Vector4_double cartPos = Raytracer::BLToCartesian({ 0.0, sceneSpheres[i].r, sceneSpheres[i].theta, sceneSpheres[i].phi }, sceneData->m_metric.a);
		sceneSpheres[i].x = cartPos.m_1;
		sceneSpheres[i].y = cartPos.m_2;
		sceneSpheres[i].z = cartPos.m_3;

		sceneSpheres[i].m_radius = s.m_radius.Animate(t);

		sceneSpheres[i].m_yaw = s.m_yaw.Animate(t).m_rad;
		sceneSpheres[i].m_pitch = s.m_pitch.Animate(t).m_rad;
		sceneSpheres[i].m_roll = s.m_roll.Animate(t).m_rad;

		sceneData->m_sceneBoundary = max(sceneData->m_sceneBoundary, sceneSpheres[i].r + sceneSpheres[i].m_radius);

		i++;
	}

}

void Scene::OutputSceneInformation(string const& fileName) {

	// Open filestream
	ofstream os;
	os.open(fileName);
	if(!os.is_open()) {
		throw runtime_error("Cannot open file " + fileName + " for writing.");
	}

	// Print
	os << m_detailedString << endl;

	// Close filestream
	os.close();
}

void Scene::CreateSceneStrings() {
	stringstream ss;

	ss << "Scene configuration:" << endl << endl;

	ss << "output scene information: " << ((m_outputSceneInformation) ? "yes" : "no") << endl;

	// cuRRay usage modes
	{
		if(m_renderFrames) {
			ss << "frames:" << endl;
			ss << "  count: " << m_frameCount << endl;
			ss << "  dimensions: " << m_frameWidth << "x" << m_frameHeight << " pixels" << endl;
			ss << "  output color images: " << ((m_outputColorImages) ? "yes" : "no") << endl;
			ss << "  output redshift images: " << ((m_outputRedshiftImages) ? "yes" : "no") << endl;
			ss << "  output pixel data: " << ((m_outputPixelData) ? "yes" : "no") << endl;
		} else {
			ss << "no frames to render" << endl;
		}
		ss << endl;
	}

	// Metric
	{
		ss << "metric:" << endl;
		ss << "  m: " << m_metric.m << endl;
		ss << "  a: " << m_metric.a << endl;
		ss << "  q: " << m_metric.q << endl;

		ss << endl;
	}

	// Observer
	if(m_renderFrames) {
		ss << "observer used for frames:" << endl;
		ss << "  r: " << m_observer.r << endl;
		ss << "  theta: " << m_observer.theta << endl;
		ss << "  phi: " << m_observer.phi << endl;

		ss << "  yaw: " << m_observer.m_yaw << endl;
		ss << "  pitch: " << m_observer.m_pitch << endl;
		ss << "  roll: " << m_observer.m_roll << endl;

		ss << "  hFov: " << m_observer.m_hFov << endl;
		if(m_observer.m_useAspectRatio) {
			ss << "  vFov: computed from aspect ratio" << endl;
		} else {
			ss << "  vFov: " << m_observer.m_vFov << endl;
		}

		ss << endl;
	}

	// Accretion
	if(m_renderFrames && m_hasAccretion) {
		ss << "accretion disc:" << endl;
		ss << "  color1: " << (int)m_accretion.m_color1.m_red << ", " << (int)m_accretion.m_color1.m_green << ", " << (int)m_accretion.m_color1.m_blue << endl;
		ss << "  color2: " << (int)m_accretion.m_color2.m_red << ", " << (int)m_accretion.m_color2.m_green << ", " << (int)m_accretion.m_color2.m_blue;
		if(m_accretion.m_calculateColor2) {
			ss << ", calculated from color1";
		}
		ss << endl;

		ss << "  resolution: " << (int)m_accretion.m_res1 << ", " << (int)m_accretion.m_res2 << endl;

		ss << "  yaw: " << m_accretion.m_yaw << endl;

		ss << "  radius: " << m_accretion.m_innerRadius << ", " << m_accretion.m_outerRadius << endl;

		ss << endl;
	}

	// Sky map
	if(m_renderFrames && m_hasSkyMap) {
		ss << "sky map:" << endl;
		ss << "  image file: " << m_skyMap.m_imageFile << endl;
		ss << "  boundary: " << m_skyMap.m_boundary << endl;

		ss << endl;
	}

	// Objects
	uint32_t i = 0;
	for(auto& o : m_spheres) {

		ss << "sphere " << i << ":" << endl;

		ss << "  resolution: " << (int)o.m_res1 << ", " << (int)o.m_res2 << endl;
		ss << "  color: " << (int)o.m_color.m_red << ", " << (int)o.m_color.m_green << ", " << (int)o.m_color.m_blue << endl;

		ss << "  r: " << o.r << endl;
		ss << "  theta: " << o.theta << endl;
		ss << "  phi: " << o.phi << endl;

		ss << "  yaw: " << o.m_yaw << endl;
		ss << "  pitch: " << o.m_pitch << endl;
		ss << "  roll: " << o.m_roll << endl;

		ss << "  radius: " << o.m_radius << endl;

		ss << endl;

		i++;
	}

	ss << Log::HLINE << endl;
	ss << endl;

	// Set summray string
	m_summaryString = ss.str();

	// Print animation values for every frame
	if(m_renderFrames) {
		for(uint32_t i = 0; i < m_frameCount; i++) {
			ss << "frame " << i << " animated values:" << endl;
			bool hasAnimatedValues = false;

			double t;
			if(m_frameCount > 1) {
				t = (double)i / (double)(m_frameCount - 1);
			} else {
				t = 0.0;
			}

			// Metric
			if(m_metric.m.m_type != AnimationType::SINGLE) {
				hasAnimatedValues = true;
				ss << "  metric.m: " << m_metric.m.Animate(t) << endl;
			}
			if(m_metric.a.m_type != AnimationType::SINGLE) {
				hasAnimatedValues = true;
				ss << "  metric.a: " << m_metric.a.Animate(t) << endl;
			}
			if(m_metric.q.m_type != AnimationType::SINGLE) {
				hasAnimatedValues = true;
				ss << "  metric.q: " << m_metric.q.Animate(t) << endl;
			}

			// Observer
			if(m_observer.r.m_type != AnimationType::SINGLE) {
				hasAnimatedValues = true;
				ss << "  observer.r: " << m_observer.r.Animate(t) << endl;
			}
			if(m_observer.theta.m_type != AnimationType::SINGLE) {
				hasAnimatedValues = true;
				ss << "  observer.theta: " << m_observer.theta.Animate(t) << endl;
			}
			if(m_observer.phi.m_type != AnimationType::SINGLE) {
				hasAnimatedValues = true;
				ss << "  observer.phi: " << m_observer.phi.Animate(t) << endl;
			}
			if(m_observer.m_yaw.m_type != AnimationType::SINGLE) {
				hasAnimatedValues = true;
				ss << "  observer.yaw: " << m_observer.m_yaw.Animate(t) << endl;
			}
			if(m_observer.m_pitch.m_type != AnimationType::SINGLE) {
				hasAnimatedValues = true;
				ss << "  observer.pitch: " << m_observer.m_pitch.Animate(t) << endl;
			}
			if(m_observer.m_roll.m_type != AnimationType::SINGLE) {
				hasAnimatedValues = true;
				ss << "  observer.roll: " << m_observer.m_roll.Animate(t) << endl;
			}
			if(m_observer.m_hFov.m_type != AnimationType::SINGLE) {
				hasAnimatedValues = true;
				ss << "  observer.hfov: " << m_observer.m_hFov.Animate(t) << endl;
			}
			if(!m_observer.m_useAspectRatio && m_observer.m_vFov.m_type != AnimationType::SINGLE) {
				hasAnimatedValues = true;
				ss << "  observer.vfov: " << m_observer.m_vFov.Animate(t) << endl;
			}

			// Accretion disc
			if(m_hasAccretion) {
				if(m_accretion.m_yaw.m_type != AnimationType::SINGLE) {
					hasAnimatedValues = true;
					ss << "  accretion.yaw: " << m_accretion.m_yaw.Animate(t) << endl;
				}
				if(m_accretion.m_innerRadius.m_type != AnimationType::SINGLE) {
					hasAnimatedValues = true;
					ss << "  accretion.radius[0]: " << m_accretion.m_innerRadius.Animate(t) << endl;
				}
				if(m_accretion.m_outerRadius.m_type != AnimationType::SINGLE) {
					hasAnimatedValues = true;
					ss << "  accretion.radius[1]: " << m_accretion.m_outerRadius.Animate(t) << endl;
				}
			}

			// Sky map
			if(m_hasSkyMap) {
				if(m_skyMap.m_boundary.m_type != AnimationType::LINEAR) {
					hasAnimatedValues = true;
					ss << "  skymap.boundary: " << m_skyMap.m_boundary.Animate(t) << endl;
				}
			}

			// Spheres
			for(uint32_t j = 0; j < m_spheres.size(); j++) {
				if(m_spheres[j].r.m_type != AnimationType::SINGLE) {
					hasAnimatedValues = true;
					ss << "  sphere[" << j << "].r: " << m_spheres[j].r.Animate(t) << endl;
				}
				if(m_spheres[j].theta.m_type != AnimationType::SINGLE) {
					hasAnimatedValues = true;
					ss << "  sphere[" << j << "].theta: " << m_spheres[j].theta.Animate(t) << endl;
				}
				if(m_spheres[j].phi.m_type != AnimationType::SINGLE) {
					hasAnimatedValues = true;
					ss << "  sphere[" << j << "].phi: " << m_spheres[j].phi.Animate(t) << endl;
				}
				if(m_spheres[j].m_yaw.m_type != AnimationType::SINGLE) {
					hasAnimatedValues = true;
					ss << "  sphere[" << j << "].yaw: " << m_spheres[j].m_yaw.Animate(t) << endl;
				}
				if(m_spheres[j].m_pitch.m_type != AnimationType::SINGLE) {
					hasAnimatedValues = true;
					ss << "  sphere[" << j << "].pitch: " << m_spheres[j].m_pitch.Animate(t) << endl;
				}
				if(m_spheres[j].m_roll.m_type != AnimationType::SINGLE) {
					hasAnimatedValues = true;
					ss << "  sphere[" << j << "].roll: " << m_spheres[j].m_roll.Animate(t) << endl;
				}
				if(m_spheres[j].m_radius.m_type != AnimationType::SINGLE) {
					hasAnimatedValues = true;
					ss << "  sphere[" << j << "].radius: " << m_spheres[j].m_radius.Animate(t) << endl;
				}
			}

			if(!hasAnimatedValues) {
				ss << "  no animated values." << endl;
			}
			ss << endl;
		}

	}

	m_detailedString = ss.str();
}

}
}