MAX_REG = 63
PTXA_OPTIONS = -v
DEFINES = -DUSE_CUDA

SRC = ./src
OBJ = ./x64/Release_linux
OUT = ../x64/Release_linux

INC = -I. -I./src -I/usr/include -I/usr/local/include -I./redist/tclap-1.2.1/include -I./redist/pngIO
LIB = -L/usr/lib -L/usr/local/lib

NVCC = nvcc
CC = g++-5

NVCC_FLAGS = \
	-arch=sm_30 -m64 -std=c++11 -cudart static -ccbin $(CC) \
	--keep-dir $(obj) -Xptxas=$(PTXA_OPTIONS) \
	--maxrregcount=$(MAX_REG) -use_fast_math $(DEFINES) -O2

LINK_FLAGS = \
	-lpthread -lcudart -lpng -lz -lboost_system -lboost_filesystem -lyaml-cpp

OBJECTS = \
	gpu/gpuManager.o \
	kerr_newman/frameRenderer.o kerr_newman/raytracer.o kerr_newman/scene.o \
	system/log.o system/program.o \
	main.o

OBJECT_FILES = $(patsubst %,$(OBJ)/%,$(OBJECTS))

all: cuRRay

cuRRay: create_directories $(OBJECT_FILES)
	$(NVCC) $(NVCC_FLAGS) $(OBJECT_FILES) -o $(OUT)/cuRRay $(LINK_FLAGS)

$(OBJ)/%.o: $(SRC)/%.cpp
	-mkdir -p $(dir $@)
	$(NVCC) $(NVCC_FLAGS) -dc $(INC) $(LIB) $< -o $@

$(OBJ)/%.o: $(SRC)/%.cu
	-mkdir -p $(dir $@)
	$(NVCC) $(NVCC_FLAGS) -dc $(INC) $(LIB) $< -o $@


create_directories:
	-mkdir -p $(OBJ)
	-mkdir -p $(OUT)

clean:
	rm -f -r $(OBJ)
	rm -f -r $(OUT)
